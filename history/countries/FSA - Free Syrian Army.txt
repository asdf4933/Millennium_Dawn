﻿2000.1.1 = {
	add_ideas = {
		#pop_050
		rampant_corruption
		gdp_3
		sunni
		defence_09
		edu_01
		health_01
		social_01
		bureau_03
		police_01
		#censored_press
		#parties_harassment
		#state_religion
		youth_radicalization
		al_jazeera_allowed
		volunteer_army
		volunteer_women
		The_Ulema
		farmers
		small_medium_business_owners
		#tribalism
		
		#tax_cost_30
	}

	capital = 184
	oob = "FSA_2000"
	set_convoys = 40
	
	### FSA ###
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 30 }
	add_to_array = { influence_array = SAU.id }
	add_to_array = { influence_array_val = 42 }
	add_to_array = { influence_array = QAT.id }
	add_to_array = { influence_array_val = 33 }
	add_to_array = { influence_array = TUR.id }
	add_to_array = { influence_array_val = 50 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 36 }
	add_to_array = { influence_array = ENG.id }
	add_to_array = { influence_array_val = 4 }
	add_to_array = { influence_array = FRA.id }
	add_to_array = { influence_array_val = 4 }
	startup_influence = yes
	
	create_field_marshal = {
		name = "Riad Al-Asaad"
		picture = "FSA_Gen_Riad_Al-Asaad.dds"
		traits = { defensive_doctrine old_guard }
		id = 21900
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
		
	}

	#Died in 2013
	create_corps_commander = {
		name = "Abdul Qader Saleh"
		picture = "FSA_Gen_Adb_Al-Kader_Saleh.dds"
		traits = {  }
		id = 21901
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
		
	}

	create_corps_commander = {
		name = "Abu Issa"
		picture = "Gen_Abu_Issa.dds"
		traits = { commando trickster }
		id = 21902
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
		
	}

	create_corps_commander = {
		name = "Bashar Al-Zoubi"
		picture = "Gen_Bashar_Al-Zoubi.dds"
		traits = { commando trait_engineer trickster  }
		id = 21903
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
		
	}

	create_corps_commander = {
		name = "Saber Safar"
		picture = "Gen_Saber_Safar.dds"
		traits = { commando desert_fox urban_assault_specialist }
		id = 21904
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
		
	}

	create_corps_commander = {
		name = "Abdul Jabbar Aqidi"
		picture = "gen_Abdul-Jabbar_Aqidi.dds"
		traits = { commando trait_engineer urban_assault_specialist }
		id = 21905
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
		
	}

	create_corps_commander = {
		name = "Ziad Fahd"
		picture = "Gen_Ziad_Fahd.dds"
		traits = { desert_fox urban_assault_specialist  }
		id = 21906
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
		
	}
}

2017.1.1 = {
	capital = 184
	oob = "FSA_2017"
	set_convoys = 80

	add_manpower = 55000

	add_ideas = {
		#pop_050
		rampant_corruption
		gdp_3
		sunni
		defence_09
		edu_01
		health_01
		social_01
		bureau_03
		police_01
		#censored_press
		#parties_harassment
		#state_religion
		youth_radicalization
		al_jazeera_allowed
		volunteer_army
		volunteer_women
		The_Ulema
		farmers
		small_medium_business_owners
		#tribalism
		
		#tax_cost_30
	}
	
	add_opinion_modifier = { target = JOR modifier = supports_us_2 }
	
	set_country_flag = enthusiastic_The_Ulema
	set_country_flag = positive_farmers
	
	set_variable = { var = debt value = 0 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 0.4 }
	set_variable = { var = tax_rate value = 30 }
	
	#set_variable = { var = size_modifier value = 0.08 } #1 CIC
	#initial_money_setup = yes

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot

	diplomatic_relation = {
		country = TUR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = JOR
		relation = military_access
		active = yes
	}
	diplomatic_relation = {
		country = NUS
		relation = military_access
		active = yes
	}
	
	set_technology = { 
		legacy_doctrines = 1 
		infiltration_assault = 1 
		frontline_defence = 1 
		early_tunnel_warfare = 1 
		guerilla_specialisation = 1

		infantry_weapons = 1
		
		combat_eng_equipment = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		
		command_control_equipment = 1
		command_control_equipment1 = 1
		
		land_Drone_equipment = 1
		
		Early_APC = 1 #Vehicle Design
		
		APC_1 = 1
		
		MBT_1 = 1
		
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		
		artillery_0 = 1
		SP_arty_0 = 1
		SP_R_arty_0 = 1
		
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1

		landing_craft = 1

		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 20
			}

			fascism = {
				popularity = 25
			}
			
			communism = {
				popularity = 0
				#banned = no #default is no
			}
			neutrality = { 
				popularity = 55
			}
		}
		
		ruling_party = neutrality
		last_election = "2012.3.25"
		election_frequency = 60
		elections_allowed = no
	}
	start_politics_input = yes
	set_variable = { party_pop_array^12 = 1 } #Neutral_Muslim_Brotherhood
	add_to_array = { ruling_party = 12 }
	startup_politics = yes

	add_opinion_modifier = { target = SYR modifier = hostile_status }
	add_opinion_modifier = { target = ISI modifier = hostile_status }
	add_opinion_modifier = { target = HEZ modifier = hostile_status }
	add_opinion_modifier = { target = PER modifier = hostile_status }

	#Country leaders
	##Western
	create_country_leader = {
		name = "Abdul-Ilah Al-Bashir"		#Pro-US and pro-EU, anti-Iran
		desc = ""
		picture = "FSA_Abdul-Ilah_Al-Bashir.dds"
		expire = "2050.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}

	##Emerging

	##Salafist
	create_country_leader = {
		name = "Hassan Soufan"				#Leader of Syrian Liberation Front
		desc = ""
		picture = "FSA_Hassan_Soufan.dds"
		expire = "2050.1.1"
		ideology = Caliphate
		traits = {
			#
		}
	}

	##Nonaligned
	create_country_leader = {
		name = "Mohamed Dheere"				#Main leader of Turkish Backed Free Syrian Army
		desc = ""
		picture = "FSA_Mohamed_Dheere.dds"
		expire = "2050.1.1"
		ideology = Neutral_Muslim_Brotherhood
		traits = {
			#
		}
	}
	create_country_leader = {
		name = "Anas Al-Abdah"				#Syrian National Coalition
		desc = ""
		picture = "FSA_Anas_Al-Abdah.dds"
		expire = "2050.1.1"
		ideology = Neutral_Muslim_Brotherhood
		traits = {
			#
		}
	}

	##nationalist
	create_country_leader = {
		name = "Ali Haidar"			#SSNP
		desc = ""
		picture = "SYR_Ali_Haidar.dds"
		ideology = Nat_Fascism	
		traits = {
		}
	}
}