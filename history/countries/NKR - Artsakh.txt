﻿2000.1.1 = {
	add_ideas = {
		#pop_050
		#small_medium_business_owners
		#Labour_Unions
		#Mass_Media
		orthodox_christian
		unrestrained_corruption
		gdp_5
		stable_growth
		defence_04
		edu_02
		health_02
		social_01
		bureau_02
		police_03
		#gerrymandering
		#accountable_press
		#state_religion
		draft_army
		volunteer_women
		international_bankers
		landowners
		The_Clergy
		#civil_law
		#tax_cost_23
	}

	capital = 710
	oob = "NKR_2000"
	set_convoys = 20
	
	add_ideas = {
		gdp_2
		#tax_cost_14
	}
	give_military_access = ARM
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 20 }
	add_to_array = { influence_array = ARM.id }
	add_to_array = { influence_array_val = 125 }
	
	set_technology = { 
		#K-3 Rifle
		infantry_weapons = 1
		
		#N-2 MRLS
		artillery_0 = 1
		SP_arty_0 = 1
		SP_R_arty_0 = 1
		
		night_vision_1 = 1
		
		#For templates
		
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		IFV_1 = 1
		util_vehicle_0 = 1
		Rec_tank_0 = 1
		MBT_1 = 1
	}
	
	set_variable = { var = debt value = 1 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 0 }
	set_variable = { var = tax_rate value = 14 }
	
	#set_variable = { var = size_modifier value = 0.08 } #1 CIC
	#initial_money_setup = yes
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 34
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 39
			}
			
			neutrality = {
				popularity = 12
			}
			
			nationalist = {
				popularity = 14
			}
		}
		
		ruling_party = democratic
		last_election = "1998.3.30"
		election_frequency = 48
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Arkadi Ghukasyan"
		picture = "Arkadi_Ghukasyan.dds"
		expire = "2065.1.1"
		ideology = Conservative
		traits = {
		}
	}
}

2017.1.1 = {
	capital = 710
	oob = "NKR_2017"
	set_convoys = 20
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		#K-3 Rifle
		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		infantry_weapons3 = 1
		
		#N-2 MRLS
		artillery_0 = 1
		SP_arty_0 = 1
		SP_arty_1 = 1
		SP_arty_2 = 1
		SP_R_arty_0 = 1
		SP_R_arty_1 = 1
		SP_R_arty_2 = 1
		
		night_vision_1 = 1
		
		#For templates
		
		combat_eng_equipment = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		Early_APC = 1
		APC_1 = 1
		IFV_1 = 1
		util_vehicle_0 = 1
		Rec_tank_0 = 1
		MBT_1 = 1
	}

	add_ideas = {
		#pop_050
		#small_medium_business_owners
		#Labour_Unions
		#Mass_Media
		orthodox_christian
		unrestrained_corruption
		gdp_5
		stable_growth
		defence_04
		edu_02
		health_02
		social_01
		bureau_02
		police_03
		#gerrymandering
		#accountable_press
		#state_religion
		draft_army
		volunteer_women
		international_bankers
		landowners
		The_Clergy
		#civil_law
		#tax_cost_23
	}
	
	#set_country_flag = gdp_3
	set_country_flag = positive_international_bankers
	set_country_flag = negative_landowners
	
	set_variable = { var = debt value = 6 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 2 }
	set_variable = { var = tax_rate value = 23 }
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot

	set_politics = {

		parties = {
			democratic = { 
				popularity = 20
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 65
			}
			
			neutrality = {
				popularity = 10
			}
			
			nationalist = {
				popularity = 5
			}
		}
		
		ruling_party = communism
		last_election = "2013.2.18"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Bako Sahakyan"
		desc = "POLITICS_KING_ZOG_DESC"
		picture = "Bako_Sahakyan.dds"
		expire = "2065.1.1"
		ideology = Conservative
		traits = {
			#
		}
	}
}