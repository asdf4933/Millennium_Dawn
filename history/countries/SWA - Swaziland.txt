﻿2000.1.1 = {
	add_ideas = {
		#pop_050
		rampant_corruption
		gdp_3
		christian
		stagnation
		defence_02
		edu_03
		health_03
		social_02
		bureau_02
		police_03
		volunteer_army
		volunteer_women
		international_bankers
		farmers
		small_medium_business_owners
		#hybrid
		#tax_cost_28
	}

	capital = 271
	oob = "SWA_2017"
	set_convoys = 5
	
	add_ideas = {
		gdp_3
		#tax_cost_18
	}
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 40 }
	add_to_array = { influence_array = SAF.id }
	add_to_array = { influence_array_val = 80 }
	add_to_array = { influence_array = MOZ.id }
	add_to_array = { influence_array_val = 30 }
	add_to_array = { influence_array = ENG.id }
	add_to_array = { influence_array_val = 5 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 3 }
	add_to_array = { influence_array = TAI.id }
	add_to_array = { influence_array_val = 10 }
	add_to_array = { influence_array = CHI.id }
	add_to_array = { influence_array_val = 1 }
	startup_influence = yes
	
	set_variable = { var = debt value = 0.4 }
	set_variable = { var = treasury value = 1 }
	set_variable = { var = tax_rate value = 18 }
	set_variable = { var = int_investments value = 0 }
	#initial_money_setup = yes
	
	# Starting tech
	set_technology = { 
		
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		
		infantry_weapons = 1

		command_control_equipment = 1
		
		land_Drone_equipment = 1
		
		Anti_tank_0 = 1
		
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		
		combat_eng_equipment = 1
		
		night_vision_1 = 1
	}

	set_politics = {

		parties = {
			democratic = { 
				popularity = 10
			}

			fascism = {
				popularity = 72
			}
			
			communism = {
				popularity = 8
			}
			
			neutrality = {
				popularity = 10
			}
			
			nationalist = {
				 popularity = 0
			}
		}
		
		ruling_party = communism
		last_election = "1995.11.29"
		election_frequency = 72
		elections_allowed = yes
	}	
	create_country_leader = {
		name = "Mswati III"
		picture = ""
		expire = "2065.6.10"
		ideology = Kingdom
		traits = {
		
		}
	}
	
}

2017.1.1 = {
	capital = 271
	oob = "SWA_2017"
	set_convoys = 5
	
	add_ideas = {
		#pop_050
		rampant_corruption
		gdp_3
		christian
		stagnation
		defence_02
		edu_03
		health_03
		social_02
		bureau_02
		police_03
		volunteer_army
		volunteer_women
		international_bankers
		farmers
		small_medium_business_owners
		#hybrid
		#tax_cost_28
	}
	#set_country_flag = gdp_3
	set_country_flag = negative_farmers
	set_country_flag = negative_small_medium_business_owners
	
	set_variable = { var = debt value = 1 }
	set_variable = { var = treasury value = 1 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 28 }
	
	#set_variable = { var = size_modifier value = 0.08 } #1 CIC
	#initial_money_setup = yes
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot

	# Starting tech
	set_technology = { 
		
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		
		infantry_weapons = 1

		command_control_equipment = 1
		
		land_Drone_equipment = 1
		
		Anti_tank_0 = 1
		
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		
		combat_eng_equipment = 1
		
		night_vision_1 = 1
	}

	set_politics = {

		parties = {
			democratic = { 
				popularity = 5
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 0
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 95
			}
		}
		
		ruling_party = neutrality
		last_election = "1932.11.8"
		election_frequency = 48
		elections_allowed = no
	}

	create_country_leader = {
		name = "Mswati III"
		desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
		picture = "Mswati_III.dds"
		expire = "2050.1.1"
		ideology = Neutral_Autocracy
		traits = {
			#
		}
	}
}