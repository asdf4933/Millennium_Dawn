﻿2000.1.1 = {
	add_ideas = {
		#pop_050
		paralyzing_corruption
		gdp_1
		sunni
		defence_09
		LoAS_member
		edu_01
		health_01
		social_01
		bureau_03
		police_05
		#state_press
		#underground_parties_only
		#theocracy
		partial_draft_army
		no_women_in_military
		foreign_jihadis
		wahabi_ulema
		farmers
		#tribalism
	}
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 80 }
	add_to_array = { influence_array = ISI.id }
	add_to_array = { influence_array_val = 65 }
	add_to_array = { influence_array = AQY.id }
	add_to_array = { influence_array_val = 30 }
	add_to_array = { influence_array = SML.id }
	add_to_array = { influence_array_val = 10 }
	add_to_array = { influence_array = ERI.id }
	add_to_array = { influence_array_val = 21 }
	startup_influence = yes

	capital = 239
	oob = "SHB_2000"
	
	set_convoys = 20
}

2017.1.1 = {
	capital = 239

	oob = "SHB_2017"

	declare_war_on = {
		target = SOM
		type = civil_war
	}

	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		infiltration_assault = 1 
		frontline_defence = 1 
		early_tunnel_warfare = 1 
		guerilla_specialisation = 1
		
		infantry_weapons = 1
		infantry_weapons1 = 1
		
		command_control_equipment = 1
		
		land_Drone_equipment = 1
		
		Anti_tank_0 = 1
		
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		
		combat_eng_equipment = 1
		
		Early_APC = 1
		MBT_1 = 1
		
		IFV_1 = 1
		
		APC_1 = 1
		
		Air_APC_1 = 1
		
		Rec_tank_0 = 1
		
		util_vehicle_0 = 1
		
		SP_arty_0 = 1
		
		artillery_0 = 1
		Arty_upgrade_1 = 1
		artillery_1 = 1
		
		SP_R_arty_0 = 1
		
		early_helicopter = 1
		
		SP_Anti_Air_0 = 1
		
		ENGI_MBT_1 = 1
		
		early_fighter = 1
		
		L_Strike_fighter1 = 1
	   
		early_bomber = 1
		
		night_vision_1 = 1
	}

	set_convoys = 20

	add_ideas = {
		#pop_050
		paralyzing_corruption
		gdp_1
		sunni
		defence_09
		LoAS_member
		edu_01
		health_01
		social_01
		bureau_03
		police_05
		#state_press
		#underground_parties_only
		#theocracy
		partial_draft_army
		no_women_in_military
		foreign_jihadis
		wahabi_ulema
		farmers
		#tribalism
	}
	#set_country_flag = gdp_1
	set_country_flag = enthusiastic_wahabi_ulema
	set_country_flag = positive_The_Ulema

	#Nat focus
	complete_national_focus = bonus_tech_slots

	set_politics = {

		parties = {
			democratic = { 
				popularity = 10
			}

			fascism = {
				popularity = 50
			}
			
			communism = {
				popularity = 0
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 10
			}
		}
		
		ruling_party = fascism
		last_election = "2016.11.10"
		election_frequency = 48
		elections_allowed = no
	}

	create_country_leader = {
		name = "Ahmad Umar"
		desc = "POLITICS_Mohamed Cheikh Biadillah_DESC" 
		picture = "Ahmad_Umar.dds"
		ideology = Caliphate
		traits = {
			#
		}
	}
}