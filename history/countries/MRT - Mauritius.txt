﻿2000.1.1 = {
	add_ideas = {
		#pop_050
		widespread_corruption
		gdp_5
		christian
			stable_growth
			defence_01
		edu_02
		health_02
		social_02
		bureau_03
		police_01
		volunteer_army
		volunteer_women
		#hybrid
		#tax_cost_19
	}

	capital = 270
	oob = "MRT_2000"
	set_convoys = 20
	
	add_ideas = {
		gdp_5
	}
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 50 }
	add_to_array = { influence_array = FRA.id }
	add_to_array = { influence_array_val = 42 }
	add_to_array = { influence_array = CHI.id }
	add_to_array = { influence_array_val = 24 }
	add_to_array = { influence_array = RAJ.id }
	add_to_array = { influence_array_val = 35 }
	add_to_array = { influence_array = PAK.id }
	add_to_array = { influence_array_val = 10 }
	add_to_array = { influence_array = BAN.id }
	add_to_array = { influence_array_val = 7 }
	add_to_array = { influence_array = MAY.id }
	add_to_array = { influence_array_val = 10 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 5 }
	startup_influence = yes
	
	set_technology = { 
		#Basic Rifles
		infantry_weapons = 1
		
		
		#Needed for SPAA template
		Anti_Air_0 = 1

		
		#Needed for HAT and HIW
		Anti_tank_0 = 1

	}
	
	create_corps_commander = {
		name = "Karl Mario Nobin"
		picture = "Portrait_Karl_Mario_Nobin.dds"
		traits = { urban_assault_specialist }
		id = 42000
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	
}

2017.1.1 = {
	capital = 270
	oob = "MRT_2017"
	set_convoys = 20

	add_ideas = {
		#pop_050
		widespread_corruption
		gdp_5
		christian
			stable_growth
			defence_01
		edu_02
		health_02
		social_02
		bureau_03
		police_01
		volunteer_army
		volunteer_women
		#hybrid
		#tax_cost_19
	}
	
	#set_country_flag = gdp_5
	set_country_flag = negative_small_medium_business_owners
	set_country_flag = positive_landowners
	set_country_flag = positive_international_bankers
	
	set_variable = { var = debt value = 7 }
	set_variable = { var = treasury value = 5 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 19 }
	
	#set_variable = { var = size_modifier value = 0.08 } #1 CIC
	#initial_money_setup = yes

	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1

		#Basic Rifles
		infantry_weapons = 1
		
		
		#Needed for SPAA template
		Anti_Air_0 = 1

		
		#Needed for HAT and HIW
		Anti_tank_0 = 1

	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 15
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 15
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 70
			}
		}
		
		ruling_party = neutrality
		last_election = "2014.12.10"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Anerood Jugnauth"
		desc = "POLITICS_Anerood_Jugnauth_DESC"
		picture = "Anerood_Jugnauth.dds"
		expire = "2050.1.1"
		ideology = Neutral_conservatism
		traits = {
			#
		}
	}
	
}