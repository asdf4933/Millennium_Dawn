﻿2000.1.1 = {
	capital = 593
	oob = "SNA_2000"
	set_convoys = 20
	
	if = {
		limit = {
			has_start_date < 2000.1.2
		}
		declare_war_on = {
			target = SOM
			type = civil_war
		}
	}
	
	if = {
		limit = {
			has_start_date < 2000.1.2
		}
		declare_war_on = {
			target = SWS
			type = annex_everything
		}
	}
	
	if = {
		limit = {
			has_start_date < 2000.1.2
		}
		declare_war_on = {
			target = JUB
			type = annex_everything
		}
	}
	
	add_ideas = {
		#pop_050
		defence_09
	}
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 35 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 20 }
	add_to_array = { influence_array = ETH.id }
	add_to_array = { influence_array_val = 22 }
	add_to_array = { influence_array = SOM.id }
	add_to_array = { influence_array_val = 10 }
	add_to_array = { influence_array = TUR.id }
	add_to_array = { influence_array_val = 5 }
	add_to_array = { influence_array = CHI.id }
	add_to_array = { influence_array_val = 0 }
	startup_influence = yes
	
	set_technology = { 
		infantry_weapons = 1
		Anti_tank_0 = 1
		Anti_Air_0 = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 20
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 0
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 100
			}
		}
		
		ruling_party = neutrality
		last_election = "1996.8.3"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Hussein Farrah Aidid"
		desc = "" 
		picture = "SNA_Hussein_Farrah_Aidid.dds"
		ideology = Neutral_conservatism
		traits = {
			#
		}
	}
}

2017.1.1 = {
}
