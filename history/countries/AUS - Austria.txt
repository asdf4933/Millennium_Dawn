﻿2000.1.1 = {
	add_ideas = {
		gdp_9
		#tax_cost_43
		stable_growth
	}

	capital = 76
	oob = "AUS_2000"
	
	set_convoys = 30
	
	add_ideas = {
		gdp_8
		#tax_cost_44
		stagnation
		#pop_050
		The_Clergy
		small_medium_business_owners
		Labour_Unions
		christian
		modest_corruption
		EU_member
		stable_growth
		defence_01
		edu_05
		health_05
		social_06
		bureau_02
		police_03
		draft_army
		volunteer_women
		western_country
		large_far_right_movement
		labour_unions
		small_medium_business_owners
		landowners
		#civil_law
		the_euro
	}
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 176 }
	add_to_array = { influence_array = FRA.id }
	add_to_array = { influence_array_val = 2 }
	add_to_array = { influence_array = GER.id }
	add_to_array = { influence_array_val = 52 }
	add_to_array = { influence_array = ITA.id }
	add_to_array = { influence_array_val = 28 }
	add_to_array = { influence_array = CZE.id }
	add_to_array = { influence_array_val = 11 }
	add_to_array = { influence_array = HUN.id }
	add_to_array = { influence_array_val = 5 }
	add_to_array = { influence_array = SWI.id }
	add_to_array = { influence_array_val = 22 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 25 }
	startup_influence = yes
	
	set_variable = { var = debt value = 211.7 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 29.2 }
	set_variable = { var = tax_rate value = 44 }
	#initial_money_setup = yes
	
	
	add_opinion_modifier = { target = SWI modifier = german_speaking }
	reverse_add_opinion_modifier = { target = SWI modifier = german_speaking }
	
	add_opinion_modifier = { target = BEL modifier = EU_member_opinion }
	reverse_add_opinion_modifier = { target = BEL modifier = EU_member_opinion }
	add_opinion_modifier = { target = FRA modifier = EU_member_opinion }
	reverse_add_opinion_modifier = { target = FRA modifier = EU_member_opinion }
	
	clr_global_flag = sell_AUS_rec_2
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1

		Early_APC = 1
		
		IFV_1 = 1
		IFV_2 = 1
		
		Rec_tank_0 = 1
		Rec_tank_1 = 1
		Rec_tank_2 = 1

		infantry_weapons = 1
		infantry_weapons1 = 1
		infantry_weapons2 = 1
		
		#2005
		
		combat_eng_equipment = 1
		
		night_vision_1 = 1
		night_vision_2 = 1
		night_vision_3 = 1 #1985
		
		command_control_equipment = 1
		command_control_equipment1 = 1
		command_control_equipment2 = 1
		
		land_Drone_equipment = 1
		
		
		APC_1 = 1
		APC_2 = 1
		APC_3 = 1
		APC_4 = 1

		MBT_1 = 1
		
		ENGI_MBT_1 = 1
		
		util_vehicle_0 = 1
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		
		artillery_0 = 1
		SP_arty_0 = 1
		
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
	
		landing_craft = 1
		
		body_armor_1980 = 1
		body_armor_2000 = 1
		camouflage = 1
		camouflage2 = 1
		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 35
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 28
			}
			
			neutrality = {
				popularity = 9
			}
			
			nationalist = {
				popularity = 28
			}
		}
		
		ruling_party = democratic
		last_election = "1999.10.3"
		election_frequency = 48
		elections_allowed = yes
	}
	
	set_variable = { election_threshold = 0.04 }
	
	create_country_leader = {
		name = "Viktor Klima"
		picture = "Viktor_Klima.dds"
		expire = "2065.1.1"
		ideology = socialism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Jörg Haider"
		picture = ""
		expire = "2065.1.1"
		ideology = Nat_Populism
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Wolfgang Schüssel"
		picture = ""
		expire = "2065.1.1"
		ideology = Conservative
		traits = {
		
		}
	}
	create_country_leader = {
		name = "Alexander Van der Bellen"
		picture = ""
		expire = "2065.1.1"
		ideology = Neutral_green
		traits = {
		
		}
	}

	create_field_marshal = {
		name = "Othmar Commenda"
		picture = "Portrait_Othmar_Commenda.dds"
		traits = { old_guard organisational_leader }
		id = 3900
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "Horst Hofer"
		picture = "Portrait_Horst_Hofer.dds"
		traits = { commando }
		id = 3901
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Franz Reissner"
		picture = "Portrait_Franz_Reissner.dds"
		traits = { trickster }
		id = 3902
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Dieter Heidecker"
		picture = "Portrait_Dieter_Heidecker.dds"
		traits = { fortress_buster }
		id = 3903
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Othmar Wohlkönig"
		picture = "Portrait_Othmar_Wohlkoenig.dds"
		traits = {  }
		id = 3904
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Heinrich Winkelmayer"
		picture = "Portrait_Heinrich_Winkelmayer.dds"
		traits = { urban_assault_specialist }
		id = 3905
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Gerhard Christiner"
		picture = "Portrait_Gerhard_Christiner.dds"
		traits = { trait_engineer }
		id = 3906
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Christian Platzner"
		picture = "Portrait_Christian_Platzner.dds"
		traits = { trait_engineer }
		id = 3907
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Karl Gruber"
		picture = "Portrait_Karl_Gruber.dds"
		traits = { commando }
		id = 3908
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Hermann Kaponig"
		picture = "Portrait_Hermann_Kaponig.dds"
		traits = {  }
		id = 3909
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Rudolf Striedinger"
		picture = "Portrait_Rudolf_Striedinger.dds"
		traits = {  }
		id = 3910
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Edwin Potocnik"
		picture = "Portrait_Edwin_Potocnik.dds"
		traits = {  }
		id = 3911
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Christian Habersatter"
		picture = "Portrait_Christian_Habersatter.dds"
		traits = { panzer_leader }
		id = 3912
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Christian Riemer"
		picture = "Portrait_Christian_Riemer.dds"
		traits = { panzer_leader }
		id = 3913
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
		
	create_corps_commander = {
		name = "Peter Grünwald"
		picture = "Portrait_Peter_Gruenwald.dds"
		traits = { hill_fighter }
		id = 3914
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Jürgen Wörgötter"
		picture = "Portrait_Juergen_Woergoetter.dds"
		traits = { hill_fighter }
		id = 3915
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}


}

2017.1.1 = {
	capital = 76

	oob = "AUS_2017"
	
	set_variable = { var = debt value = 354 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = treasury value = 23.4 }
	set_variable = { var = tax_rate value = 43 }
	
	#set_variable = { var = size_modifier value = 1.18 } #8 CIC
	#initial_money_setup = yes
	
	set_technology = { 
		land_Drone_equipment1 = 1
		
		command_control_equipment3 = 1 #2005
		command_control_equipment4 = 1 #2015
		
		infantry_weapons3 = 1
		
		IFV_3 = 1
		IFV_4 = 1
		IFV_5 = 1
		
		APC_5 = 1

		body_armor_2010 = 1
		
	}

	remove_ideas = {
		gdp_8
		#tax_cost_44
		stagnation
	}
	
	add_ideas = {
		gdp_9
		#tax_cost_43
		stable_growth
	}
	
	set_country_flag = negative_labour_unions
	set_country_flag = positive_small_medium_business_owners
	set_country_flag = positive_landowners
	clr_global_flag = sell_AUS_rec_2

	#Nat focus
		complete_national_focus = bonus_tech_slots
		complete_national_focus = Generic_4K_GDPC_slot
		complete_national_focus = Generic_15K_GDPC_slot
		complete_national_focus = Generic_30K_GDPC_slot
		complete_national_focus = Generic_4_IC_slot
		complete_national_focus = Generic_8_IC_slot

		set_politics = {

		parties = {
			democratic = { 
				popularity = 63
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 4
			}
			
			neutrality = {
				popularity = 12
			}
			
			nationalist = {
				popularity = 21
			}
		}
		
		ruling_party = democratic
		last_election = "2013.9.29"
		election_frequency = 60
		elections_allowed = yes
	}


	set_convoys = 150


	create_country_leader = {
		name = "Reinhold Mitterlehner"
		desc = "POLITICS_WILLIAM_DUDLEY_PELLEY_DESC"
		picture = "reinhold_mitterlehner.dds"
		expire = "2065.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Matthias Strolz"
		picture = "matthias_strolz.dds"
		expire = "2065.1.1"
		ideology = liberalism
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Frank Stronach"
		picture = "frank_stronach.dds"
		expire = "2065.1.1"
		ideology = Conservative
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Eva Glawischnig"
		picture = "eva_glawischnig.dds"
		expire = "2065.1.1"
		ideology = Neutral_green
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Heinz-Christian Strache"
		picture = "heinz_christian_strache.dds"
		expire = "2065.1.1"
		ideology = Nat_Fascism
		traits = {
			#
		}
	}


	create_country_leader = {
		name = "Christian Kern"
		desc = "POLITICS_EARL_BROWDER_DESC"
		picture = "Christian_Kern.dds"
		expire = "2065.1.1"
		ideology = socialism
		traits = {
			#
		}
	}
	
}