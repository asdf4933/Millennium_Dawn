﻿2000.1.1 = {
	capital = 16
	oob = "WAS_2000"
	
	set_convoys = 5

	add_ideas = {
		#pop_050
		small_medium_business_owners
		Labour_Unions
		unrestrained_corruption
		gdp_7
		pluralist
		police_03
		bureau_02
		western_country
		large_far_right_movement
	}
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 40 }
	add_to_array = { influence_array = ENG.id }
	add_to_array = { influence_array_val = 60 }
	add_to_array = { influence_array = FRA.id }
	add_to_array = { influence_array_val = 5 }
	add_to_array = { influence_array = GER.id }
	add_to_array = { influence_array_val = 1 }
	startup_influence = yes
	
}
	
2017.1.1 = {
	capital = 16

	oob = "WAS_1936"

	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
	}

	#set_country_flag = gdp_7

	#Nat focus
		complete_national_focus = bonus_tech_slots
		complete_national_focus = Generic_4K_GDPC_slot
		complete_national_focus = Generic_15K_GDPC_slot


	set_convoys = 5


	set_politics = {

		parties = {
			democratic = { 
				popularity = 100
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 0
			}
			
			neutrality = {
				popularity = 0
			}
			
			nationalist = {
				popularity = 0
			}
		}
		
		ruling_party = democratic
		last_election = "2016.4.4"
		election_frequency = 5
		elections_allowed = yes
	}



}