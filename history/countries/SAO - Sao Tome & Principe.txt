﻿2000.1.1 = {
	add_ideas = {
		#pop_050
		systematic_corruption
		gdp_2
		christian
		stable_growth
		defence_01
		edu_02
		health_02
		social_01
		bureau_02
		police_01
		volunteer_army
		volunteer_women
		international_bankers
		farmers
		small_medium_business_owners
		#hybrid
		#tax_cost_23
	}

	capital = 320
	oob = "SAO_2000"
	
	set_convoys = 5
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		#For templates
		infantry_weapons = 1
		command_control_equipment = 1
		Anti_tank_0 = 1
		Anti_Air_0 = 1
	}
	
	add_ideas = {
		gdp_2
		#pop_050
		christian
		defence_01
		edu_02
		health_02
		social_01
		bureau_02
		police_01
		international_bankers
		farmers
		small_medium_business_owners
		#hybrid
		#tax_cost_23
	}
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 15 }
	add_to_array = { influence_array = POR.id }
	add_to_array = { influence_array_val = 42 }
	add_to_array = { influence_array = BRA.id }
	add_to_array = { influence_array_val = 34 }
	add_to_array = { influence_array = CHI.id }
	add_to_array = { influence_array_val = 18 }
	add_to_array = { influence_array = TAI.id }
	add_to_array = { influence_array_val = 10 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 5 }
	add_to_array = { influence_array = RAJ.id }
	add_to_array = { influence_array_val = 11 }
	add_to_array = { influence_array = FRA.id }
	add_to_array = { influence_array_val = 2 }
	startup_influence = yes
	
	set_country_flag = positive_international_bankers
	set_country_flag = negative_farmers
	set_country_flag = negative_small_medium_business_owners
	
	set_variable = { var = debt value = 0 }
	set_variable = { var = treasury value = 0 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 23 }
	#initial_money_setup = yes

}

2017.1.1 = {
	capital = 320
	oob = "SAO_2017"
	set_convoys = 5
	
	add_ideas = {
		#pop_050
		systematic_corruption
		gdp_2
		christian
		stable_growth
		defence_01
		edu_02
		health_02
		social_01
		bureau_02
		police_01
		volunteer_army
		volunteer_women
		international_bankers
		farmers
		small_medium_business_owners
		#hybrid
		#tax_cost_23
	}
	#set_country_flag = gdp_2
	set_country_flag = positive_international_bankers
	set_country_flag = negative_farmers
	set_country_flag = negative_small_medium_business_owners
	
	set_variable = { var = debt value = 0 }
	set_variable = { var = treasury value = 0 }
	set_variable = { var = int_investments value = 0 }
	set_variable = { var = tax_rate value = 23 }
	
	#set_variable = { var = size_modifier value = 0.03 } #0 CIC
	#initial_money_setup = yes
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	
	remove_opinion_modifier = { target = TAI modifier = recognize_taiwan }
	remove_opinion_modifier = { target = CHI modifier = recognize_taiwan_chi }
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 10
			}

			fascism = {
				popularity = 0
			}
			
			communism = {
				popularity = 5
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 85
			}
		}
		
		ruling_party = neutrality
		last_election = "2016.7.17"
		election_frequency = 60
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Patrice Trovoada"
		desc = "POLITICS_Mohamed_Cheikh Biadillah_DESC" 
		picture = "SAO_Patrice_Trovoada.dds"
		expire = "2050.1.1"
		ideology = Neutral_Libertarian
		traits = {
			#
		}
	}


}