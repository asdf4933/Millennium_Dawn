﻿2000.1.1 = {
	add_ideas = {
		#pop_050
		rampant_corruption
		gdp_4
		shia
		rentier_state
		export_economy
		fast_growth
		defence_03
		edu_04
		health_02
		social_03
		bureau_01
		police_01
		#state_press
		#parties_harassment ###Opposition allowed to cabinet, but not to clerical rule itself
		#state_religion ###Should be #theocracy, remove salafist requirement?
		draft_army
		volunteer_women
		irgc
		fossil_fuel_industry
		The_Ulema
		intervention_regional_interventionism
		#hybrid
		#tax_cost_18
		multi_ethnic_state_idea
	}

	capital = 405
	oob = "PER_2000"
	if = {
		limit = {
			has_dlc = "Man the Guns"
		}
		set_naval_oob = "PER_2000_naval_mtg"
		else = {
			set_naval_oob = "PER_2000_naval_legacy"
		}
	}
	set_convoys = 150
	
	add_ideas = {
		gdp_5
		#pop_050
		shia
		defence_03
		edu_04
		health_02
		social_03
		bureau_01
		police_01
		irgc
		fossil_fuel_industry
		The_Ulema
		#hybrid
		#tax_cost_18
		multi_ethnic_state_idea
	}
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 402 }
	add_to_array = { influence_array = PAK.id }
	add_to_array = { influence_array_val = 4 }
	add_to_array = { influence_array = SOV.id }
	add_to_array = { influence_array_val = 42 }
	add_to_array = { influence_array = CHI.id }
	add_to_array = { influence_array_val = 5 }
	add_to_array = { influence_array = RAJ.id }
	add_to_array = { influence_array_val = 31 }
	add_to_array = { influence_array = FRA.id }
	add_to_array = { influence_array_val = 2 }
	startup_influence = yes

	set_variable = { var = debt value = 64 }
	set_variable = { var = treasury value = 18.6 }
	set_variable = { var = tax_rate value = 18 }
	set_variable = { var = int_investments value = 0 }
	#initial_money_setup = yes
	
	set_global_flag = sell_PER_H_anti_tank_1
	
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		night_vision_1 = 1
		night_vision_2 = 1
		
		infantry_weapons = 1
		infantry_weapons1 = 1
		
		combat_eng_equipment = 1

		command_control_equipment = 1
		command_control_equipment1 = 1
		command_control_equipment2 = 1

		land_Drone_equipment = 1
		land_Drone_equipment1 = 1

		#Tosan
		Rec_tank_0 = 1

		#Zulfiqar
		MBT_1 = 1
		MBT_2 = 1
		MBT_3 = 1

		#Aras
		util_vehicle_0 = 1
		
		#DIO Boragh
		IFV_1 = 1
		
		Early_APC = 1
		
		#Sarir APC
		APC_1 = 1
		
		#Raad-2 and Fajr-5
		artillery_0 = 1
		Arty_upgrade_1 = 1
		SP_arty_0 = 1
		SP_R_arty_0 = 1
		
		SP_arty_1 = 1
		SP_R_arty_1 = 1
		
		#Dehlavie AT (and Nafez)
		Anti_tank_0 = 1
		Heavy_Anti_tank_0 = 1
		AT_upgrade_1 = 1
		Anti_tank_1 = 1
		Heavy_Anti_tank_1 = 1

		#Misagh-2 and Bavar-373
		Anti_Air_0 = 1
		SP_Anti_Air_0 = 1
		

		#Saeqeh
		early_fighter = 1
		MR_Fighter1 = 1

		#IAIO Toufan
		early_helicopter = 1
		attack_helicopter1 = 1
		transport_helicopter1 = 1
		
		early_bomber = 1
		
		transport_plane1 = 1
		transport_plane2 = 1 #Iran-140 Faraz
		
		naval_plane1 = 1
		
		#Moudge-Class
		corvette_1 = 1
		
		#Besat-Class
		submarine_1 = 1
		diesel_attack_submarine_1 = 1
		
		#Alvand-Class
		frigate_1 = 1
		frigate_2 = 1
		
		landing_craft = 1
		
		ENGI_MBT_1 = 1	
		
		#USED TO STOP CRASHING
		diesel_attack_submarine_2 = 1
		diesel_attack_submarine_3 = 1
		
		body_armor_1980 = 1
		camouflage = 1
		camouflage2 = 1
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 4
			}

			fascism = {
				popularity = 2
			}
			
			communism = {
				popularity = 87
			}
			
			neutrality = { 
				popularity = 7
			}
		}
		
		ruling_party = communism
		last_election = "1997.6.8"
		election_frequency = 48
		elections_allowed = yes
	}
	add_opinion_modifier = { target = SYR modifier = resistance_axis }
	add_opinion_modifier = { target = HEZ modifier = resistance_axis }
	
	add_opinion_modifier = { target = SAU modifier = strategic_rivals }
	reverse_add_opinion_modifier = { target = SAU modifier = strategic_rivals }
	add_opinion_modifier = { target = UAE modifier = large_commercial_relations }
	reverse_add_opinion_modifier = { target = UAE modifier = large_commercial_relations }
	
	add_opinion_modifier = { target = GER modifier = large_commercial_relations }
	add_opinion_modifier = { target = ITA modifier = large_commercial_relations }
	add_opinion_modifier = { target = KOR modifier = large_commercial_relations }
	add_opinion_modifier = { target = RAJ modifier = large_commercial_relations }
	
	add_opinion_modifier = { target = USA modifier = uss_vincennes }
	add_opinion_modifier = { target = USA modifier = operation_praying_mantis }
	add_opinion_modifier = { target = USA modifier = cia_coup_mossadeq }
	add_opinion_modifier = { target = USA modifier = helped_iraqi_chemical_weapons }
	add_opinion_modifier = { target = USA modifier = no_diplomatic_ties }
	reverse_add_opinion_modifier = { target = USA modifier = no_diplomatic_ties }
	reverse_add_opinion_modifier = { target = USA modifier = iran_hostage_crisis }
	reverse_add_opinion_modifier = { target = USA modifier = beirut_bombing }
	
	add_opinion_modifier = { target = ISR modifier = no_diplomatic_ties }
	reverse_add_opinion_modifier = { target = ISR modifier = no_diplomatic_ties }
	
	add_opinion_modifier = { target = FRA modifier = helped_iraqi_chemical_weapons }
	add_opinion_modifier = { target = HOL modifier = helped_iraqi_chemical_weapons }
	add_opinion_modifier = { target = ENG modifier = helped_iraqi_chemical_weapons }
	add_opinion_modifier = { target = GER modifier = helped_iraqi_chemical_weapons }
	add_opinion_modifier = { target = IRQ modifier = iraqi_chemical_weapons }
	
	add_opinion_modifier = { target = SAU modifier = oppresses_shiites }
	add_opinion_modifier = { target = BHR modifier = oppresses_shiites }
	add_opinion_modifier = { target = IRQ modifier = oppresses_shiites }
	add_opinion_modifier = { target = PAK modifier = oppresses_shiites }
	reverse_add_opinion_modifier = { target = PAK modifier = oppresses_sunni }
	reverse_add_opinion_modifier = { target = SAU modifier = oppresses_sunni }
	reverse_add_opinion_modifier = { target = BHR modifier = oppresses_sunni }
	reverse_add_opinion_modifier = { target = IRQ modifier = oppresses_sunni }
	reverse_add_opinion_modifier = { target = KUW modifier = oppresses_sunni }
	reverse_add_opinion_modifier = { target = QAT modifier = oppresses_sunni }
	reverse_add_opinion_modifier = { target = EGY modifier = oppresses_sunni }
	reverse_add_opinion_modifier = { target = JOR modifier = oppresses_sunni }
	
	add_opinion_modifier = { target = RAJ modifier = historic_friends }
	reverse_add_opinion_modifier = { target = RAJ modifier = historic_friends }
	

	#Leader of Green Movement, in house arrest
	create_country_leader = {
		name = "Mir-Hossein Mousavi"
		desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
		picture = "Neutral_Mir-Hossein_Mousavi.dds"
		expire = "2065.1.1"
		ideology = Neutral_conservatism
		traits = {
			writer
			neutrality_Neutral_conservatism
			capable
			stubborn
			honest
		}
	}

	#Son of the departed Shah
	create_country_leader = {
		name = "Shah Reza Pahlavi"
		desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
		picture = "Western_King_Reza_Pahlavi.dds"
		expire = "2065.1.1"
		ideology = liberalism
		traits = {
			king
			western_liberalism
			inexperienced
		}
	}

	#MEK leader, should really be leftist pro-western authoritarian
	create_country_leader = {
		name = "Maryam Rajavi"
		desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
		picture = "Western_Maryam_Rajavi.dds"
		expire = "2065.1.1"
		ideology = Western_Autocracy
		traits = {
			guerrilla_leader
			western_Western_Autocracy
			pro_american
			emotional
			deceitful
		}
	}

	#IRCG 
	create_country_leader = {
		name = "Qasem Soleimani"
		picture = "IRCG_Gen_Qasem_Soleimani.dds"
		expire = "2065.1.1"
		ideology = Vilayat_e_Faqih
		traits = {
			guerrilla_leader
			emerging_Vilayat_e_Faqih
			IRGC_Member
			geopolitical_thinker
			sly
			stubborn
		}
	}

	create_country_leader = {
		name = "Ayatollah Khamenei"
		picture = "Ayatollahi_Khamenei.dds"
		expire = "2030.1.1"
		ideology = Vilayat_e_Faqih
		traits = {
			cleric
			emerging_Vilayat_e_Faqih
			geopolitical_thinker
			political_dancer
			stubborn
		}
	}
	create_country_leader = {
		name = "Mahmoud Ahmadinejad"
		picture = "ahmadinejad.dds"
		expire = "2016.1.1"
		ideology = Vilayat_e_Faqih
		traits = {
			emerging_Vilayat_e_Faqih
			IRGC_Member
			likeable
			rash
			zealous
		}
	}

	create_country_leader = {
		name = "Mohammad Khatami"
		picture = "Mohammad_Khatami.dds"
		expire = "2065.1.1"
		ideology = Mod_Vilayat_e_Faqih
		traits = {
			emerging_Mod_Vilayat_e_Faqih
		}
	}
	
	create_field_marshal = {
		name = "Ahmadreza Pourdastan"
			picture = "Field_Marshall_Army_Ahmadreza_Pourdastan.dds"
		traits = { defensive_doctrine }
		id = 47700
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3

	}

	create_field_marshal = {
		name = "Mohammed Bagheri"
			picture = "IRCG_Field_Marshall_Mohammed_Bagheri.dds"
		traits = { inspirational_leader }
		id = 47701
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2

	}

	create_corps_commander = {
		name = "Ali Hajilo"
		picture = "Army_Gen_Ali_Hajilo.dds"
		traits = { desert_fox trait_mountaineer }
		id = 47702
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Hassan Saadi"
		picture = "Army_Gen_Hassan_Saadi.dds"
		traits = { panzer_leader desert_fox }
		id = 47703
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "Heshmatollah Malekian"
		picture = "Army_Gen_Heshmatollah_Malekian.dds"
		traits = { trait_mountaineer }
		id = 47704
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Kiomars Sharafi"
		picture = "Army_Gen_Kiomars_Sharafi.dds"
		traits = { trait_mountaineer }
		id = 47705
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Manoucher Kazemi"
		picture = "Army_Gen_Manoucher_Kazemi.dds"
		traits = { hill_fighter desert_fox }
		id = 47706
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Nader Najafi"
		picture = "Army_Gen_Nader_Najafi.dds"
		traits = { panzer_leader }
		id = 47707
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Abdol-Ali Najafi"
		picture = "IRCG_Gen_Abdol-Ali_Najafi.dds"
		traits = { commando desert_fox }
		id = 47708
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Ali Shadmani"
		picture = "IRCG_Gen_Ali_Shadmani.dds"
		traits = { commando hill_fighter }
		id = 47709
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Mohammad Pakpour"
		picture = "IRCG_Gen_Mohammad_Pakpour.dds"
		traits = { commando desert_fox hill_fighter }
		id = 47710
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 4
		logistics_skill = 4
	}

	create_corps_commander = {
		name = "Ali Jafari"
		picture = "IRCG_Gen_Mohammed_Ali_Jafari.dds"
		traits = { commando urban_assault_specialist }
		id = 47711
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Mohammed-Nazer Azimi"
		picture = "IRCG_Gen_Mohammed-Nazer_Azimi.dds"
		traits = { commando trickster }
		id = 47712
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Mostafa Izadi"
		picture = "IRCG_Gen_Mostafa_Izadi.dds"
		traits = { commando trait_engineer }
		id = 47713
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}

	create_corps_commander = {
		name = "Qasem Soleimani"
		picture = "IRCG_Gen_Qasem_Soleimani.dds"
		traits = { commando fortress_buster trickster urban_assault_specialist }
		id = 47714
		skill = 5
		attack_skill = 5
		defense_skill = 5
		planning_skill = 5
		logistics_skill = 5
	}

	create_corps_commander = {
		name = "Yahya Rahim Safavi"
		picture = "IRCG_Gen_Yahya_Rahim_Safavi.dds"
		traits = { commando fortress_buster swamp_fox }
		id = 47715
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}


	create_navy_leader = {
		name = "Ali Fadavi"
			picture = "IRCG_Navy_Ali_Fadavi.dds"
		traits = { blockade_runner }
		id = 47716
	}

	create_navy_leader = {
		name = "Habibollah Sayyari"
			picture = "navy_Habibollah_Sayyari.dds"
		traits = { seawolf }
		id = 47717
	}
	create_navy_leader = {
		name = "Ali Shamkani"
		picture = "Portrait_Ali_Shamkhani.dds"
		traits = { seawolf }
		id = 47718
	}
}

2017.1.1 = {
	capital = 405
	oob = "PER_2017"
	if = {
		limit = {
			has_dlc = "Man the Guns"
		}
		set_naval_oob = "PER_2017_naval_mtg"
		else = {
			set_naval_oob = "PER_2017_naval_legacy"
		}
	}
	
	# Starting tech
	set_technology = { 
		diesel_attack_submarine_2 = 1
		diesel_attack_submarine_3 = 1
		diesel_attack_submarine_4 = 1
		
		#Khalije Fars-Class
		destroyer_1 = 1
		destroyer_2 = 1
		missile_destroyer_1 = 1
		
		corvette_2 = 1
		missile_corvette_1 = 1
		
		attack_helicopter2 = 1
		
		#Kowsar
		L_Strike_fighter1 = 1
		L_Strike_fighter2 = 1
		
		MR_Fighter2 = 1
		
		AA_upgrade_1 = 1
		Anti_Air_1 = 1
		SP_Anti_Air_1 = 1
		AA_upgrade_3 = 1
		Anti_Air_2 = 1
		SP_Anti_Air_2 = 1
		
		AT_upgrade_2 = 1
		Anti_tank_2 = 1
		Heavy_Anti_tank_2 = 1
		
		SP_arty_2 = 1
		SP_R_arty_2 = 1
		
		artillery_1 = 1
		Arty_upgrade_3 = 1
		
		util_vehicle_1 = 1
		util_vehicle_2 = 1
		util_vehicle_3 = 1
		
		IFV_2 = 1
		APC_2 = 1
		APC_3 = 1
		
		land_Drone_equipment2 = 1 #2005
		
		infantry_weapons2 = 1
		infantry_weapons3 = 1
		
		night_vision_3 = 1
		
		command_control_equipment3 = 1
		
		Rec_tank_1 = 1
		
		Air_UAV1 = 1
		
		body_armor_2000 = 1
		
	}

	add_ideas = {
		#pop_050
		rampant_corruption
		gdp_4
		shia
		rentier_state
		export_economy
		fast_growth
		defence_03
		edu_04
		health_02
		social_03
		bureau_01
		police_01
		#state_press
		#parties_harassment ###Opposition allowed to cabinet, but not to clerical rule itself
		#state_religion ###Should be #theocracy, remove salafist requirement?
		draft_army
		volunteer_women
		irgc
		fossil_fuel_industry
		The_Ulema
		intervention_regional_interventionism
		#hybrid
		#tax_cost_18
		multi_ethnic_state_idea
	}
	
	set_variable = { var = debt value = 177  }
	set_variable = { var = treasury value = 134 }
	set_variable = { var = int_investments value = 91 }
	set_variable = { var = tax_rate value = 18 }
	
	#set_variable = { var = size_modifier value = 2.50 } #14 CIC
	#initial_money_setup = yes
	
	set_global_flag = sell_PER_H_anti_tank_2
	
	#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	complete_national_focus = Generic_30_IC_slot

	create_faction = RESISTANCE_AXIS
	add_to_faction = PER
	add_to_faction = HEZ

	diplomatic_relation = {
		country = SYR
		relation = military_access
		active = yes
	}
	
	diplomatic_relation = {
		country = HEZ
		relation = military_access
		active = yes
	}
	
	diplomatic_relation = {
		country = HOU
		relation = military_access
		active = yes
	}
	
	diplomatic_relation = {
		country = IRQ
		relation = military_access
		active = yes
	}
	
	diplomatic_relation = {
		country = KUR
		relation = military_access
		active = yes
	}

	add_opinion_modifier = { target = IRQ modifier = resistance_axis }
	
	remove_opinion_modifier = { target = IRQ modifier = oppresses_shiites }
	remove_opinion_modifier = { target = IRQ modifier = iraqi_chemical_weapons }
	
	add_opinion_modifier = { target = KUR modifier = PER_Dont_Support_Kurdish_Independence }
	add_opinion_modifier = { target = ROJ modifier = PER_Dont_Support_Kurdish_Independence }
	
	add_opinion_modifier = { target = SAU modifier = hajj_massacre }
	
	add_opinion_modifier = { target = CHI modifier = large_commercial_relations }

	set_politics = {

		parties = {
			democratic = { 
				popularity = 4
			}

			fascism = {
				popularity = 2
			}
			
			communism = {
				popularity = 77
			}
			
			neutrality = { 
				popularity = 17
			}
		}
		
		ruling_party = communism
		last_election = "2013.6.14"
		election_frequency = 48
		elections_allowed = yes
	}

	#Reformist, current foreign minister
	create_country_leader = {
		name = "Mohammad Javad Zarif"
		desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
		picture = "Mohammad_Javad_Zarif.dds"
		expire = "2065.1.1"
		ideology = Mod_Vilayat_e_Faqih
		traits = {
			scientist
			emerging_Mod_Vilayat_e_Faqih
			cautious
			stubborn
			political_dancer
		}
	}

	#Close to the IRCG, popular as major of Tehran
	create_country_leader = {
		name = "Mohammad Bagher Ghalibaf"
		desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
		picture = "Mohammad_Bagher_Ghalibaf.dds"
		expire = "2065.1.1"
		ideology = Vilayat_e_Faqih
		traits = {
			guerrilla_leader
			emerging_Vilayat_e_Faqih
			IRGC_Member
		}
	}

	#IRCG 
	create_country_leader = {
		name = "Ali Akbar Velayati"
		desc = "POLITICS_REZA_SHAH_PAHLAVI_DESC"
		picture = "IRCG_cand_Ali_Akbar_Velayati.dds"
		expire = "2065.1.1"
		ideology = Vilayat_e_Faqih
		traits = {
			doctor
			emerging_Vilayat_e_Faqih
			IRGC_Member
		}
	}

	create_country_leader = {
		name = "Hassan Rouhani"
		picture = "Hassan_Rouhani.dds"
		expire = "2065.1.1"
		ideology = Mod_Vilayat_e_Faqih
		traits = {
			lawyer
			emerging_Mod_Vilayat_e_Faqih
			capable
			humble
		}
	}
}