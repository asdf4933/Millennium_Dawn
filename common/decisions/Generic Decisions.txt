GEN_political_decisions = {
	
	GEN_remove_saudi_aid = {

		icon = break_treaty

		available = {
			has_idea = saudi_aid
		}

		fire_only_once = yes
		
		ai_will_do = {
			factor = 0
		}

		visible = {
			has_idea = saudi_aid
		}
		
		complete_effect = {
			add_political_power = -100
			add_stability = -0.05
			remove_ideas = saudi_aid
			SAU = { add_opinion_modifier = { target = ROOT modifier = GEN_banned_saudi_mosques } }
			}
		}

	GEN_crack_down_on_muslim_broterhood = {

		icon = oppression

		available = {
			has_idea = al_jazeera_banned
		}

		fire_only_once = yes
		
		ai_will_do = {
			factor = 0
		}

		visible = {
			has_idea = al_jazeera_banned
			NOT = { 
				has_idea = muslim_brotherhood_crackdown 
			}
		}
		
		complete_effect = {
			add_political_power = -50
			add_ideas = muslim_brotherhood_crackdown
			QAT = { add_opinion_modifier = { target = ROOT modifier = Is_cracking_down_on_Muslim_Brotherhood } }
			TUN = { add_opinion_modifier = { target = ROOT modifier = Is_cracking_down_on_Muslim_Brotherhood } }
		}
	}
	
	GEN_stop_crackdowns = {

		icon = operation

		available = {
		has_idea = muslim_brotherhood_crackdown
		}

		fire_only_once = yes
		
		ai_will_do = {
			factor = 0
		}

		visible = {
			has_idea = muslim_brotherhood_crackdown	
		}
		
		complete_effect = {
			remove_ideas = muslim_brotherhood_crackdown
			QAT = { add_opinion_modifier = { target = ROOT modifier = GEN_stopped_crackdown } }
			TUN = { add_opinion_modifier = { target = ROOT modifier = GEN_stopped_crackdown } }
		}
	}

	GEN_ban_al_jazeera = {

		icon = oppression

		available = {
			has_idea = al_jazeera_allowed
		}

		fire_only_once = yes
		
		ai_will_do = {
			factor = 0
		}

		visible = {
			has_idea = al_jazeera_allowed	
		}
		
		complete_effect = {
		QAT = { add_opinion_modifier = { target = ROOT modifier = GEN_banned_al_jazeera } }
		add_political_power = -25
			swap_ideas = {
				remove_idea = al_jazeera_allowed
				add_idea = al_jazeera_banned
			}
		}
	}
	
	GEN_allow_al_jazeera = {

		icon = decision

		available = {
			has_idea = al_jazeera_banned
		}

		fire_only_once = yes
		
		ai_will_do = {
			factor = 0
		}

		visible = {
			has_idea = al_jazeera_banned	
		}
		
		complete_effect = {
		QAT = { add_opinion_modifier = { target = ROOT modifier = GEN_allowed_al_jazeera } }
			swap_ideas = {
				remove_idea = al_jazeera_banned
				add_idea = al_jazeera_allowed
			}
		}
	}
}