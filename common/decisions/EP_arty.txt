#written by ahmex the decision master

EP_arty_cat = {
	
	#Infantry Decision Category
	
	EP_arty_close = { #closes infantry tab
	
		icon = GFX_decision_generic_prepare_civil_war
	
		visible = {
			has_country_flag = arty_cat_open
		}
		
		available = {
			NOT = { has_country_flag = arty_order_active }
		}
		
		complete_effect = {
			clr_country_flag = arty_cat_open
		}
	}
	
	#artillery
	
	EP_artillery_open = { #will lead to country selection for small arms
	
		icon = GFX_decision_generic_prepare_civil_war
	
		visible = {
			has_country_flag = arty_cat_open
			hidden_trigger = {
				NOT = { has_country_flag = show_arty }
			}
		}
		
		complete_effect = {
			set_country_flag = show_arty
		}
	}
	
	EP_artillery_close = { #cant close it if one your orders havent delivered yet
	
		icon = GFX_decision_generic_prepare_civil_war
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
		}
		
		available = {
			NOT = { has_country_flag = arty_order_active }
		}
		
		complete_effect = {
			clr_country_flag = show_arty
		}
	}
	
	#China
	
	EP_buy_CHI_artillery_1 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = CHI
			has_global_flag = sell_CHI_arty_1
			ROOT = {
				NOT = {
					tag = CHI
				}
			}
		}
		
		icon = china
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.1 }
		}
		custom_cost_text = cost_0_1
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.1 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_1
				amount = 1200
				producer = CHI
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_CHI_artillery_2 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = CHI
			has_global_flag = sell_CHI_arty_2
			ROOT = {
				NOT = {
					tag = CHI
				}
			}
		}
		
		icon = china
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.125 }
		}
		custom_cost_text = cost_0_125
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.125 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_2
				amount = 1200
				producer = CHI
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_CHI_artillery_3 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = CHI
			has_global_flag = sell_CHI_arty_3
			ROOT = {
				NOT = {
					tag = CHI
				}
			}
		}
		
		icon = china
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.15 }
		}
		custom_cost_text = cost_0_15
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.15 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_3
				amount = 1200
				producer = CHI
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_CHI_artillery_4 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = CHI
			has_global_flag = sell_CHI_arty_4
			ROOT = {
				NOT = {
					tag = CHI
				}
			}
		}
		
		icon = china
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.175 }
		}
		custom_cost_text = cost_0_175
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.175 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_4
				amount = 1200
				producer = CHI
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	#Finland
	
	EP_buy_FIN_artillery_1 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = FIN
			has_global_flag = sell_FIN_arty_1
			ROOT = {
				NOT = {
					tag = FIN
				}
			}
		}
		
		icon = finland
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.1 }
		}
		custom_cost_text = cost_0_1
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.1 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_1
				amount = 1200
				producer = FIN
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_FIN_artillery_2 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = FIN
			has_global_flag = sell_FIN_arty_2
			ROOT = {
				NOT = {
					tag = FIN
				}
			}
		}
		
		icon = finland
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.125 }
		}
		custom_cost_text = cost_0_125
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.125 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_2
				amount = 1200
				producer = FIN
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_FIN_artillery_3 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = FIN
			has_global_flag = sell_FIN_arty_3
			ROOT = {
				NOT = {
					tag = FIN
				}
			}
		}
		
		icon = finland
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.15 }
		}
		custom_cost_text = cost_0_15
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.15 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_3
				amount = 1200
				producer = FIN
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_FIN_artillery_4 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = FIN
			has_global_flag = sell_FIN_arty_4
			ROOT = {
				NOT = {
					tag = FIN
				}
			}
		}
		
		icon = finland
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.175 }
		}
		custom_cost_text = cost_0_175
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.175 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_4
				amount = 1200
				producer = FIN
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	#France
	
	EP_buy_FRA_artillery_1 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = FRA
			has_global_flag = sell_FRA_arty_1
			ROOT = {
				NOT = {
					tag = FRA
				}
			}
		}
		
		icon = france
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.1 }
		}
		custom_cost_text = cost_0_1
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.1 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_1
				amount = 1200
				producer = FRA
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_FRA_artillery_2 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = FRA
			has_global_flag = sell_FRA_arty_2
			ROOT = {
				NOT = {
					tag = FRA
				}
			}
		}
		
		icon = france
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.125 }
		}
		custom_cost_text = cost_0_125
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.125 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_2
				amount = 1200
				producer = FRA
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_FRA_artillery_3 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = FRA
			has_global_flag = sell_FRA_arty_3
			ROOT = {
				NOT = {
					tag = FRA
				}
			}
		}
		
		icon = france
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.15 }
		}
		custom_cost_text = cost_0_15
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.15 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_3
				amount = 1200
				producer = FRA
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_FRA_artillery_4 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = FRA
			has_global_flag = sell_FRA_arty_4
			ROOT = {
				NOT = {
					tag = FRA
				}
			}
		}
		
		icon = france
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.175 }
		}
		custom_cost_text = cost_0_175
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.175 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_4
				amount = 1200
				producer = FRA
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	#Israel
	
	EP_buy_ISR_artillery_1 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = ISR
			has_global_flag = sell_ISR_arty_1
			ROOT = {
				NOT = {
					tag = ISR
				}
			}
		}
		
		icon = israel
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.1 }
		}
		custom_cost_text = cost_0_1
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.1 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_1
				amount = 1200
				producer = ISR
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_ISR_artillery_2 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = ISR
			has_global_flag = sell_ISR_arty_2
			ROOT = {
				NOT = {
					tag = ISR
				}
			}
		}
		
		icon = israel
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.125 }
		}
		custom_cost_text = cost_0_125
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.125 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_2
				amount = 1200
				producer = ISR
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_ISR_artillery_3 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = ISR
			has_global_flag = sell_ISR_arty_3
			ROOT = {
				NOT = {
					tag = ISR
				}
			}
		}
		
		icon = israel
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.15 }
		}
		custom_cost_text = cost_0_15
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.15 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_3
				amount = 1200
				producer = ISR
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_ISR_artillery_4 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = ISR
			has_global_flag = sell_ISR_arty_4
			ROOT = {
				NOT = {
					tag = ISR
				}
			}
		}
		
		icon = israel
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.175 }
		}
		custom_cost_text = cost_0_175
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.175 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_4
				amount = 1200
				producer = ISR
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	#South Africa
	
	EP_buy_SAF_artillery_1 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SAF
			has_global_flag = sell_SAF_arty_1
			ROOT = {
				NOT = {
					tag = SAF
				}
			}
		}
		
		icon = south_africa
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.1 }
		}
		custom_cost_text = cost_0_1
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.1 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_1
				amount = 1200
				producer = SAF
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_SAF_artillery_2 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SAF
			has_global_flag = sell_SAF_arty_2
			ROOT = {
				NOT = {
					tag = SAF
				}
			}
		}
		
		icon = south_africa
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.125 }
		}
		custom_cost_text = cost_0_125
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.125 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_2
				amount = 1200
				producer = SAF
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_SAF_artillery_3 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SAF
			has_global_flag = sell_SAF_arty_3
			ROOT = {
				NOT = {
					tag = SAF
				}
			}
		}
		
		icon = south_africa
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.15 }
		}
		custom_cost_text = cost_0_15
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.15 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_3
				amount = 1200
				producer = SAF
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_SAF_artillery_4 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SAF
			has_global_flag = sell_SAF_arty_4
			ROOT = {
				NOT = {
					tag = SAF
				}
			}
		}
		
		icon = south_africa
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.175 }
		}
		custom_cost_text = cost_0_175
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.175 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_4
				amount = 1200
				producer = SAF
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	#Serbia
	
	EP_buy_SER_artillery_1 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SER
			has_global_flag = sell_SER_arty_1
			ROOT = {
				NOT = {
					tag = SER
				}
			}
		}
		
		icon = serbia
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.1 }
		}
		custom_cost_text = cost_0_1
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.1 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_1
				amount = 1200
				producer = SER
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_SER_artillery_2 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SER
			has_global_flag = sell_SER_arty_2
			ROOT = {
				NOT = {
					tag = SER
				}
			}
		}
		
		icon = serbia
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.125 }
		}
		custom_cost_text = cost_0_125
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.125 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_2
				amount = 1200
				producer = SER
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_SER_artillery_3 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SER
			has_global_flag = sell_SER_arty_3
			ROOT = {
				NOT = {
					tag = SER
				}
			}
		}
		
		icon = serbia
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.15 }
		}
		custom_cost_text = cost_0_15
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.15 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_3
				amount = 1200
				producer = SER
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_SER_artillery_4 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SER
			has_global_flag = sell_SER_arty_4
			ROOT = {
				NOT = {
					tag = SER
				}
			}
		}
		
		icon = serbia
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.175 }
		}
		custom_cost_text = cost_0_175
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.175 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_4
				amount = 1200
				producer = SER
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	#Singapore
	
	EP_buy_SIN_artillery_1 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SIN
			has_global_flag = sell_SIN_arty_1
			ROOT = {
				NOT = {
					tag = SIN
				}
			}
		}
		
		icon = singapore
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.1 }
		}
		custom_cost_text = cost_0_1
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.1 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_1
				amount = 1200
				producer = SIN
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_SIN_artillery_2 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SIN
			has_global_flag = sell_SIN_arty_2
			ROOT = {
				NOT = {
					tag = SIN
				}
			}
		}
		
		icon = singapore
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.125 }
		}
		custom_cost_text = cost_0_125
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.125 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_2
				amount = 1200
				producer = SIN
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_SIN_artillery_3 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SIN
			has_global_flag = sell_SIN_arty_3
			ROOT = {
				NOT = {
					tag = SIN
				}
			}
		}
		
		icon = singapore
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.15 }
		}
		custom_cost_text = cost_0_15
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.15 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_3
				amount = 1200
				producer = SIN
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_SIN_artillery_4 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = SIN
			has_global_flag = sell_SIN_arty_4
			ROOT = {
				NOT = {
					tag = SIN
				}
			}
		}
		
		icon = singapore
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.175 }
		}
		custom_cost_text = cost_0_175
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.175 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_4
				amount = 1200
				producer = SIN
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	#US of A
	
	EP_buy_USA_artillery_1 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = USA
			has_global_flag = sell_USA_arty_1
			ROOT = {
				NOT = {
					tag = USA
				}
			}
		}
		
		icon = usa
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.1 }
		}
		custom_cost_text = cost_0_1
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.1 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_1
				amount = 1200
				producer = USA
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_USA_artillery_2 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = USA
			has_global_flag = sell_USA_arty_2
			ROOT = {
				NOT = {
					tag = USA
				}
			}
		}
		
		icon = usa
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.125 }
		}
		custom_cost_text = cost_0_125
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.125 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_2
				amount = 1200
				producer = USA
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_USA_artillery_3 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = USA
			has_global_flag = sell_USA_arty_3
			ROOT = {
				NOT = {
					tag = USA
				}
			}
		}
		
		icon = usa
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.15 }
		}
		custom_cost_text = cost_0_15
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.15 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_3
				amount = 1200
				producer = USA
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	EP_buy_USA_artillery_4 = {
	
		visible = {
			has_country_flag = show_arty
			has_country_flag = arty_cat_open
			country_exists = USA
			has_global_flag = sell_USA_arty_4
			ROOT = {
				NOT = {
					tag = USA
				}
			}
		}
		
		icon = usa
		
		days_remove = 25
		days_re_enable = 0
		
		custom_cost_trigger = {
			check_variable = { treasury > 0.175 }
		}
		custom_cost_text = cost_0_175
		
		complete_effect = {
			hidden_effect = {
				subtract_from_variable = { treasury = 0.175 }
				set_country_flag = arty_order_active
			}
		}
		remove_effect = {
			add_equipment_to_stockpile = {
				type = artillery_4
				amount = 1200
				producer = USA
			}
			hidden_effect = {
				set_country_flag = artillery_delivered
				clr_country_flag = arty_order_active
			}
		}
	}
	
	#end of artiljerija

}