Syria_decisions = {
	
	allowed = {
		OR = {
			original_tag = SYR
			original_tag = NUS
			original_tag = FSA
			original_tag = ALA
			original_tag = ISI
			original_tag = DRU
			original_tag = ROJ
		}
	}
	
}

Syria_foreign_policy = {

	allowed = {
		OR = {
			original_tag = SYR
			original_tag = FSA
			original_tag = NUS
		}
	}
	
}

Syria_external_countries = {

	allowed = {
		always = yes
	}
	
}