
EP_main_cat = {
	
	EP_open_cats = {
	
		icon = GFX_decision_generic_prepare_civil_war
	
		visible = {
			hidden_trigger = {
				NOT = {
					has_country_flag = EP_hide_forever
					has_country_flag = EP_open_cats
				}
			}
			is_ai = no
		}
		
		complete_effect = {
			set_country_flag = EP_open_cats
		}
	}
	
	EP_hide_forever = {
	
		icon = GFX_decision_generic_prepare_civil_war
	
		visible = {
			hidden_trigger = {
				NOT = {
					has_country_flag = EP_hide_forever
					has_country_flag = EP_open_cats
					has_country_flag = inf_cat_open
					has_country_flag = vhc_cat_open
					has_country_flag = arty_cat_open
					has_country_flag = air_cat_open
					has_country_flag = navy_cat_open
				}
			}
			is_ai = no
		}
		
		complete_effect = {
			set_country_flag = EP_hide_forever
		}
	}
	
	EP_close_cats = {
	
		icon = GFX_decision_generic_prepare_civil_war
	
		visible = {
			hidden_trigger = {
				NOT = {
					has_country_flag = EP_hide_forever
				}
				has_country_flag = EP_open_cats
			}
		}
		
		complete_effect = {
			clr_country_flag = EP_open_cats
		}
	}
	
	EP_infantry_open = { #opens infantry tab
	
		icon = GFX_decision_generic_prepare_civil_war
	
		visible = {
			hidden_trigger = {
				NOT = {
					has_country_flag = inf_cat_open
				}
				has_country_flag = EP_open_cats
			}
		}
		
		complete_effect = {
			set_country_flag = inf_cat_open
		}
	}
	
	EP_arty_open = { #opens infantry tab
	
		icon = GFX_decision_generic_prepare_civil_war
	
		visible = {
			hidden_trigger = {
				NOT = {
					has_country_flag = arty_cat_open
				}
				has_country_flag = EP_open_cats
			}
		}
		
		complete_effect = {
			set_country_flag = arty_cat_open
		}
	}
	
	EP_vhc_open = { #opens vhc tab
	
		icon = GFX_decision_generic_prepare_civil_war
	
		visible = {
			hidden_trigger = {
				NOT = {
					has_country_flag = vhc_cat_open
				}
				has_country_flag = EP_open_cats
			}
		}
		
		complete_effect = {
			set_country_flag = vhc_cat_open
		}
	}
	
	EP_air_open = { #opens air tab
	
		icon = GFX_decision_generic_prepare_civil_war
	
		visible = {
			hidden_trigger = {
				NOT = {
					has_country_flag = air_cat_open
				}
				has_country_flag = EP_open_cats
			}
		}
		
		complete_effect = {
			set_country_flag = air_cat_open
		}
	}
	
	EP_navy_open = { #opens navy tab
	
		icon = GFX_decision_generic_prepare_civil_war
	
		visible = {
			hidden_trigger = {
				NOT = {
					has_country_flag = navy_cat_open
				}
				has_country_flag = EP_open_cats
			}
		}
		
		complete_effect = {
			set_country_flag = navy_cat_open
		}
	}
}