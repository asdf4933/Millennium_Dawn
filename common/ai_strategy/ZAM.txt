# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Zambia supports Kabila
ZAM_DRC = {

	enable = {
		original_tag = ZAM
	}
	abort = {
		has_war_with = DRC
	}

	ai_strategy = {
		type = befriend
		id = "DRC"
		value = 50
	}
}