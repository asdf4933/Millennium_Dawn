# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Eritrean border conflict with Djibouti
Djibouti_Eritrea_Border_Conflict = {
	enable = {
		original_tag = DJI
		country_exists = ERI
		has_government = nationalist
		has_opinion_modifier = ERI_Border_Disputes
	}
	abort = {
		OR = {
			NOT = { country_exists = ERI }
			ERI = { NOT = { has_government = nationalist } }
			NOT = { has_opinion_modifier = ERI_Border_Disputes }
		}
	}
	
	ai_strategy = {
		type = antagonize
		id = "ERI"
		value = 50
	}
	ai_strategy = {
		type = contain
		id = "ERI"
		value = 100
	}
	ai_strategy = {
		type = conquer
		id = "ERI"
		value = 50
	}
}