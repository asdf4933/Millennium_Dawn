# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

Oman_SaudiArabia_Relations {
	enable = {
		original_tag = OMA
		has_opinion_modifier = Gulf_Cooperation_Council_Relations
	}
	abort = {
		NOT = { has_opinion_modifier = Gulf_Cooperation_Council_Relations }
	}
	
	ai_strategy = {
		type = befriend
		id = "SAU"
		value = 200
	}
	ai_strategy = {
		type = alliance
		id = "SAU"
		value = -50
	}
	
}