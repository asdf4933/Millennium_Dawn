# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#South Sudanese forces occasionally cross the border into Sudan
South_Sudan_intervene_Sudan = {

	enable = {
		original_tag = SSU
	}
	
	abort = {
		SRF = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
				in_different_faction = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "SRF"
		value = 50
	}
	
	ai_strategy = {
		type = protect 
		id = "SRF"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "SRF"
		value = 50
	}
	
	ai_strategy = {
		type = support
		id = "SRF"
		value = 50
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "SRF"
		value = 50
	}
}