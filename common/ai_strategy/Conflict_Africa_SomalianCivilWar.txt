# Written by Niko92

# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#AMISOM mission
AMISOM_mission_SOM = {
	
	enable = {
		OR = {
			original_tag = UGA
			original_tag = BUR
			original_tag = KEN
			original_tag = DJI
			original_tag = SIE
			original_tag = NIG
			original_tag = GAH
		}
		OR = {
			has_government = democratic
			has_government = neutrality
			has_government = communism
		}
		SOM = {
			OR = {
				has_war_with = SHB
				has_civil_war = yes
			}
			OR = {
				has_government = democratic
				has_government = neutrality
				has_government = communism
			}
		}			
	}
	
	abort = {
		SOM = {
			OR = {
				AND = {
					NOT = { has_war_with = SHB }
					has_civil_war = no
				}
			}
			OR = {
				has_government = nationalist
				has_government = fascism
			}
		}
	}
	
	ai_strategy = {
		type = befriend
		id = "SOM"
		value = 100
	}
	
	ai_strategy = {
		type = support
		id = "SOM"
		value = 100
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "SOM"
		value = 100
	}
	
	ai_strategy = {
		type = protect
		id = "SOM"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "SOM"
		value = 50
	}
}

#AMISOM, if Somalia has fallen, deal with Al-Shabaab
AMISOM_intervene_SOM = {
	
	enable = {
		OR = {
			original_tag = UGA
			original_tag = BUR
			original_tag = ETH
			original_tag = KEN
			original_tag = DJI
			original_tag = SIE
			original_tag = NIG
			original_tag = GAH
		}
		OR = {
			has_government = democratic
			has_government = neutrality
			has_government = communism
		}
		OR = {
			AND = {
				SOM = { exists = no }
				SHB = { has_government = fascism }
			}
			SOM = { has_government = fascism }
		}
	}
	
	abort = {
		OR = {
			AND = {
				SHB = { exists = no }
				SOM = { NOT = { has_government = fascism } }
			}
			AND = {
				SOM = { exists = no }
				SHB = { NOT = { has_government = fascism } }
			}
			has_government = fascism
		}
	}
	
	ai_strategy = {
		type = antagonize
		id = "SHB"
		value = 50
	}
	ai_strategy = {
		type = antagonize
		id = "SOM"
		value = 50
	}
	
	ai_strategy = {
		type = contain
		id = "SHB"
		value = 50
	}
	ai_strategy = {
		type = contain
		id = "SOM"
		value = 50
	}
	
	ai_strategy = {
		type = conquer
		id = "SHB"
		value = 50
	}
	ai_strategy = {
		type = conquer
		id = "SOM"
		value = 50
	}
	
}

#EU supports the government against Al-Shabaab
EU_support_SOM = {
	
	enable = {
		has_idea = EU_member
		SOM = {
			OR = {
				has_government = democratic
				has_government = neutrality
			}
			has_war_with = SHB
		}
	}
	
	abort = {
		OR = {
			SOM = { NOT = { has_war_with = SHB } }
			SOM = { has_government = fascism }
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "SOM"
		value = 100
	}
	
	ai_strategy = {
		type = support 
		id = "SOM"
		value = 100
	}
	
	ai_strategy = {
		type = protect 
		id = "SOM"
		value = 25
	}
}
		