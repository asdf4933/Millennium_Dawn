# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

Kosovo_Serbia_Relations = {
	enable = {
		tag = KOS
		country_exists = SER
	}
	abort = {
		NOT = { country_exists = SER }
	}

	ai_strategy = {
		type = antagonize
		id = "SER"
		value = 50
	}
}