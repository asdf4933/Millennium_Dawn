# Written by Killerrabbit

# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

### Two Talibans help each other ###
TTP_TAL = {

	enable = {
		original_tag = TTP
	}
	
	abort = {
		TAL = { 
			OR = { 
				war_with_us = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "TAL"
		value = 100
	}
	
	ai_strategy = {
		type = protect 
		id = "TAL"
		value = 100
	}
	
	ai_strategy = {
		type = support
		id = "TAL"
		value = 200
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "TAL"
		value = 300
	}
}

### Reverse if ever ###
TAL_TTP = {

	enable = {
		original_tag = TAL
	}
	
	abort = {
		TTP = { 
			OR = { 
				war_with_us = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "TTP"
		value = 100
	}
	
	ai_strategy = {
		type = protect 
		id = "TTP"
		value = 100
	}
	
	ai_strategy = {
		type = support
		id = "TTP"
		value = 200
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "TTP"
		value = 300
	}
}