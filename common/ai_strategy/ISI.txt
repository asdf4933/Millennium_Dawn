# Written by Andreas "Cold Evul" Brostrom

# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

War_On_Terrorism_ISIS = {
	enable = {
		NOT = {
			original_tag = ISI
		}
		has_opinion_modifier = ISIS_Are_Terrorists
	}
	abort = {
		OR = {
			has_completed_focus = isis_allegiance
			NOT = { has_opinion_modifier = ISIS_Are_Terrorists }
		}
	}

	# ISIS
	ai_strategy = {
		type = antagonize
		id = "ISI"
		value = 100
	}
	ai_strategy = {
		type = contain
		id = "ISI"
		value = 100
	}
}