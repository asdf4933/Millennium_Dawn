# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Countries that recognise Abkhazia
Recognise_Abkhazia = {
	
	enable = {
		OR = {
			original_tag = SOV
			original_tag = NIC
			original_tag = VEN
			original_tag = SOO
			original_tag = PMR
		}
	}
	
	abort = {
		OR = {
			has_country_flag = Withdrawed_recognition_of_Abkhazia
			has_war_with = ABK
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "ABK"
		value = 25
	}
	
}

#Support to Syria
ABK_Support_Syria = {
	
	enable = {
		original_tag = ABK
	}
	
	abort = {
		has_war_with = SYR
	}
	
	ai_strategy = {	
		type = support
		id = "SYR"
		value = 25
	}
}

#Anti-Georgian attitude
ABK_relations_Georgia = {
	
	enable = {
		original_tag = ABK
	}
	
	abort = {
		GEO = { has_country_flag = Recognised_Abkhazia }
	}
	
	ai_strategy = {	
		type = contain
		id = "GEO"
		value = 50
	}
	ai_strategy = {	
		type = antagonize
		id = "GEO"
		value = 50
	}
	ai_strategy = {	
		type = befriend
		id = "GEO"
		value = -50
	}
}

#Seek help from Russia
ABK_relations_Russia = {
	
	enable = {
		original_tag = ABK
	}
	
	abort = {
		OR = {
			has_war_with = SOV
			GEO = { is_in_faction_with = SOV }
		}
	}
	
	ai_strategy = {
		type = befriend
		id = "SOV"
		value = 100
	}
}