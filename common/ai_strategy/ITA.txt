# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Italian support for Afghanistan
ITA_AFG2 = {

	enable = {
		original_tag = ITA
		date < 2017.1.12
	}
	
	abort = {
		date > 2017.1.12
	}

	ai_strategy = {
		type = send_volunteers_desire
		id = "AFG"
		value = -200
	}
}

ITA_AFG = {

	enable = {
		original_tag = ITA
	}
	
	abort = {
		AFG = { 
			OR = { 
				war_with_us = yes
				is_bad_salafist = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "AFG"
		value = 50
	}
	
	ai_strategy = {
		type = support
		id = "AFG"
		value = 150
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "AFG"
		value = 150
	}
}

#Italy has special forces in Libya
Italy_Ops_in_Libya_GNA = {

	enable = {
		original_tag = ITA
	}
	
	abort = {
		GNA = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
				in_different_faction = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "GNA"
		value = 25
	}
	
	ai_strategy = {
		type = protect 
		id = "GNA"
		value = 25
	}
	
	ai_strategy = {
		type = influence
		id = "GNA"
		value = 25
	}
}