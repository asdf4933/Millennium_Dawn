# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Countries with more at least 1 combat battalion deployed
UNAMSIL_military_deployment = {

	enable = {
		OR = {
			original_tag = NIG
			original_tag = JOR
			original_tag = GAH
			original_tag = GUI
			original_tag = BAN
			original_tag = KEN
		}
	}
	
	abort = {
		SIE = { 
			OR = {
				NOT = { has_government = democratic }
				NOT = { has_war_with = AFR }
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "SIE"
		value = 50
	}
	
	ai_strategy = {
		type = support
		id = "SIE"
		value = 150
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "SIE"
		value = 100
	}
}

#Countries providing only equipment/logistic support
UNAMSIL_secondary_help = {

	enable = {
		OR = {
			original_tag = SOV
			original_tag = UKR
			original_tag = RAJ
		}
	}
	
	abort = {
		SIE = { 
			OR = {
				NOT = { has_government = democratic }
				NOT = { has_war_with = AFR }
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "SIE"
		value = 25
	}
	
	ai_strategy = {
		type = support
		id = "SIE"
		value = 100
	}
}