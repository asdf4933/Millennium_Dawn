# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Belarus supports Georgian integrity
BLR_ABK = {

	enable = {
		original_tag = BLR
	}
	
	abort = {
		OR = {
			ABK = { exists = no }
			has_war_with = GEO
		}
	}
	
	ai_strategy = {
		type = contain 
		id = "ABK"
		value = 25
	}
	
	ai_strategy = {
		type = befriend
		id = "GEO"
		value = 25
	}
}