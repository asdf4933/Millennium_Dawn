set_leader_POR = {

	if = { limit = { has_country_flag = set_Monarchist }

		if = { limit = { check_variable = { Monarchist_leader = 0 } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Duarte Pio"
				picture = "generic.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Conservative }
		if = { limit = { check_variable = { Conservative_leader = 0 } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Paulo Portas"
				picture = "POR_Paulo_Portas.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 0 } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "José Manuel Barroso"
				picture = "POR_Jose_Manuel_Barroso.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_socialism }
		if = { limit = { check_variable = { socialism_leader = 0 } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "António Guterres"
				picture = "POR_Antonio_Guterres.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { socialism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jorge Sampaio"
				picture = "Jorge_Sampaio.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { socialism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ferro Rodrigues"
				picture = "generic.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Fascism }
		if = { limit = { check_variable = { Nat_Fascism_leader = 0 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Jose Pinto Coelho"
				picture = "jose_pinto_coelho.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_social }
		if = { limit = { check_variable = { Neutral_social_leader = 0 } }
			add_to_variable = { Neutral_social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Collective leadership "
				picture = "collective_leadership .dds"
				ideology = Neutral_social
				traits = {
					emerging_Communist-State
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_conservatism }
		if = { limit = { check_variable = { conservatism_leader = 0 } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Pedro Passos Coelho"
				picture = "pedro_passos_coelho.dds"
				ideology = conservatism
				traits = {
					western_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Anibal Cavaco Silva"
				picture = "Anibal_Cavaco_Silva.dds"
				ideology = conservatism
				traits = {
					western_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
}