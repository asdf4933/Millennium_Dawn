set_leader_TUA = {

	if = { limit = { has_country_flag = set_Nat_Populism }

		if = { limit = { check_variable = { Nat_Populism_leader = 0 } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Bilal Ag Acherif"
				picture = "nationalist_Tuaregs_Bilal_Ag_Acherif.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_conservatism }
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mustafa Salem"
				picture = "Neutrality_Tuaregs_Mustafa_Salem.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Kingdom }
		if = { limit = { check_variable = { Kingdom_leader = 0 } }
			add_to_variable = { Kingdom_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mohamed Ag Intalla"
				picture = "Salafist_Tuaregs_Mohamed_Ag_Intalla.dds"
				ideology = Kingdom
				traits = {
					salafist_Kingdom
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Kingdom_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
}