
UKR_west_east_rivalry_towards_east = {
	
	#4
	if = {
		limit = {  has_idea = UKR_east_strengthened }
		swap_ideas = {
			remove_idea = UKR_east_strengthened
			add_idea = UKR_east_dominant
		}
	}
	
	#3
	if = {
		limit = { has_idea = UKR_west_east_rivalry_balanced }
		swap_ideas = {
			remove_idea = UKR_west_east_rivalry_balanced
			add_idea = UKR_east_strengthened
		}
	}
	
	#2
	if = {
		limit = { has_idea = UKR_west_strengthened }
		swap_ideas = {
			remove_idea = UKR_west_strengthened
			add_idea = UKR_west_east_rivalry_balanced
		}
	}
	
	#1
	if = {
		limit = { has_idea = UKR_west_dominant }
		swap_ideas = {
			remove_idea = UKR_west_dominant
			add_idea = UKR_west_strengthened
		}
	}
}

UKR_west_east_rivalry_towards_west = {
	
	#4
	if = {
		limit = { has_idea = UKR_west_strengthened }
		swap_ideas = {
			remove_idea = UKR_west_strengthened
			add_idea = UKR_west_dominant
		}
	}
	
	#3
	if = {
		limit = { has_idea = UKR_west_east_rivalry_balanced }
		swap_ideas = {
			remove_idea = UKR_west_east_rivalry_balanced
			add_idea = UKR_west_strengthened
		}
	}
	
	#2
	if = {
		limit = { has_idea = UKR_east_strengthened }
		swap_ideas = {
			remove_idea = UKR_east_strengthened
			add_idea = UKR_west_east_rivalry_balanced
		}
	}
	
	#1
	if = {
		limit = { has_idea = UKR_east_dominant }
		swap_ideas = {
			remove_idea = UKR_east_dominant
			add_idea = UKR_east_strengthened
		}
	}
}