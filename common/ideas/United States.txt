ideas = {

	country = {
		#Starting Ideas - USA Legacy
		american_militarism = {
			picture = american_militarism
		
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			
			removal_cost = -1
			
			modifier = {
				stability_factor = 0.05
				conscription_factor = 0.1
				army_leader_start_level = 1
				experience_gain_army = 0.01
				experience_gain_navy = 0.01
				experience_gain_air = 0.01
			}
		}
		
		idea_USA_political_establishment = {
			picture = political_establishment
		
			allowed = { always = no }
			
			cancel = {
				NOT = { has_government = democratic }
			}
			
			allowed_civil_war = {
				original_tag = USA
				has_government = democratic
			}
		
			removal_cost = -1
			
			modifier = {
				stability_weekly = -0.002
				democratic_drift = 0.025
				political_power_gain = -0.5
			}
		}
		
		USA_first_amendment = {
			picture = disjointed_government
			
			allowed = { always = no }
			allowed_civil_war = { has_government = democratic }
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = 0.001
				drift_defence_factor = -0.3
			}
		}
		
		USA_second_amendment = {
			picture = volunteer_defenders
			
			allowed = { always = no }
			allowed_civil_war = { 
				AND = {
					has_government = democratic 
					has_government = neutrality
				}
			}
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = 0.001
				army_core_attack_factor = 0.05
				army_core_defence_factor = 0.1
				political_power_factor = -0.2
			}
		}
		
		USA_congress_authority = {
			picture = scw_intervention_nat
			
			allowed = { always = no }
			allowed_civil_war = { 
				AND = {
					has_government = democratic 
					has_government = neutrality
				}
			}
			
			removal_cost = -1
			
			modifier = {
				justify_war_goal_time = 2
				political_power_factor = -0.1
			}
		}
		
		#Political Ideas - Focus Rewards and the Like
		
		idea_USA_new_education_program = {
			picture = research_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			
			removal_cost = -1
			
			modifier = { 
				research_speed_factor = 0.03
			}
		}
		
		idea_USA_united_republic = {
			picture = political_power_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = 0.001
				drift_defence_factor = 0.4
				political_power_factor = 0.2
			}
		}
		
		USA_abolished_the_house = {
			picture = anti_soviet_pact
			
			allowed = { always = no }
			allowed_civil_war = { has_government = communism }
			
			removal_cost = -1
			
			modifier = {
				political_power_factor = -0.25
				stability_weekly = -0.003
			}
		}
		
		USA_abolished_the_senate = {
			picture = matignon_agreements
			
			allowed = { always = no }
			allowed_civil_war = { has_government = communism }
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = -0.005
			}
		}
		
		USA_american_communist_influence = {
			picture = communism9
			
			allowed = { always = no }
			allowed_civil_war = { has_government = communism }
			
			removal_cost = -1
			
			modifier = {
				communism_drift = 0.05
			}
		}
		
		USA_american_democratic_influence = {
			picture = democracy
			
			allowed = { always = no }
			allowed_civil_war = { has_elections = no }
			
			removal_cost = -1
			
			modifier = {
				democratic_drift = 0.05
			}
		}
		
		USA_american_fascism = {
			picture = segregation2
			
			allowed = { always = no }
			allowed_civil_war = { has_government = nationalist }
			
			removal_cost = -1
			
			modifier = {
				nationalist_drift = 0.10
				local_resources_factor = 0.5
				justify_war_goal_time = -0.5
				industrial_capacity_factory = -0.75
				conscription_factor = -0.75
				stability_weekly = -0.006
			}
		}
		
		USA_american_fascism_weakened = {
			picture = segregation
			
			allowed = { always = no }
			allowed_civil_war = { has_government = nationalist }
			
			removal_cost = -1
			
			modifier = {
				nationalist_drift = 0.05
				local_resources_factor = 0.25
				justify_war_goal_time = -0.25
				industrial_capacity_factory = -0.1
				conscription_factor = -0.1
				stability_weekly = -0.001
			}
		}
		
		USA_american_nationalist_influence = {
			picture = fascism2
			
			allowed = { always = no }
			allowed_civil_war = { has_elections = no }
			
			removal_cost = -1
			
			modifier = {
				nationalist_drift = 0.10
			}
		}
		
		USA_anglo_saxon_economic_principles = {
			picture = consumer_goods
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				industrial_capacity_factory = 0.05
				consumer_goods_factor = -0.03
			}
		}
		
		USA_automotive_industry_funding = {
			picture = manpower_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			equipment_bonus = {
				motorized = {
					build_cost_ic = -0.1
					instant = yes
				}
				light_mechanized = {
					build_cost_ic = -0.1
					instant = yes
				}
				mechanized = {
					build_cost_ic = -0.1
					instant = yes
				}
			}
		}
		
		#Military 
		
		idea_USA_drone_program = {
			picture = air_support
			
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			
			removal_cost = -1
			
			equipment_bonus = {
				tactical_bomber = { 
					build_cost_ic = -0.25 
				}
			}
		}
		
		USA_aerial_scouts = {
			picture = air_support
			
			allowed = { always = no }
			allowed_civil_war = { has_government = communism }
			
			removal_cost = -1
			
			equipment_bonus = {
				fighter_equipment = {
					air_superiority = 0.1
					maximum_speed = 0.1
				}
			}
		}
		
		USA_american_militias_idea = {
			picture = manpower_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				conscription = 0.01
			}
		}
		
		USA_army_college = {
			picture = army_war_college
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				army_leader_start_level = 1
				experience_gain_army = 0.01
				planning_speed = 0.2
			}
		}
		
		USA_border_militias = {
			picture = infantry_bonus
			
			allowed = { always = no }
			allowed_civil_war = { has_government = democratic }
			
			removal_cost = -1
			
			modifier = {
				conscription = 0.005
			}
		}
		
		#Diplo Ideas - Diplo Tree rewards and etc
		
		USA_american_aid_for_israel = {
			picture = american_militarism
			
			allowed = { always = no }
			allowed_civil_war = { has_elections = no }
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = 0.002
				army_leader_start_level = 1
				experience_gain_air = 0.04
				experience_gain_army = 0.04
				experience_gain_navy = 0.04
			}
		}
		
		USA_american_military_assistance = {
			picture = american_militarism
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = 0.001
				army_leader_start_level = 1
				experience_gain_air = 0.03
				experience_gain_army = 0.03
				experience_gain_navy = 0.03
			}
		}
		
		USA_border_protection_reform = {
			picture = fortification2
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = 0.001
				production_speed_bunker_factor = 0.3
				monthly_population = -0.05
			}
		}
		
		USA_carrier_group_tactics_idea = {
			picture = liberty_ships
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			equipment_bonus = {
				carrier = {
					max_strength = 100
					carrier_size = 10
				}
			}
		}
		
		USA_centralized_industrial_control_idea = {
			picture = construction
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				industrial_capacity_factory = 0.1
			}
		}
		
		USA_christian_influence = {
			picture = protestant_faith
			
			allowed = { always = no }
			allowed_civil_war = {
				NOT = { has_government = fascism }
				NOT = { has_government = communism }
			}
			cancel = {
				OR = {
					has_government = fascism
					has_government = communism
				}
			}
			
			removal_cost = -1
			
			modifier = {
				stability_factor = 0.05
				political_power_factor = 0.1
				fascism_drift = -0.02
			}
		}
		
		USA_communist_purge = {
			picture = communism4
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				communism_drift = 0.04
				political_power_factor = 0.1
				stability_weekly = -0.003
			}
		}
		
		USA_decreased_education_funding = {
			picture = scientists_defect
			
			allowed = { always = no }
			allowed_civil_war = { has_government = democratic }
			
			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.02
				research_speed_factor = 0.02
			}
		}
		
		USA_defeat_in_afghanistan = {
			picture = war_to_end_all_wars
			
			allowed = { always = no }
			allowed_civil_war = { has_government = democratic }
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = -0.001
				army_morale_factor = -0.1
				army_org_Factor = -0.1
			}
		}
		
		USA_defeat_in_iraq = {
			picture = war_to_end_all_wars
			
			allowed = { always = no }
			allowed_civil_war = { has_government = democratic }
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = -0.001
				army_morale_factor = -0.1
				army_org_Factor = -0.1
			}
		}
		
		USA_destroyer_group_tactics_idea = {
			picture = degauss_ship_hulls
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			equipment_bonus = {
				destroyer = {
					sub_detection = 0.10
				}
			}
		}
		
		USA_diplomatic_corps_idea = {
			picture = political_power_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				political_power_factor = 0.2
			}
		}
		
		USA_drone_warfare_idea = {
			picture = spy_intel
			
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			
			removal_cost = -1
			
			equipment_bonus = {
				ucav_equipment = {
					air_ground_attack = 0.1
					naval_strike_attack = 0.1
					naval_strike_targetting = 0.1
				}
			}
		}
		 
		USA_economic_deregulation = {
			picture = consumer_goods
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.05
			}
		}
		
		USA_economic_regulation = {
			picture = intel_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				global_building_slots_factor = 0.1
			}
		}
		
		USA_expanded_national_guard = {
			picture = infantry_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				conscription = 0.01
			}
		}
		
		USA_food_deliveries_to_the_black_free_state = {
			picture = agriculture
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = 0.1
			}
		}
		
		USA_free_college_idea = {
			picture = shared_research
			
			allowed = { always = no }
			allowed_civil_war = { NOT = { has_government = democratic } }
			
			removal_cost = -1
			
			modifier = {
				research_speed_factor = 0.03
				stability_weekly = 0.001
				consumer_goods_factor = 0.025
			}
		}
		
		USA_glorify_the_military_idea = {
			picture = infantry_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				army_morale_factor = 0.1
				army_org_Factor = 0.1
			}
		}
		
		USA_gun_funding = {
			picture = volunteer_defenders
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			equipment_bonus = {
				Inf_equipment = {
					build_cost_ic = -0.07
					instant = yes
				}
			}
		}
		
		USA_healthcare_reform_idea = {
			picture = production_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = 0.001
				monthly_population = 0.02
			}
		}
		
		USA_improved_tanks1 = {
			picture = german_advisors
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			equipment_bonus = {
				light_armor = {
					build_cost_ic = -0.1
					breakthrough = 0.05
				}
				modern_armor = {
					build_cost_ic = -0.1
					breakthrough = 0.05
				}
			}
		}
		
		USA_improved_tanks2 = {
			picture = german_advisors
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			equipment_bonus = {
				light_armor = {
					build_cost_ic = -0.1
					breakthrough = 0.15
				}
				modern_armor = {
					build_cost_ic = -0.1
					breakthrough = 0.15
				}
			}
		}
		
		USA_industrial_development_program_idea1 = {
			picture = production_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				production_speed_industrial_complex_factor = 0.1
			}
		}
		
		USA_industrial_development_program_idea2 = {
			picture = production_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				production_speed_industrial_complex_factor = 0.2
			}
		}
		
		USA_industrial_lobbyism_idea = {
			picture = production_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				industrial_capacity_factory = 0.05
				production_speed_industrial_complex_factor = 0.05
			}
		}
		
		USA_jingoism = {
			picture = national_mobilization
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				army_morale_factor = 0.1
				army_org_Factor = 0.1
				justify_war_goal_time = -0.1
			}
		}
		
		USA_massive_military_industrial_complex = {
			picture = new_deal
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				production_speed_arms_factory_factor = 0.15
				production_speed_dockyard_factor = 0.15
			}
		}
		
		USA_military_development_program_idea = {
			picture = construction
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				production_speed_arms_factory_factor = 0.1
			}
		}
		
		USA_national_disgrace = {
			picture = war_to_end_all_wars
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				political_power_gain = -0.1
				army_morale_factor = -0.1
				army_org_Factor = -0.1
			}
		}
		
		USA_national_surveillance = {
			picture = spy_coup
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				decryption = 1
				encryption = 1
			}
		}
		
		USA_naval_development_program_idea = {
			picture = escort_effort
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				production_speed_dockyard_factor = 0.1
			}
		}
		
		USA_no_child_left_behind_act = {
			picture = research_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				research_speed_factor = 0.015
			}
		}
		
		USA_every_student_succeeds_act = {
			picture = research_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				research_speed_factor = 0.035
			}
		}
		
		USA_patriot_act_idea = {
			picture = triumphant_will
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				political_power_factor = 0.25
				stability_weekly = -0.001
			}
		}
		
		USA_persecution_of_conservatives = {
			picture = prisoners
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				political_power_gain = -0.1
				democratic_drift = -0.05
			}
		}
		
		USA_persecution_of_liberals = {
			picture = prisoners
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				political_power_gain = -0.1
				democratic_drift = -0.05
			}
		}
		
		USA_prism_idea = {
			picture = spy_intel
			
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			
			removal_cost = -1
			
			modifier = {
				political_power_factor = 0.15
				research_speed_factor = 0.01
				stability_weekly = -0.001
			}
		}
		
		USA_private_research_teams_idea = {
			picture = shared_research
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				research_speed_factor = 0.02
			}
		}
		
		USA_resource_investment_program1 = {
			picture = construction
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				local_resources_factor = 0.1
			}
		}
		
		USA_resource_investment_program2 = {
			picture = construction
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				local_resources_factor = 0.25
			}
		}
		
		USA_resource_investment_program3 = {
			picture = construction
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				local_resources_factor = 0.4
			}
		}
		
		USA_scientific_investments_idea = {
			picture = research_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				research_speed_factor = 0.01
			}
		}
		
		USA_seven_in_five_plan_idea = {
			picture = planning_bonus
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				political_power_factor = 0.1
				stability_weekly = -0.001
				ai_join_ally_desire_factor = -75
				ai_call_ally_desire_factor = -25
			}
		}
		
		USA_strengthened_marine_corps = {
			picture = escort_effort
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				amphibious_invasion = 0.2
				naval_invasion_capacity = 15
			}
		}
		
		USA_submarine_tactics = {
			picture = escort_effort
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			equipment_bonus = {
				submarine = {
					build_cost_ic = -0.1
					surface_detection = 0.05
					sub_detection = 0.05
					torpedo_attack = 0.05
				}
			}
		}
		
		USA_the_war_on_terror = {
			picture = political_power_bonus
			
			allowed = { always = no }
			allowed_civil_war = { NOT = { has_government = fascism } }
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = 0.001
				army_morale_factor = 0.05
				ai_call_ally_desire_factor = -100
			}
		}
	
		USA_uncle_sam = {
			picture = manpower_bonus
		
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			
			removal_cost = -1
			
			modifier = {
				conscription_factor = 0.3
				army_core_defence_factor = 0.1
			}
		}
		
		USA_universal_healthcare_idea = {
			picture = agriculture
			
			allowed = { always = no }
			allowed_civil_war = { always = no }
			
			removal_cost = -1
			
			modifier = {
				stability_weekly = 0.001
				monthly_population = 0.03
				political_power_factor = 0.1
			}
		}
		
		#Event and Globallization Things
		
		USA_victory_in_afghanistan = {
			picture = victors_of_ww1
			
			allowed = { always = no }
			allowed_civil_war = { has_government = democratic }
			
			removal_cost = -1
			
			modifier = {
				army_morale_factor = 0.1
				army_org_Factor = 0.1
			}
		}
		
		USA_victory_in_iraq = {
			picture = victors_of_ww1
			
			allowed = { always = no }
			allowed_civil_war = { has_government = democratic }
			
			removal_cost = -1
			
			modifier = {
				army_morale_factor = 0.1
				army_org_Factor = 0.1
			}
		}
	}
}