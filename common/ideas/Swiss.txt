ideas = {
	country = {
		swiss_political_system = {
		
			picture = swiss_political_system
			
			allowed = {
				original_tag = SWI
			}
			
			cancel = {
				OR = {
					is_puppet = yes
					AND = {
						NOT = { has_government = democratic }
						NOT = { has_government = neutrality }
					}
				}
			}
			
			modifier = {
				stability_factor = 0.05
				war_support_factor = 0.1
				democratic_drift = 0.01
				political_power_factor = -0.25
			}
		}
	}
}