
ideas = {

	country = {
		
		party_before_country = {
			
			allowed = {
					original_tag = CHI
			}
			
			default = yes
	
			modifier = {
				drift_defence_factor = 1.0
				communism_drift = 0.1
				army_org_factor = -0.03
				experience_gain_factor = -0.20
				send_volunteer_size = -2
				conscription = -0.015
			}
		}

		
		zhonghua = {
			
			allowed = {
					original_tag = CHI
			}
			
			default = yes
	
			modifier = {
				political_power_gain = 0.5
				stability_factor = 0.05
				foreign_subversive_activites = -0.5
				join_faction_tension = 0.75
			}
		}

		
		chabuduo = {
			
			allowed = {
					original_tag = CHI
			}
			
			default = yes
	
			modifier = {
				production_speed_buildings_factor = 0.2
				industry_free_repair_factor = -0.75
				industry_repair_factor = -0.75
				local_resources_factor = -0.10
				corruption_cost_factor = 0.6
			}
		}
}

}