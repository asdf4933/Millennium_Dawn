ideas = {
	country = {

		kamajors = {
			
			allowed = {
				original_tag = SIE
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				political_power_factor = -0.25
				stability_factor = -0.10
				army_core_attack_factor = 0.10
				army_core_defence_factor = 0.10
			}
		}
		
		blood_diamond_trade = {
				
			allowed = {
				OR = {
					original_tag = SIE
					original_tag = AFR
				}
			}
			
			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1
			
			modifier = {
				political_power_gain = 0.50
				stability_factor = -0.10
				corruption_cost_factor = 0.50
			}
		}
	}
}