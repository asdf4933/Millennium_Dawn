#########################################################################
# OPINION MODIFIERS
##########################################################################
# value
# min_trust
# max_trust
# decay
# months/years/days = timer
# trade = yes/no

opinion_modifiers = {

	hostile_status = { #special used for civil wars that are on hold and blocks faction stuff
		value = -100
	}
	
	attacked_our_influence = {
		value = -20
		min_trust = -5
		months = 36
	}

	unstable_alliance = {
		value = -10
	}

	holds_our_cores = {
		value = -30
	}
	holds_our_claims = {
		value = -15
	}
	claims_on_us = { 
		value = -10
	}
	at_war_with_faction = {
		value = -25
	}
	
	justifying_war_goal = {
		value = -10
		min_trust = -10
		days = 10
		decay = 1
	}

	improve_relation = {
		value = 0
		months = 0
		decay = 1
		max_trust = 50
	}
	
	guarantee = {
		value = 0
	}
	
	guarantee = {
		target = yes
		value = 20
	}
	
	betrayed_guarantee = {
		value = -100
		months = 36
	}
	
	at_war = {
		value = -50
	}
	
	in_faction = {
		value = 50
	}
	
	military_access = {
		value = 5
	}

	same_ruling_party = {
		value = 10
	}
	
	similar_ruling_party = {
		value = 10
	}
	
	different_party_types = {
		value = -10
	}
	
	long_term_trade_partner = {
		value = 1
	}
	
	puppet_opinion = {
		value = 50
	}

	sanctions_relations = {
		value = -40
	}
	minor_sanctions_relations = {
		value = -20
	}
	
	faction_traitor = {
		value = -75
	}
	
	faction_traitor_trade = {
		trade = yes
		value = -40
	}
	
	our_liberators = {
		value = 40
		months = 18
		decay = 1
	}
	
	unprovoked_aggression = {
		value = -50
		months = 36
		decay = 1
	}
	
	border_conflict_started = {
		value = -50
	}
	
	peace_talks = {
		value = 10
		decay = 0.25
	}
	
	declaration_of_friendship = {
		value = 25
	}
	
	economic_mission = {
		value = 50
		decay = 1
	}
}	

