﻿add_namespace = taiwan

### 228 Hand-in-Hand rally

country_event = {
	id = taiwan.1

	title = taiwan.1.t
	desc = taiwan.1.d
	picture = GFX_TAI_taiwan

	fire_only_once = yes

	trigger = {
		tag = TAI
		date > 2004.2.28
	}

	option = {
		name = taiwan.1.a
		log = "[GetDateText]: [This.GetName]: taiwan.1.a executed"
		add_popularity = { ideology = democratic popularity = 0.03 }
	}
}

### Typhoon Bilis

country_event = {
	id = taiwan.2

	title = taiwan.2.t
	desc = taiwan.2.d
	picture = GFX_typhoon

	fire_only_once = yes

	trigger = {
		tag = TAI
		date > 2000.8.20
	}

	option = {
		name = taiwan.2.a
		log = "[GetDateText]: [This.GetName]: taiwan.2.a executed"
		subtract_from_variable = {
			var = treasury
			value = 0.13
		}
		add_manpower = -14
		598 = { damage_building = { type = infrastructure damage = 0.2 } damage_building = { type = industrial_complex damage = 0.3 } }
		600 = { damage_building = { type = infrastructure damage = 0.2 } damage_building = { type = industrial_complex damage = 0.3 } }
		hidden_effect = { CHI = { country_event = { id = china.4 days = 2 } } }
	}
}

### 2000 presidential election

country_event = {

	id = taiwan.3
	title = taiwan.3.t
	desc = taiwan.3.d
	picture = GFX_TAI_taiwan

	fire_only_once = yes

	is_triggered_only = yes

	trigger = {
		tag = TAI
		date > 2000.1.1
		date < 2001.1.1
	}

	option = {
		name = taiwan.3.a
		log = "[GetDateText]: [This.GetName]: taiwan.3.a executed"
		set_politics = {
			ruling_party = democratic
			elections_allowed = yes
		}
	}
	option = {
		name = taiwan.3.b
		log = "[GetDateText]: [This.GetName]: taiwan.3.b executed"
		create_country_leader = {
			name = "Lien Chan"
			desc = ""
			picture = "Lien_Chan.dds"
			expire = "2055.1.1"
			ideology = Neutral_conservatism
			traits = {

			}
		}
	}
	option = {
		name = taiwan.3.c
		log = "[GetDateText]: [This.GetName]: taiwan.3.c executed"
		create_country_leader = {
			name = "James Soong"
			desc = ""
			picture = "James_Soong.dds"
			expire = "2055.1.1"
			ideology = Neutral_conservatism
			traits = {

			}
		}
	}
}

### Coast Guard created

country_event = {
	id = taiwan.4

	title = taiwan.4.t
	desc = taiwan.4.d
	picture = GFX_TAI_coast_guard

	fire_only_once = yes

	is_triggered_only = yes

	option = {
		name = taiwan.4.a
		log = "[GetDateText]: [This.GetName]: taiwan.4.a executed"
		navy_experience = 5
	}
}

### National University of Kaohsiung

country_event = {
	id = taiwan.5

	title = taiwan.5.t
	desc = taiwan.5.d
	picture = GFX_TAI_kaoshiung_university

	fire_only_once = yes

	is_triggered_only = yes

	option = {
		name = taiwan.5.a
		log = "[GetDateText]: [This.GetName]: taiwan.5.a executed"
		### TODO research bonus for industry
	}
}