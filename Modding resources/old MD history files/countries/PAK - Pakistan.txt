﻿capital = 440

oob = "PKS_2000"

set_convoys = 320
set_stability = 0.5

set_country_flag = country_language_urdu
set_country_flag = country_language_english

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 2
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	frigate2 = 2
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
	
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

add_ideas = {
	population_growth_rapid
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
		nationalist = {
			popularity = 5
		}
		reactionary = {
			popularity = 20
		}
		islamist = {
			popularity = 45
		}
		social_liberal = {
			popularity = 10
		}
		social_democrat = {
			popularity = 6
		}
		communist = {
			popularity = 15
		}
	}
	
	ruling_party = islamist
	last_election = "1998.2.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Muhammad Rafiq Tarar"
	picture = "Muhammad_R_Tarar.dds"
	ideology = islamic_republican
}

create_country_leader = {
	name = "Yakub Habeebuddin Tucy"
	picture = "Yakub_Tucy.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Fazl-ur-Rehman"
	picture = "Fazl_ur_Rehman.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Pervez Musharraf"
	picture = "Pervez_Musharraf.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Mir Hasil Khan Bizenjo"
	picture = "Mir_Khan.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Bilawal Bhutto Zardari"
	picture = "Bilawal_Zardari.dds"
	ideology = progressive_ideology
}

create_country_leader = {
	name = "Altaf Hussain"
	picture = "Altaf_Hussain.dds"
	ideology = centrist
}

create_country_leader = {
	name = "Jameel Ahmad Malik"
	picture = "Jameel_Ahmad_Malik.dds"
	ideology = leninist
}

create_country_leader = {
	name = "Afrasiab Khattak"
	picture = "Afrasiab_Khattak.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Nawaz Sharif"
	picture = "Nawaz_Sharif.dds"
	ideology = constitutionalist
}

create_country_leader = {
	name = "Imran Khan"
	picture = "Imran_Khan.dds"
	ideology = national_democrat
}

2013.1.1 = {
	set_politics = {
	
	parties = {
		nationalist = {
			popularity = 13
		}
		reactionary = {
			popularity = 20
		}
		islamist = {
			popularity = 40
		}
		social_liberal = {
			popularity = 10
		}
		progressive = {
			popularity = 18
		}
	}
		
		ruling_party = islamist
		last_election = "2013.5.11"
		election_frequency = 48
		elections_allowed = yes
    }
	create_country_leader = {
		name = "Mamnoon Hussain"
		picture = "Mamnoon_Hussain.dds"
		ideology = islamic_republican
	}
}

2015.1.1 = {
	set_politics = {
		last_election = "2013.5.11"
		elections_allowed = yes
	}	
}