﻿capital = 282

oob = "JAP_2000"

set_convoys = 800
set_stability = 0.5

set_country_flag = country_language_japanese

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1


	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_decline
	free_trade
	idea_JAP_emperor_akihito
	japanese_article_9
}

set_politics = {

	parties = {
		reactionary = { popularity = 10 }
		conservative = { popularity = 40 }
		market_liberal = { popularity = 5 }
		social_liberal = { popularity = 20 }
		social_democrat = { popularity = 5 }
		communist = { popularity = 10 }
	}
	
	ruling_party = conservative
	last_election = "1998.7.12"
	election_frequency = 48
	elections_allowed = yes
}

2000.1.1 = {
	
	create_country_leader = {
		name = "Keizo Obuchi"
		ideology = fiscal_conservative
		picture = "Keizo_Obuchi.dds"
	}

	create_country_leader = {
		name = "Naoto Kan"
		picture = "Naoto_Kan.dds"
		ideology = moderate
	}

	create_country_leader = {
		name = "Ichiro Ozawa"
		picture = "Ichiro_Ozawa.dds"
		ideology = libertarian
	}

	create_country_leader = {
		name = "Takenori Kanzaki"
		picture = "Takenori_Kanzaki.dds"
		ideology = counter_progressive_democrat
	}

	create_country_leader = {
		name = "Tetsuzo Fuwa"
		picture = "Tetsuzo_Fuwa.dds"
		ideology = marxist
	}

	create_country_leader = {
		name = "Takako Doi"
		picture = "Takako_Doi.dds"
		ideology = social_democrat_ideology
	}

	create_country_leader = {
		name = "Yuko Mori"
		picture = "Yuko_Mori.dds"
		ideology = green
	}

	create_country_leader = {
		name = "Keiko Itokazu"
		picture = "Keiko_Itokazu.dds"
		ideology = democratic_socialist_ideology
	}

	create_country_leader = {
		name = "Noboyuki Suzuki"
		picture = "Nobuyuki_Suzuki.dds"
		ideology = fascist_ideology
	}

	create_country_leader = {
		name = "Kyoko Nakayama"
		picture = "Kyoko_Nakayama.dds"
		ideology = national_democrat
	}
	
	create_country_leader = {
		name = "Akihito"
		picture = "Akihito.dds"
		ideology = absolute_monarchist
	}

	create_corps_commander = {
		name = "Kiyofumi Iwata"
		picture = "generals/Kiyofumi_Iwata.dds"
		skill = 2
	}

	create_corps_commander = {
		name = "Toshiya Okabe"
		picture = "generals/Toshiya_Okabe.dds"
		skill = 2
	}

	create_corps_commander = {
		name = "Makiya Ota"
		picture = "generals/Makiya_Ota.dds"
		skill = 1
	}

	create_corps_commander = {
		name = "Kazuaki Sumida"
		picture = "generals/Kazuaki_Sumida.dds"
		skill = 1
	}

	create_corps_commander = {
		name = "Takayuki Onozuka"
		picture = "generals/Takayuki_Onozuka.dds"
		skill = 1
	}

	create_corps_commander = {
		name = "Jiro Hiroe"
		picture = "generals/Jiro_Hiroe.dds"
		skill = 1
	}

	create_corps_commander = {
		name = "Ichiro Kawasaki"
		picture = "generals/Ichiro_Kawasaki.dds"
		skill = 1
	}

	create_corps_commander = {
		name = "Masahiro Nagai"
		picture = "generals/Masahira_Nagai.dds"
		skill = 1
	}

	create_corps_commander = {
		name = "Shigeru Kobayashi"
		picture = "generals/Shigeru_Kobayashi.dds"
		skill = 1
	}

	create_navy_leader = {
		name = "Katsutoshi Kawano"
		picture = "admirals/Katsutoshi_Kawano.dds"
		skill = 3
	}

	create_navy_leader = {
		name = "Tomohisa Takei"
		picture = "admirals/Tomohisa_Takei.dds"
		skill = 2
	}

	create_navy_leader = {
		name = "Yoshihisa Inui"
		picture = "admirals/Yoshihisa_Inui.dds"
		skill = 2
	}

	create_navy_leader = {
		name = "Takashi Nishimura"
		picture = "admirals/Takashi_Nishimura.dds"
		skill = 1
	}

	create_navy_leader = {
		name = "Keiji Akahoshi"
		picture = "admirals/Keiji_Akahoshi.dds"
		skill = 1
	}
}

2001.1.1 = {
	complete_national_focus = JAP_develop_honshu
	complete_national_focus = JAP_develop_kyushu
	
	complete_national_focus = JAP_national_renewal
	complete_national_focus = JAP_path_of_peace
	complete_national_focus = JAP_national_industrial_project
	complete_national_focus = JAP_political_renewal
	complete_national_focus = JAP_adapt_article_9
	
	complete_national_focus = JAP_bushido
	complete_national_focus = JAP_continental_campaigns
	complete_national_focus = JAP_ocean_campaigns
	complete_national_focus = JAP_air_force_focus
	
	complete_national_focus = JAP_finish_nishiseto
	complete_national_focus = JAP_southern_expressways
	complete_national_focus = JAP_northern_expressways
	
	complete_national_focus = JAP_new_trade_policy
	complete_national_focus = JAP_trade_with_america
	complete_national_focus = JAP_trade_with_south_korea
}

2010.1.1 = {
	set_party_name = {
		ideology = market_liberal
		long_name = JAP_market_liberal_party_IfO_long
		name = JAP_market_liberal_party_IfO
	}
}

2013.1.1 = { 
	set_politics = {
		parties = {
			nationalist = {
				popularity = 3
			}
			reactionary = {
				popularity = 8
			}
			conservative = {
				popularity = 47
			}
			social_democrat = {
				popularity = 2
			}
			social_liberal = {
				popularity = 25
			}
			market_liberal = {
				popularity = 7
			}
			progressive = {
				popularity = 2
			}
			democratic_socialist = {
				popularity = 1
			}
			communist = {
				popularity = 5
			}
		}
		
		ruling_party = conservative
		last_election = "2013.7.21"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Shinzo Abe"
		ideology = fiscal_conservative
		picture = "Shinzo_Abe.dds"
	}

	create_country_leader = {
		name = "Banri Kaieda"
		picture = "Banri_Kaieda.dds"
		ideology = liberalist
	}

	create_country_leader = {
		name = "Tadamato Yoshida"
		picture = "Tadamato_Yoshida.dds"
		ideology = social_democrat_ideology
	}

	create_country_leader = {
		name = "Natsuo Yamaguchi"
		picture = "Natsuo_Yamaguchi.dds"
		ideology = counter_progressive_democrat
	}

	create_country_leader = {
		name = "Kazuo Shii"
		picture = "Kazuo_Shii.dds"
		ideology = marxist
	}

	create_country_leader = {
		name = "Toru Hashimoto"
		picture = "Toru_Hashimoto.dds"
		ideology = libertarian
	}

}
