﻿capital = 231

oob = "GEO_2000"

set_convoys = 200
set_stability = 0.5

set_country_flag = country_language_georgian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	corvette1 = 1
	corvette2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_decline
	extensive_conscription
}

set_politics = {

	parties = {
		nationalist = {
			popularity = 5
		}
		conservative = {
			popularity = 15
		}
		social_liberal = {
			popularity = 50
		}
		social_democrat = {
			popularity = 10
		}
		communist = {
			popularity = 20
		}
	}
	
	ruling_party = social_liberal
	last_election = "1999.10.31"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Eduard Shevardnadze"
	picture = "Eduard_Shevardnadze.dds"
	ideology = liberalist
}

add_namespace = {
	name = "geo_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Vladimer Chachibaia"
	picture = "generals/Vladimer_Chachigbaia.dds"
	traits = { organisational_leader }
	skill = 1
}

create_field_marshal = {
	name = "Vakhtang Kapanadze"
	picture = "generals/Vakhtang_Kapanadze.dds"
	traits = { inspirational_leader }
	skill = 1
}

create_corps_commander = {
	name = "Zaza Chkhaidze"
	picture = "generals/Zaza_Chkhaidze.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Shalva Jabakhidze"
	picture = "generals/Shalva_Jabakhidze.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Dimitri Kiknadze"
	picture = "generals/Dimitri_Kiknadze.dds"
	traits = { commando ranger }
	skill = 1
}

create_corps_commander = {
	name = "Nikoloz Janjgava"
	picture = "generals/Nikoloz_Janjgava.dds"
	traits = { hill_fighter }
	skill = 1
}

2003.1.1 = {
	create_country_leader = {
		name = "Giorgi Margvelashvili"
		picture = "Giorgi_Margvelashvili.dds"
		ideology = liberalist
	}
}

2016.1.1 = {
	set_politics = {

		parties = {
			nationalist = {
				popularity = 7
			}
			reactionary = {
				popularity = 7
			}
			conservative = {
				popularity = 33
			}
			social_liberal = {
				popularity = 33
			}
			market_liberal = {
				popularity = 9
			}
			social_democrat = {
				popularity = 7
			}
			communist = {
				popularity = 4
			}
		}
		
		ruling_party = social_liberal
		last_election = "2012.10.1"
		election_frequency = 48
		elections_allowed = yes
	}
}