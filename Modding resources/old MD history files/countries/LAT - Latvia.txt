﻿capital = 12

oob = "LAT_2000"

set_convoys = 100
set_stability = 0.5

set_country_flag = country_language_latvian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1



	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_decline
	limited_conscription
}

set_politics = {

	parties = {
		nationalist = {
			popularity = 3
		}
		reactionary = {
			popularity = 7
		}
		conservative = {
			popularity = 25
		}
		social_liberal = {
			popularity = 15
		}
		social_democrat = {
			popularity = 20
		}
		progressive = {
			popularity = 20
		}
		democratic_socialist = {
			popularity = 10
		}
	}
	
	ruling_party = conservative
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

2002.1.1 = { add_ideas = the_euro }
2004.1.1 = { add_ideas = idea_eu_member }

create_country_leader = {
	name = "Andris Šķēle"
	picture = "Andris_Skele.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "Māris Kučinskis"
	picture = "Maris_Kucinskis.dds"
	ideology = green
}

2016.1.1 = {

	oob = "LAT_2016"

	set_politics = {

		parties = {
			nationalist = {
				popularity = 5
			}
			reactionary = {
				popularity = 12
			}
			conservative = {
				popularity = 25
			}
			market_liberal = {
				popularity = 5
			}
			social_liberal = {
				popularity = 5
			}
			social_democrat = {
				popularity = 8
			}
			progressive = {
				popularity = 28
			}
			democratic_socialist = {
				popularity = 12
			}
		}
		
		ruling_party = progressive
		last_election = "2012.1.1"
		election_frequency = 48
		elections_allowed = yes
	}

}