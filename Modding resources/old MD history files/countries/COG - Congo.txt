﻿capital = 759

oob = "CNR_2000"

set_convoys = 20
set_stability = 0.5

set_country_flag = country_language_french
set_country_flag = country_language_lingala
set_country_flag = country_language_munukutuba

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	african_union_member
}

set_politics = {

	parties = {
		reactionary = {
			popularity = 0
		}
		conservative = {
			popularity = 10
		}
		social_liberal = {
			popularity = 30
		}
		social_democrat = {
			popularity = 5
		}
		democratic_socialist = {
			popularity = 45
		}
		communist = {
			popularity = 5
		}
	}
	
	ruling_party = democratic_socialist
	last_election = "1992.8.1"
	election_frequency = 84
	elections_allowed = no
}

create_country_leader = {
	name = "Auguste-Célestin Gongarad Nkoua"
	picture = "Auguste_Nkoua.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Denis Sassou-Nguesso"
	picture = "Denis_Nguesso.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Joseph Kignoumbi Kia Mboungou"
	picture = "Joseph_Mboungou.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Guy Brice Parfait Kolélas"
	picture = "Guy_Kolelas.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "André Milongo"
	picture = "Andre_Milongo.dds"
	ideology = constitutionalist
}

create_country_leader = {
	name = "Jean-Pierre Thystère Tchicaya"
	picture = "Jean_Tchicaya.dds"
	ideology = progressive_ideology
}

create_corps_commander = {
	name = "Guy Blanchard Okoi"
	picture = "generals/Guy_Okoi.dds"
	skill = 1
}

2009.7.12 = {
	
	set_politics = {

		parties = {
			conservative = {
				popularity = 31
			}
			social_liberal = {
				popularity = 2
			}
			social_democrat = {
				popularity = 2
			}
			democratic_socialist = {
				popularity = 50
			}
			communist = {
				popularity = 15
			}
		}
		
		ruling_party = democratic_socialist
		last_election = "2009.7.12"
		election_frequency = 84
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Pascal Tsaty-Mabiala"
		picture = "Pascal_Mabiala.dds"
		expire = "2020.1.1"
		ideology = social_democrat_ideology
		
		traits = {
		
		}
	}
}
2017.1.1 = {

oob = "CNG_2017"


set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
}

add_ideas = {
	pop_050
	rampant_corruption
	christian
	gdp_2
		stable_growth
		defence_07
	edu_03
	health_02
	social_02
	bureau_02
	police_01
	rentier_state
    export_economy
	volunteer_army
	volunteer_women
	fossil_fuel_industry
	international_bankers
	farmers
	hybrid
}
set_country_flag = gdp_2
set_country_flag = enthusiastic_fossil_fuel_industry
set_country_flag = positive_international_bankers
set_country_flag = negative_farmers

#Nat focus
	complete_national_focus = bonus_tech_slots

set_politics = {

	parties = {
		democratic = { 
			popularity = 10
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 70
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 20
		}
	}
	
	ruling_party = communism
	last_election = "2016.3.20"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Denis Sassou Nguesso"
	desc = "POLITICS_Mohamed_Cheikh Biadillah_DESC" 
	picture = "CGO_Denis_Sassou_Nguesso.dds"
	expire = "2050.1.1"
	ideology = Conservative
	traits = {
		#
	}
}
}