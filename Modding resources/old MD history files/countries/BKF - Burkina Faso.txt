﻿capital = 751

oob = "BKF_2000"

set_convoys = 0
set_stability = 0.5

set_country_flag = country_language_french
set_country_flag = country_language_fula
set_country_flag = country_language_jula
set_country_flag = country_language_more

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	african_union_member
}

set_politics = {

	parties = {
		nationalist = {
			popularity = 50
		}
		social_liberal = {
			popularity = 25
		}
		conservative = {
			popularity = 10
		}
		communist = {
			popularity = 15
		}
	}
	
	ruling_party = nationalist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Blaise Compaore"
	picture = "Blaise_Compaore.dds"
	ideology = autocrat
}
create_country_leader = {
	name = "Koch Marc Christian Kabore"
	picture = "Koch_Marc_Christian_Kabore.dds"
	ideology = right_wing_conservative
}

2014.1.1 = {
	set_politics = {
		parties = {
			conservative = {
				popularity = 40
			}
			social_liberal = {
				popularity = 30
			}
			communist = {
				popularity = 20
			}
			nationalist = {
				popularity = 10
			}
		}
		ruling_party = conservative
		elections_allowed = yes
	}
}
2017.1.1 = {

oob = "BFA_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	land_Drone_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	Anti_Air_0 = 1
	Early_APC = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
}

add_ideas = {
	pop_050
	crippling_corruption
	gdp_1
	sufi_islam
	    fast_growth
		defence_01
	edu_01
	health_02
	social_01
	bureau_04
	police_03
	volunteer_army
	volunteer_women
	industrial_conglomerates
	farmers
	international_bankers
	hybrid
	}
set_country_flag = gdp_1
set_country_flag = positive_farmers
set_country_flag = positive_international_bankers

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4_IC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 38
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 8
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 56
		}
	}
	
	ruling_party = neutrality
	last_election = "2015.11.29"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Roch Marc Christian Kabore"
	desc = ""
	picture = "BUF_Roch_Marc_Christian_Kabore.dds"
	expire = "2065.1.1"
	ideology = neutral_Social
	traits = {
		#
	}
}

create_field_marshal = {
	name = "Honoré Traore"
	picture = "Portrait_Honore_Traore_army.dds"
	traits = { old_guard inspirational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Gilbert Diendere"
	picture = "Portrait_Gilbert_Diendere_army.dds"
	traits = { old_guard offensive_doctrine }
	skill = 3
}

create_field_marshal = {
	name = "Yacouba Isaac Zida"
	picture = "Portrait_Yacouba_Zida.dds"
	traits = { old_guard organisational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Oumarou Sadou"
	picture = "Portrait_Oumarou_Sadou.dds"
	traits = { thorough_planner }
	skill = 2
}

create_field_marshal = {
	name = "Jean Claude Bouda"
	picture = "Portrait_Jean_Claude_Bouda.dds"
	traits = { defensive_doctrine }
	skill = 2
}

create_field_marshal = {
	name = "Pingrenoma Zagré"
	picture = "Portrait_Pingrenoma_Zagre.dds"
	traits = { organisational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Yaya Sere"
	picture = "Portrait_Yaya_Sere.dds"
	traits = { fast_planner }
	skill = 2
}

create_corps_commander = {
	name = "Gaoussou Coulibaly"
	picture = "Portrait_Gaoussou_Coulibaly.dds"
	traits = { desert_fox }
	skill = 1
}

create_corps_commander = {
	name = "Moise Minningou"
	picture = "Portrait_Moise_Minningou.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Omer Marie Bruno Tapsoba"
	picture = "Portrait_Omer_Tapsoba.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Ahmed Rouamba"
	picture = "Portrait_Ahmed_Rouamba.dds"
	traits = { jungle_rat }
	skill = 1
}

create_corps_commander = {
	name = "Didier Bamouni"
	picture = "Portrait_Didier_Bamouni.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Léon Traore"
	picture = "Portrait_Leon_Traore.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Moussa Diallo"
	picture = "Portrait_Moussa_Diallo.dds"
	traits = { jungle_rat }
	skill = 1
}

create_corps_commander = {
	name = "Claude Kabore"
	picture = "Portrait_Claude_Kabore.dds"
	traits = { desert_fox }
	skill = 1
}

create_corps_commander = {
	name = "Mahamadi Bonkoungou"
	picture = "Portrait_Mahamadi_Bonkoungou.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Boureima Kéré"
	picture = "Portrait_Boureima_Kere.dds"
	traits = { commando ranger }
	skill = 1
}

create_corps_commander = {
	name = "Raboyinga Kabore"
	picture = "Portrait_Raboyinga_Kabore.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Philippe Bonkoungou"
	picture = "Portrait_Philippe_Bonkoungou.dds"
	traits = { commando urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Yipènè Djibril Bassolet"
	picture = "Portrait_Ypene_Djibrill_Bassole.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Oumarou Sawadogo"
	picture = "Portrait_Oumarou_Sawadogo.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Théophile Nikiéma"
	picture = "Portrait_Teophile_Nikiema.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Barro Gnibanga"
	picture = "Portrait_Barro_Gnibanga.dds"
	traits = { commando desert_fox }
	skill = 1
}

create_corps_commander = {
	name = "T.N. Pale"
	picture = "Portrait_TN_Pale.dds"
	traits = { trait_engineer }
	skill = 1
}

create_navy_leader = {
	name = "Honoré Traore"
	picture = "Portrait_Honore_Traore_navy.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Gilbert Diendere"
	picture = "Portrait_Gilbert_Diendere_navy.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Yacouba Isaac Zida"
	picture = "Portrait_Isaac_Zida_navy.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Pingrenoma Zagré"
	picture = "Portrait_Pingrenoma_Zagre_navy.dds"
	traits = {  }
	skill = 1
}

create_equipment_variant = {
	name = "Panhard AML"
	type = Rec_tank_Equipment_0
	upgrades = {
		tank_reliability_upgrade = 1
		tank_engine_upgrade = 1
		tank_armor_upgrade = 0
		tank_gun_upgrade = 2
	}
}

create_equipment_variant = {
	name = "Panhard M3"
	type = Rec_tank_Equipment_0
	upgrades = {
	}
}

}