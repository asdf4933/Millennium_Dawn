﻿capital = 527

oob = "PRK_2000"

set_convoys = 270
set_stability = 0.5

set_country_flag = country_language_korean

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	corvette1 = 1
	corvette2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1

	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
	war_economy
	service_by_requirement
	closed_economy
}

add_opinion_modifier = {
	target = JAP
	modifier = past_japanese_war_crimes
}

add_opinion_modifier = {
	target = KOR
	modifier = rival
}

add_opinion_modifier = {
	target = KOR
	modifier = rival_trade
}

set_politics = {

	parties = {
		
		conservative = {
			popularity = 5
		}
		social_liberal = {
			popularity = 6
		}
		social_democrat = {
			popularity = 7
		}
		progressive = {
			popularity = 8
		}
		democratic_socialist = {
			popularity = 19
		}
		communist = {
			popularity = 55
		}
	}
	
	ruling_party = communist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Kim Jong-Il"
	picture = "Kim_Jong_Il.dds"
	ideology = juche
}

add_namespace = {
	name = "prk_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Ri Myong Su"
	picture = "generals/Ri_Myong_Su.dds"
	traits = { old_guard defensive_doctrine }
	skill = 2
}

create_field_marshal = {
	name = "Ri Yong Gil"
	picture = "generals/Ri_Yong_Gil.dds"
	traits = { logistics_wizard offensive_doctrine }
	skill = 1
}

create_corps_commander = {
	name = "Hyon Yong Chol"
	picture = "generals/Hyon_Yong_Chol.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Cho Ryong Hae"
	picture = "generals/Cho_Ryong_Hae.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Choe Pu Il"
	picture = "generals/Choe_Pu_Il.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Hwang Pyong So"
	picture = "generals/Hwang_Pyong_So.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Jo Chun Ryong"
	picture = "generals/Jo_Chun_Ryong.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Kim Chun Sop"
	picture = "generals/Kim_Chun_Sop.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Jo Kyong Chol"
	picture = "generals/Jo_Kyong_Chol.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Kim Kyok Sik"
	picture = "generals/Kim_Kyok_Sik.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Kim Myong Kuk"
	picture = "generals/Kim_Myong_Kuk.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Kim Won Hong"
	picture = "generals/Kim_Won_Hong.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Kim Yong Chun"
	picture = "generals/Kim_Yong_Chun.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Pak Yong Sik"
	picture = "generals/Pak_Yong_Sik.dds"
	traits = {  }
	skill = 1
}

2011.12.1 = {
	set_country_flag = prk_death_of_kim_jong_il
	create_country_leader = {
		name = "Kim Jong-Un"
		picture = "Kim_Jong_Un.dds"
		ideology = juche
	}
}

2014.1.1 = {
	set_technology = {
	}
	add_named_threat = {
		threat = 3
		name = threat_PRK_nuclear_program
	}
}