﻿capital = 44

oob = "ALB_2000"

set_convoys = 5
set_stability = 0.5

set_country_flag = country_language_albanian

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	corvette3 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_stagnation
}

set_politics = {

	parties = {
		nationalist = { popularity = 1 }
		reactionary = { popularity = 2 }
		conservative = { popularity = 23 }
		social_democrat = { popularity = 31 }
		progressive = { popularity = 3 }
		democratic_socialist = { popularity = 16 }
		communist = { popularity = 2 }
	}
	
	ruling_party = social_democrat
	last_election = "1997.6.29"
	election_frequency = 48
	elections_allowed = yes
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

create_country_leader = {
	name = "Rexhep Meidani"
	picture = "Rexhep_Meidani.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Fatmir Mediu"
	picture = "Fatmir_Mediu.dds"
	ideology = constitutionalist 
}

create_country_leader = {
	name = "Shpetim Idrizi"
	picture = "Shpetim_Idrizi.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Ervin Mete"
	picture = "Ervin_Mete.dds"
	ideology = liberalist 
}
	
create_country_leader = {
	name = "Leka I"
	picture = "Leka_I.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Edlir Petanaj"
	picture = "Edlir_Petanaj.dds"
	ideology = green
}

create_country_leader = {
	name = "Petrit Vasili"
	picture = "Petrit_Vasili.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Adriatik Alimadhi"
	picture = "Adriatik_Alimadhi.dds"
	ideology = proto_fascist 
}

create_country_leader = {
	name = "Adriatik Alimadhi"
	picture = "Adriatik_Alimadhi.dds"
	ideology = proto_fascist 
}

create_country_leader = {
	name = "Idajet Beqiri"
	picture = "Idajet_Beqiri.dds"
	ideology = fascist_ideology  
}

create_country_leader = {
	name = "Idajet Beqiri"
	picture = "Idajet_Beqiri.dds"
	ideology = fascist_ideology  
}

create_country_leader = {
	name = "Hysni Milloshi"
	picture = "Hysni_Milloshi.dds"
	ideology = stalinist   
}

create_field_marshal = {
	name = "Jeronim Bazo"
	picture = "generals/Jeronim_Bazo.dds"
	traits = { old_guard organisational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Dedë Prenga"
	picture = "generals/Dede_Prenga.dds"
	traits = { inspirational_leader }
	skill = 1
}

create_corps_commander = {
	name = "Nazmi Cahani"
	picture = "generals/Nazmi_Cahani.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Dhori Spirollari"
	picture = "generals/Dhori_Spirollari.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Ekland Dauti"
	picture = "generals/Ekland_Dauti.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Manushaqe Shehu"
	picture = "generals/Manushaqe_Shehu.dds"
	traits = { trickster }
	skill = 1
}

create_navy_leader = {
	name = "Ylber Dogjani"
	picture = "admirals/Ylber_Dogjani.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 2
}

create_navy_leader = {
	name = "Kristaq Gërveni"
	picture = "admirals/Kristaq_Gerveni.dds"
	traits = { blockade_runner }
	skill = 2
}

2016.1.1 = {
	create_country_leader = {
		name = "Bujar Nishani"
		picture = "Bujar_Nishani.dds"
		ideology = social_democrat_ideology
	}
}
2017.1.1 = {

oob = "ALB_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	night_vision_1 = 1
	night_vision_2 = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	gw_artillery = 1
	Anti_Air_0 = 1
	util_vehicle_equipment_0 = 1
	Heavy_Anti_tank_0 = 1
}

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 60
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 5
		}
		
		neutrality = {
			popularity = 25
		}
		
		nationalist = {
			popularity = 10
		}
	}
	
	ruling_party = democratic
	last_election = "2013.6.23"
	election_frequency = 48
	elections_allowed = yes
}

add_ideas = {
	pop_050
	#small_medium_business_owners
	#Labour_Unions
	#Mass_Media
	unrestrained_corruption
	gdp_3
	sunni
		stable_growth
		defence_01
	edu_02
	health_02
	social_02
	bureau_02
	police_03
	volunteer_army
	volunteer_women
	NATO_member
	international_bankers
	landowners
	industrial_conglomerates
	civil_law
}
set_country_flag = gdp_3
set_country_flag = positive_international_bankers
set_country_flag = positive_landowners

#NATO military access
diplomatic_relation = {
	country = BEL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BUL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CAN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CRO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CZE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = DEN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = EST
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = FRA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GER
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GRE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HUN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ICE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ITA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LAT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LIT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LUX
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HOL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = NOR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ROM
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLV
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SPR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = TUR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ENG
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = USA
	relation = military_access
	active = yes
}

set_convoys = 5


create_country_leader = {
	name = "Lulzim Basha" 
	desc = "POLITICS_EARL_BROWDER_DESC"
	picture = "lulzim_basha.dds"
	expire = "2065.1.1"
	ideology = conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Collective leadership"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "collective_leadership.dds"
	expire = "2065.1.1"
	ideology = Communist-State
	traits = {
		#
	}
}

create_country_leader = {
	name = "Ilir Meta"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "ilir_meta.dds"
	expire = "2065.1.1"
	ideology = neutral_Social
	traits = {
		#
	}
}

create_country_leader = {
	name = "Shpetim Idrizi"
	desc = "POLITICS_FRANKLIN_DELANO_ROOSEVELT_DESC"
	picture = "shpetim_idrizi.dds"
	expire = "2065.1.1"
	ideology = Nat_Populism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Edi Rama"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "ALB-edi-rama.dds"
	expire = "2065.1.1"
	ideology = socialism
	traits = {
		#
	}
}

create_field_marshal = {
	name = "Bardhyl Kollcaku"
	picture = "Portrait_Bardhyl_Kollcaku.dds"
	traits = { organisational_leader }
	skill = 1
}

create_field_marshal = {
	name = "Vladimir Avdiaj"
	picture = "Portrait_Vladimir_Avdiaj.dds"
	traits = { logistics_wizard }
	skill = 1
}

create_field_marshal = {
	name = "Jeronim Bazo"
	picture = "Portrait_Jeronim_Bazo.dds"
	traits = { old_guard inspirational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Dedë Prenga"
	picture = "Portrait_Dede_Prenga.dds"
	traits = { thorough_planner }
	skill = 1
}

create_field_marshal = {
	name = "Manushaqe Shehu"
	picture = "Portrait_Manushaqe_Shehu.dds"
	traits = { fast_planner }
	skill = 1
}

create_corps_commander = {
	name = "Nazmi Cahani"
	picture = "Portrait_Nazmi_Cahani.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Dhori Spirollari"
	picture = "Portrait_Dhori_Spirollari.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Ekland Dauti"
	picture = "Portrait_Ekland_Dauti.dds"
	traits = { commando urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Qemal Shkurti"
	picture = "Portrait_Qemal_Shkurti.dds"
	traits = { naval_invader }
	skill = 1
}

create_corps_commander = {
	name = "Ardian Bali"
	picture = "Portrait_Ardian_Bali.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Luan Hoxha"
	picture = "Portrait_Luan_Hoxha.dds"
	traits = { bearer_of_artillery }
	skill = 2
}

create_navy_leader = {
	name = "Ylber Dogjani"
	picture = "Portrait_Ylber_Dogjani.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 2
}

create_navy_leader = {
	name = "Kristaq Gërveni"
	picture = "Portrait_Kristaq_Gerveni.dds"
	traits = { blockade_runner }
	skill = 2
}

create_navy_leader = {
	name = "Kudret Çela"
	picture = "Portrait_Kudret_Cela.dds"
	traits = { spotter }
	skill = 2
}


}
