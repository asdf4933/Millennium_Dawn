﻿capital = 448

oob = "LBA_2000"

set_convoys = 300
set_stability = 0.5

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_opinion_modifier = {
	target = ISR
	modifier = death_to_israel
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

add_ideas = {
	population_growth_rapid
	african_union_member
	arab_league_member
	extensive_conscription
}

set_politics = {

	parties = {
		islamist = { popularity = 15 }
		nationalist = { popularity = 40 }
		reactionary = { popularity = 2 }
		conservative = { popularity = 5 }
		social_liberal = { popularity = 2 }
		communist = { popularity = 20 }
		market_liberal = { popularity = 2 }
		progressive  = { popularity = 4 }
		social_democrat = { popularity = 5 }
		democratic_socialist  = { popularity = 5 }
	}
	
	ruling_party = nationalist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Muammar al-Gaddafi"
	picture = "Muammar_Gaddafi.dds"
	ideology = autocrat
	expire = "2011.1.1"
}

create_country_leader = {
	name = "Abubakr Mustafa Buera"
	picture = "Abubakr_Mustafa_Buera.dds"
	ideology = fiscal_conservative
}
create_country_leader = {
	name = "Mohammed Sawan"
	picture = "Mohammed_Sawan.dds"
	ideology = islamic_republican
} 
create_country_leader = {
    name = "Mustafa Abdul Jalil"
	picture = "Mustafa_Abdul_Jalil.dds"
	ideology = social_democrat_ideology
}
create_country_leader = {
	name = "Mohammed El-Senussi"
	ideology = absolute_monarchist
	picture = "Mohammed_Senussi.dds"
}

create_country_leader = {
	name = "Ali Tarhouni"
	ideology = centrist 
	picture = "Ali_Tarhouni.dds"
}
create_country_leader = {
	name = "Mahmoud Jibril"
	picture = "Mahmoud_Jibril.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Mohammed Kilani"
	picture = "Mohammed_Kilani.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Uthman Ali"
	picture = "Uthman_Ali.dds"
	ideology = marxist
}   

create_country_leader = {
	name = "Mohamed Ali Abdallah"
	picture = "Mohamed_Ali_Abdallah.dds"
	ideology = green
}

create_country_leader = {
	name = "Mousa Kussa"
	picture = "Mousa_Kussa.dds"
	ideology = national_socialist
}

create_field_marshal = {
	name = "Khalifa Haftar"
	picture = "generals/Khalifa_Haftar.dds"
	traits = { old_guard thorough_planner }
	skill = 2
}

create_corps_commander = { 
	name = "Yousef Mangoush"
	picture = "generals/Yousef_Mangoush.dds"
	traits = { panzer_leader trait_engineer }
	skill = 2
}

create_corps_commander = { 
	name = "Khamis Gaddafi"
	picture = "generals/Khamis_Gaddafi.dds"
	traits = { desert_fox }
	skill = 1
}

create_corps_commander = { 
	name = "Suleiman Mahmoud"
	picture = "generals/Suleiman_Mahmoud.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = { 
	name = "Abu-Bakr Younis Jaber"
	picture = "generals/Abubakr_Jaber.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = { 
	name = "Mohamed El-Mismari"
	picture = "generals/Mohamed_El_Mismari.dds"
	traits = { desert_fox }
	skill = 1
}

create_corps_commander = { 
	name = "Fatah Younis Al-Obeidi"
	picture = "generals/Fatah_Al-Obeidi.dds"
	traits = { trickster urban_assault_specialist }
	skill = 2
}

create_corps_commander = { 
	name = "Ahmed Oun"
	picture = "generals/Ahmed_Oun.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = { 
	name = "Mohammed Bougfir"
	picture = "generals/Mohammed_Bougfir.dds"
	traits = { bearer_of_artillery }
	skill = 2
}

create_corps_commander = { 
	name = "Al-Saadi Gaddafi"
	picture = "generals/Al-Saadi_Gaddafi.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = { 
	name = "Massoud Abdelhafid"
	picture = "generals/Massoud_Abdelhafid.dds"
    traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = { 
	name = "Al-Mahdi Al-Barghathi"
	picture = "generals/Al-Mahdi_Al-Barghathi.dds"
	traits = { ranger hill_fighter }
	skill = 1
}

create_navy_leader = { 
	name = "Abdolazim Ahmad"
	picture = "admirals/Abdolazim_Ahmad.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 2
}

2012.1.1 = {
	set_politics = {
		parties = {
			communist = { popularity = 6 }
			social_democrat = { popularity = 30 }
			islamist = { popularity = 23 }
			conservative = { popularity = 28 }
			market_liberal = { popularity = 10 }
			reactionary = { popularity = 14 }
			nationalist = { popularity = 11 }
		}
		ruling_party = social_democrat
		elections_allowed = yes
		last_election = "2011.12.1"
	}	
	create_country_leader = {
	name = "Fayez al-Sarraj"
	picture = "Fayez_al_Sarraj.dds"
	ideology = social_democrat_ideology
    }
	create_country_leader = {
	name = "Saif al-Islam Gaddafi"
	picture = "Saif_Islam_Gaddafi.dds"
	ideology = national_democrat
    }
}
2014.8.5 = {
    create_country_leader = {
        name = "Aguila Saleh Issa"
	    picture = "Aguila_Saleh_Issa.dds"
	    ideology = fiscal_conservative
    }
}