﻿capital = 302

oob = "BOL_2000"

set_stability = 0.5

set_country_flag = country_language_spanish

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	limited_conscription
}

set_politics = {

	parties = {
		islamist = {
			popularity = 1
		}
		nationalist = {
			popularity = 30
		}
		reactionary = {
			popularity = 2
		}
		conservative = {
			popularity = 1
		}
		market_liberal = {
			popularity = 6
		}
		social_liberal = {
			popularity = 25
		}
		social_democrat = {
			popularity = 5
		}
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 10
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = nationalist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Hugo Banzer"
	picture = "Hugo_Banzer.dds"
	ideology = proto_fascist
	expire = "2002.5.5"
}

create_country_leader = {
	name = "Gustavo Sejas Revollo"
	picture = "Gustavo_Sejas_Revollo.dds"
	ideology = falangist 
}

create_country_leader = {
	name = "Evo Morales"
	picture = "Evo_Morales.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Jorge Quiroga"
	picture = "Jorge_Quiroga.dds"
	ideology = christian_democrat  
}

create_country_leader = {
	name = "Samuel Jorge Doria Medina Auza"
	picture = "Samuel_Jorge_Doria_Medina_Auza.dds"
	ideology = libertarian  
}

create_country_leader = {
	name = "Ignacio Mendoza"
	picture = "Ignacio_Mendoza.dds"
	ideology = marxist   
}

create_country_leader = {
	name = " Juan del Granado"
	picture = "Juan_Del_Granado.dds"
	ideology = progressive_ideology   
}

2002.5.5 = {
	create_country_leader = {
		name = "Freddy Terrazas Salas"
		ideology = autocrat
		picture = "Freddy_Terrazas_Salas.dds"
	}
}

2014.10.13 = {
	set_politics = {

		parties = {
			conservative = {
				popularity = 10
			}
			market_liberal = {
				popularity = 25
			}
			progressive = {
				popularity = 3
			}
			democratic_socialist = {
				popularity = 62
			}
		}
	
		ruling_party = democratic_socialist
		last_election = "2014.10.12"
		election_frequency = 48
		elections_allowed = yes
	}
}

2017.1.1 = {

oob = "BOL_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	night_vision_2 = 1

	#For Templates
	infantry_weapons = 1
	
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
}

add_ideas = {
	pop_050
	unrestrained_corruption
	christian
	gdp_3
		fast_growth
		defence_02
	export_economy
	edu_04
	health_04
	social_03
	bureau_01
	police_03
	draft_army
	drafted_women
	international_bankers
	industrial_conglomerates
	civil_law
	cartels_3
}
set_country_flag = bud_neg_8
set_country_flag = gdp_3
set_country_flag = positive_international_bankers
set_country_flag = positive_industrial_conglomerates
set_country_flag = positive_fossil_fuel_industry

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot

	set_politics = {

	parties = {
		democratic = { 
			popularity = 38
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 52
		}
		
		neutrality = { 
			popularity = 10
		}
	}
	
	ruling_party = communism
	last_election = "2014.10.12"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Evo Morales"
	desc = ""
	picture = "BOL_evo-morales.dds"
	expire = "2065.1.1"
	ideology = anarchist_communism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Luis Orlando Ariñez Bazán"
	picture = "Portrait_Luis_Bazan.dds"
	traits = { organisational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Marcelo Antezana"
	picture = "Portrait_Marcelo_Antezana.dds"
	traits = { old_guard inspirational_leader }
	skill = 4
}

create_field_marshal = {
	name = "Óscar Naranjo"
	picture = "Portrait_Oscar_Naranjo.dds"
	traits = { old_guard offensive_doctrine }
	skill = 4
}

create_field_marshal = {
	name = "Juan Gonzalo Duran Flores"
	picture = "Portrait_Juan_Flores.dds"
	traits = { old_guard fast_planner }
	skill = 4
}

create_corps_commander = {
	name = "Carlos Erik Rück Arzabe"
	picture = "Portrait_Carlos_Arzabe.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Melvin Arteaga Aguada"
	picture = "Portrait_Melvin_Aguada.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Abel Galo de la Barra Cáceres"
	picture = "Portrait_Abel_Caceres.dds"
	traits = { urban_assault_specialist }
	skill = 4
}

create_corps_commander = {
	name = "Gina Reque Terán"
	picture = "Portrait_Gina_Teran.dds"
	traits = { ranger }
	skill = 3
}

create_corps_commander = {
	name = "Luis Fernando Aramayo Mercado"
	picture = "Portrait_Luis_Mercado.dds"
	traits = { trickster }
	skill = 3
}

create_corps_commander = {
	name = "Felipe Eduardo Vasquez Moya"
	picture = "Portrait_Felipe_Moya.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Guillermo Chalup Liendo"
	picture = "Portrait_Guillermo_Liendo.dds"
	traits = { fortress_buster }
	skill = 1
}

create_navy_leader = {
	name = "José Manuel Puente Guarachi"
	picture = "Portrait_Jose_Guarachi.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 4
}

}