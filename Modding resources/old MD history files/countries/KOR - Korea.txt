﻿capital = 525 #Seoul

oob = "KOR_2000"

set_convoys = 580
set_stability = 0.5

set_country_flag = country_language_korean

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	missile_destroyer_2 = 1
	missile_destroyer_3 = 1

	cruiser_1 = 1
	cruiser_2 = 1
	missile_cruiser_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_stagnation
	extensive_conscription
	free_trade
}

add_opinion_modifier = {
	target = JAP
	modifier = past_japanese_war_crimes
}

add_opinion_modifier = {
	target = PRK
	modifier = rival
}

add_opinion_modifier = {
	target = PRK
	modifier = rival_trade
}

set_politics = {

	parties = { 
		islamist = { popularity = 0 } 
		nationalist = { popularity = 2 } 
		reactionary = { popularity = 0 } 
		conservative = { popularity = 30 } 
		market_liberal = { popularity = 10 } 
		social_liberal = { popularity = 45 } 
		social_democrat = { popularity = 5 } 
		progressive = { popularity = 5 } 
		democratic_socialist = { popularity = 3 } 
		communist = { popularity = 0 } 
	} 
	
	ruling_party = social_liberal
	last_election = "1998.12.5"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Kim Dae-jung"
	picture = "Kim_Dae_Jung.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Lee Hoi-chang"
	picture = "Lee_Hoi_Chang.dds"
	ideology = constitutionalist
}

create_country_leader = {
	name = "Hong Se-hwa"
	picture = "Hong_Se_Hwa.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Park Jie-won"
	picture = "Park_Jie_Won.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Sin Sang-jung"
	picture = "Sim_Sang_Jun.dds"
	ideology = progressive_ideology
}

create_country_leader = {
	name = "Ha Seung-soo"
	picture = "Ha_Seung_Soo.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Geum Min"
	picture = "Geum_Min.dds"
	ideology = marxist
}

create_corps_commander = {
	name = "Han Min-goo"
	picture = "generals/Han_Min_Goo.dds"
	skill = 2
}

create_corps_commander = {
	name = "Jang Jun-gyu"
	picture = "generals/Jang_Jun_Gyu.dds"
	skill = 2
}

create_corps_commander = {
	name = "Jeong Seung-jo"
	picture = "generals/Jeong_Seung_Jo.dds"
	skill = 1
}

create_corps_commander = {
	name = "Kim Kwan-jin"
	picture = "generals/Kim_Kwan_Jin.dds"
	skill = 1
}

create_corps_commander = {
	name = "Lee Sang-hoon"
	picture = "generals/Lee_Sang_Hoon.dds"
	skill = 1
}

create_corps_commander = {
	name = "Lee Soon-jin"
	picture = "generals/Lee_Soon_Jin.dds"
	skill = 1
}

create_navy_leader = {
	name = "Jung Ho-sub"
	picture = "admirals/Jung_Ho_Sub.dds"
	skill = 2
}

create_navy_leader = {
	name = "Lee Ki-sik"
	picture = "admirals/Lee_Ki_Sik.dds"
	skill = 1
}

2015.10.1 = {
	set_politics = {
		parties = { 
			islamist = { popularity = 0 } 
			nationalist = { popularity = 0 } 
			reactionary = { popularity = 0 } 
			conservative = { popularity = 48 } 
			market_liberal = { popularity = 7 } 
			social_liberal = { popularity = 35 } 
			social_democrat = { popularity = 0 } 
			progressive = { popularity = 2 } 
			democratic_socialist = { popularity = 0 } 
			communist = { popularity = 0 } 
		}
		ruling_party = conservative
		last_election = "2012.4.11"
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Park Geun-hye"
		picture = "Park_Gyeung_He.dds"
		ideology = constitutionalist
	}

	create_country_leader = {
		name = "Moon Jae-in"
		picture = "Moon_Jae_In.dds"
		ideology = liberalist
	}
}