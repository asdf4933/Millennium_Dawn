﻿capital = 640

oob = "MYA_2000"

set_convoys = 320
set_stability = 0.5

set_country_flag = country_language_burmese

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
	extensive_conscription
}

set_politics = {

	parties = {
		
		islamist = {
			popularity = 1
		}
		nationalist = {
			popularity = 35
		}
		conservative = {
			popularity = 10
		}
		market_liberal = {
			popularity = 1
		}
		social_liberal = {
			popularity = 30
		}
		social_democrat = {
			popularity = 5
		}
		progressive = {
			popularity = 1
		}
		democratic_socialist = {
			popularity = 1
		}
		communist = {
			popularity = 20
		}
	}
	
	ruling_party = nationalist
	last_election = "1990.5.27"
	election_frequency = 60
	elections_allowed = no
}

2000.1.1 = {

	create_country_leader = {
		name = "Than Shwe"
		picture = "Than_Shwe.dds"
		ideology = autocrat
	}

	create_country_leader = {
		name = "Aung San Suu Kyi"
		picture = "Aung_San_Suu_Kyi.dds"
		ideology = liberalist
	}
	
	create_country_leader = {
		name = "Kyaw Swar Soe"
		picture = "Kyaw_Swar_Soe.dds"
		ideology = social_democrat_ideology
	}

	create_country_leader = {
		name = "Khin Maung Swe"
		picture = "Khin_Maung_Swe.dds"
		ideology = progressive_ideology
	}

	create_country_leader = {
		name = "Thein Sein"
		picture = "Thein_Sein.dds"
		ideology = constitutionalist
	}

	create_corps_commander = {
		name = "Sein Win"
		picture = "generals/Sein_Win.dds"
		skill = 2
	}

	create_corps_commander = {
		name = "Maung Aye"
		picture = "generals/Maung_Aye.dds"
		skill = 1
	}

	create_corps_commander = {
		name = "Min Aung Hlaing"
		picture = "generals/Min_Aung_Hlaing.dds"
		skill = 1
	}

	create_corps_commander = {
		name = "Soe Win"
		picture = "generals/Soe_Win.dds"
		skill = 1
	}

	create_navy_leader = {
		name = "Thura Thet Swe"
		picture = "admirals/Thura_Thet_Swe.dds"
		skill = 1
	}

	create_navy_leader = {
		name = "Tin Aung San"
		picture = "admirals/Tin_Aung_San.dds"
		skill = 1
	}

}

2015.11.8 = {

	add_ideas = {
		volunteer_only
	}
	
	set_politics = {

	parties = {
		
		islamist = {
			popularity = 0
		}
		nationalist = {
			popularity = 0
		}
		reactionary = {
			popularity = 25
		}
		conservative = {
			popularity = 10
		}
		market_liberal = {
			popularity = 0
		}
		social_liberal = {
			popularity = 60
		}
		social_democrat = {
			popularity = 0
		}
		progressive = {
			popularity = 3
		}
		democratic_socialist = {
			popularity = 0
		}
		communist = {
			popularity = 2
		}
	}
	
	ruling_party = social_liberal
	last_election = "2015.11.8"
	election_frequency = 60
	elections_allowed = yes
}
}