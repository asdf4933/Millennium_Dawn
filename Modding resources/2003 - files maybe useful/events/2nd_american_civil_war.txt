#########################################################
#							#
# 2nd American Civil War event chain by danielshannon   #
# modified by Zokan                                     #
#                                			#
#########################################################

#########
# Index #
#########################################################
#From	- To	#	Name 			 
#########################################################
#99501 - 100000# Main events		
#########################################################

############################################################################################################################

###################################################
# Socialist revolution in the USA
###################################################

event = {
	id = 99501
	random = no
	country = USA

	name = "EVT_99501_NAME"
	desc = "EVT_99501_DESC"
	style = 0
	picture = "american_militia"

	trigger = {
		OR = {
			AND = {
				government = democratic
				dissent = 30
				atwar = no
			}
			AND = {
				government = democratic
				dissent = 20
				OR = {
					war = { country = RUS country = USA }
					war = { country = CHC country = USA }
				}
			}
		}
	}

	date = { day = 1 month = january year = 2004 }
	offset = 25 # Check for this every 25 days
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "We will prevail"
		command = { type = leave_alliance }
		command = { type = independence which = U22 value = 1 } #Socialist States of America
		command = { type = trigger which = 99502 }
		command = { type = sleepteam which = 17504 } #Colt
		command = { type = sleepteam which = 17509 } #General Electric
		command = { type = sleepteam which = 17513 } #Argonne National Labs
		command = { type = sleepteam which = 17516 } #United Technologies
		command = { type = sleepteam which = 17525 } #Bath Iron Works
		command = { type = sleepteam which = 17527 } #GD Electric Boat
	}
}
###################################################
# SSA leadership
###################################################

event = {
id = 99502
random = no
country = U22

name = "EVT_99502_NAME"
desc = "EVT_99502_DESC"
style = 0
 picture = "far_left_revolution_us"

	action_a = {
		name = "Sam Webb (CP USA)"
		ai_chance = 70
		command = { type = set_domestic which = democratic value = 3 }
		command = { type = set_domestic which = interventionism value = 4 }
		command = { type = set_domestic which = political_left value = 8 }
		command = { type = set_domestic which = free_market value = 3 }
		command = { type = set_domestic which = defense_lobby value = 3 }
		command = { type = set_domestic which = freedom value = 4 }
		command = { type = belligerence which = USA value = -1 }
		command = { type = headofstate which = 87597 }#Webb
		command = { type = headofgovernment which = 87613 }#Webb
		command = { type = foreignminister which = 87617 }#Brenner
		command = { type = armamentminister which = 87636 }#Alam
		command = { type = ministerofsecurity which = 87641 }#Ignatiev
		command = { type = ministerofintelligence which = 87664 }#Klonsky
		command = { type = chiefofstaff which = 87800 }#Prysner
		command = { type = chiefofarmy which = 87801 }#Prysner
		command = { type = chiefofnavy which = 87806 }#Mayers
		command = { type = chiefofair which = 87805 }#Goodrich
		command = { type = trigger which = 99503 }
	}
	action_b = {
		name = "Bob Avakian (RCP USA)"
		ai_chance = 30
		command = { type = set_domestic which = democratic value = 1 }
		command = { type = set_domestic which = interventionism value = 8 }
		command = { type = set_domestic which = political_left value = 10 }
		command = { type = set_domestic which = defense_lobby value = 9 }
		command = { type = set_domestic which = free_market value = 1 }
		command = { type = set_domestic which = freedom value = 2 }
		command = { type = belligerence which = USA value = -1 }
		command = { type = headofstate which = 87596 }#Avakian
		command = { type = headofgovernment which = 87614 }#Avakian
		command = { type = foreignminister which = 87619 }#Leupp
		command = { type = armamentminister which = 87636 }#Alam
		command = { type = ministerofsecurity which = 87642 }#Dix
		command = { type = ministerofintelligence which = 87659 }#Taylor
		command = { type = chiefofstaff which = 87668 }#Avakian
		command = { type = chiefofarmy which = 87674 }#Avakian
		command = { type = chiefofnavy which = 87804 }#Franklin
		command = { type = chiefofair which = 87802 }#Franklin
		command = { type = trigger which = 99504 }
	}
}
###################################################
# Rise of the SSA - Webb declares the Union a dead state
###################################################

event = {
id = 99503
random = no
country = U22

name = "EVT_99503_NAME"
desc = "EVT_99503_DESC"
style = 0
 picture = "usa"

	action_a = {
		name = "Long live the Socialist States of America"
		command = { type = war which = USA }
		command = { type = trigger which = 99505 }
		command = { type = add_division value = mechanized when = 2 }
		command = { type = add_division value = mechanized when = 2 }
		command = { type = add_division value = mechanized when = 2 }
		command = { type = add_division value = mechanized when = 2 }
		command = { type = add_division value = mechanized when = 2 }
		command = { type = add_corps which = "Chicago Defense Force" value = land where = 620 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Detroit Defense Force" value = land where = 616 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Columbus Defense Force" value = land where = 628 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Cleveland Defense Force" value = land where = 615 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Baltimore Defense Force" value = land where = 607 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Minneapolis Defense Force" value = land where = 693 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Hartford Defense Force" value = land where = 601 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Portland Defense Force" value = land where = 583 }
		command = { type = add_division value = garrison when = 8 }
	}
}
###################################################
# Rise of the SSA - Avakian declares the Union a dead state
###################################################

event = {
id = 99504
random = no
country = U22

name = "EVT_99504_NAME"
desc = "EVT_99504_DESC"
style = 0
 picture = "usa"

	action_a = {
		name = "Long live the Socialist States of America"
		command = { type = war which = USA }
		command = { type = trigger which = 99506 }
		command = { type = add_division value = mechanized when = 2 }
		command = { type = add_division value = mechanized when = 2 }
		command = { type = add_division value = mechanized when = 2 }
		command = { type = add_division value = mechanized when = 2 }
		command = { type = add_division value = mechanized when = 2 }
		command = { type = add_corps which = "Chicago Defense Force" value = land where = 620 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Detroit Defense Force" value = land where = 616 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Columbus Defense Force" value = land where = 628 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Cleveland Defense Force" value = land where = 615 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Baltimore Defense Force" value = land where = 607 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Minneapolis Defense Force" value = land where = 693 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Hartford Defense Force" value = land where = 601 }
		command = { type = add_division value = garrison when = 8 }
		command = { type = add_corps which = "Portland Defense Force" value = land where = 583 }
		command = { type = add_division value = garrison when = 8 }
	}
}
###################################################
# Rise of the SSA - Webb declares the Union a dead state
###################################################

event = {
id = 99505
random = no
country = USA

name = "EVT_99505_NAME"
desc = "EVT_99505_DESC"
style = 0
 picture = "usa"

	action_a = {
		name = "The Union will hold, God save America!"
		command = { type = dissent value = 1 }
	}
}
###################################################
# Rise of the SSA - Avakian declares the Union a dead state
###################################################

event = {
id = 99506
random = no
country = USA

name = "EVT_99506_NAME"
desc = "EVT_99506_DESC"
style = 0
 picture = "usa"

	action_a = {
		name = "The Union will hold, God save America!"
		command = { type = dissent value = 1 }
	}
}
###################################################
# California and Texas leave the Union
###################################################

event = {
	id = 99507
	random = no
	country = USA

	name = "EVT_99507_NAME"
	desc = "EVT_99507_DESC"
	style = 0
	picture = "congress"

	trigger = {
		exists = U22
		war = { country = U22 country = USA }
		OR = {
			event = 99505
			event = 99506
		}
		
	}

	date = { day = 1 month = january year = 2004 }
	offset = 12 # Check for this every 12 days
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "We will prevail"
		command = { type = dissent value = 1 }
		command = { type = independence which = U20 value = 1 } #Texas
		command = { type = independence which = U21 value = 1 } #California
		command = { type = trigger which = 99508 }
		command = { type = trigger which = 99509 }

		command = { type = sleepteam which = 17505 } #Northrop Grumman
		command = { type = sleepteam which = 17506 } #Lockheed

		command = { type = sleepteam which = 17508 } #Bell Helicopters
		command = { type = sleepteam which = 17517 } #Consolidated Robotics

		command = { type = sleepleader which = 87729 }
		command = { type = sleepleader which = 87730 }
		command = { type = sleepleader which = 87731 }
		command = { type = sleepleader which = 87732 }
		command = { type = sleepleader which = 87733 }	
		command = { type = sleepleader which = 87734 }
		command = { type = sleepleader which = 87735 }
		command = { type = sleepleader which = 87736 }
		command = { type = sleepleader which = 87737 }
		command = { type = sleepleader which = 87738 }
		command = { type = sleepleader which = 87739 }
		command = { type = sleepleader which = 87740 }
		command = { type = sleepleader which = 87741 }	
		command = { type = sleepleader which = 87742 }
		command = { type = sleepleader which = 87743 }
		command = { type = sleepleader which = 87744 }

		command = { type = sleepleader which = 87762 }
		command = { type = sleepleader which = 87763 }	
		command = { type = sleepleader which = 87764 }
		command = { type = sleepleader which = 87765 }
		command = { type = sleepleader which = 87766 }
		command = { type = sleepleader which = 87767 }
		command = { type = sleepleader which = 87768 }
		command = { type = sleepleader which = 87769 }
		command = { type = sleepleader which = 87770 }
		command = { type = sleepleader which = 87771 }	
		command = { type = sleepleader which = 87772 }
		command = { type = sleepleader which = 87773 }
	}
}
###################################################
# Texas is an independent nation!
###################################################

event = {
id = 99508
random = no
country = U20

name = "EVT_99508_NAME"
desc = "EVT_99508_DESC"
style = 0
 picture = "congress"

	action_a = {
		name = "Ok"
		command = { type = dissent value = -1 }

		command = { type = add_division value = interceptor when = 3 }
		command = { type = add_corps which = "Texas Army National Guard" value = land where = 715 }
		command = { type = add_division which = "36th Infantry Division" value = infantry when = 3 }
		command = { type = add_division which = "49th Armored Division" value = armor when = 15 where = heavy_armor }
		command = { type = add_division value = mechanized when = 3 }
		command = { type = add_division value = mechanized when = 3 }
		command = { type = add_division value = mechanized when = 3 }
		command = { type = add_division value = mechanized when = 3 }

		command = { type = set_domestic which = democratic value = 9 }
		command = { type = set_domestic which = political_left value = 1 }
		command = { type = set_domestic which = free_market value = 9 }
		command = { type = set_domestic which = freedom value = 7 }
		command = { type = set_domestic which = professional_army value = 6 }
		command = { type = set_domestic which = defense_lobby value = 5 }
		command = { type = set_domestic which = interventionism value = 4 }
		
		command = { type = headofstate which = 95001 }
		command = { type = headofgovernment which = 95002 }
		command = { type = foreignminister which = 95003 }
		command = { type = armamentminister which = 95004 }
		command = { type = ministerofsecurity which = 95005 }
		command = { type = ministerofintelligence which = 95006 }
		command = { type = chiefofstaff which = 95007 }
		command = { type = chiefofarmy which = 95008 }
		command = { type = chiefofnavy which = 95009 }
		command = { type = chiefofair which = 95010 }
	}
}
###################################################
# California is an independent nation!
###################################################

event = {
id = 99509
random = no
country = U21

name = "EVT_99509_NAME"
desc = "EVT_99509_DESC"
style = 0
 picture = "congress"

	action_a = {
		name = "Ok"
		command = { type = dissent value = -1 }

		command = { type = add_division value = interceptor when = 3 }
		command = { type = add_corps which = "California Army National Guard" value = land where = 762 }
		command = { type = add_division which = "40th Infantry Division" value = infantry when = 3 where = heavy_armor }
		command = { type = add_division value = mechanized when = 3 }
		command = { type = add_division value = mechanized when = 3 }
		command = { type = add_division value = mechanized when = 3 }
		command = { type = add_division value = mechanized when = 3 }
		
		command = { type = set_domestic which = democratic value = 9 }
		command = { type = set_domestic which = political_left value = 6 }
		command = { type = set_domestic which = free_market value = 9 }
		command = { type = set_domestic which = freedom value = 7 }
		command = { type = set_domestic which = professional_army value = 6 }
		command = { type = set_domestic which = defense_lobby value = 5 }
		command = { type = set_domestic which = interventionism value = 4 }
	
		command = { type = headofstate which = 95501 }
		command = { type = headofgovernment which = 95502 }
		command = { type = foreignminister which = 95503 }
		command = { type = armamentminister which = 95504 }
		command = { type = ministerofsecurity which = 95505 }
		command = { type = ministerofintelligence which = 95506 }
		command = { type = chiefofstaff which = 95507 }
		command = { type = chiefofarmy which = 95508 }
		command = { type = chiefofnavy which = 95509 }
		command = { type = chiefofair which = 95510 }
	}
}
###################################################
# Status of renegade states
###################################################

event = {
id = 99510
random = no
country = USA

name = "EVT_99510_NAME"
desc = "EVT_99510_DESC"
style = 0
 picture = "usa"

trigger = {
		OR = {
		    AND = {
			exists = U20
			exists = U21
			exists = U23
		    }	
		    AND = {
			exists = U20
			exists = U23
		    }		
		}
		NOT = {	
			war = { country = U21 country = USA }
			war = { country = U20 country = USA }
			war = { country = U23 country = USA }
	        }
		war = { country = U22 country = USA }
	}

	date = { day = 1 month = january year = 2004 }
	offset = 18 # Check for this every 18 days
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "Secession is illegal - this means war!"
		ai_chance = 50
		command = { type = war which = U20 }
		command = { type = war which = U21 }
		command = { type = war which = U23 }
	}
	action_b = {
		name = "We can't afford a war on two fronts, leave them be"
		ai_chance = 50
		command = { type = dissent value = 1 }
		command = { type = domestic which = interventionism value = -2 }
	}
}
###################################################
# Pacific States declare independence
###################################################

event = {
	id = 99511
	random = no
	country = USA

	name = "EVT_99511_NAME"
	desc = "EVT_99511_DESC"
	style = 0
	picture = "congress"

	trigger = {
		exists = U20
		exists = U21
		exists = U22
		war = { country = U22 country = USA }
		OR = {
			event = 99505
			event = 99506
		}
		
	}

	date = { day = 1 month = january year = 2004 }
	offset = 12 # Check for this every 12 days
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "We will prevail"
		command = { type = dissent value = 1 }
		command = { type = independence which = U23 value = 1 } #PSA
		command = { type = trigger which = 99512 }

		command = { type = sleepteam which = 17507 } #Boeing

		command = { type = sleepleader which = 87745 }
		command = { type = sleepleader which = 87746 }	
		command = { type = sleepleader which = 87747 }
		command = { type = sleepleader which = 87748 }
		command = { type = sleepleader which = 87749 }
		command = { type = sleepleader which = 87750 }
		command = { type = sleepleader which = 87751 }
		command = { type = sleepleader which = 87752 }
		command = { type = sleepleader which = 87753 }
		command = { type = sleepleader which = 87754 }	
		command = { type = sleepleader which = 87755 }
		command = { type = sleepleader which = 87756 }
		command = { type = sleepleader which = 87757 }
		command = { type = sleepleader which = 87758 }
		command = { type = sleepleader which = 87759 }	
		command = { type = sleepleader which = 87760 }
		command = { type = sleepleader which = 87761 }
	
	}
}
###################################################
# PSA formed
###################################################

event = {
id = 99512
random = no
country = U23

name = "EVT_99512_NAME"
desc = "EVT_99512_DESC"
style = 0
 picture = "congress"

	action_a = {
		name = "Ok"
		command = { type = dissent value = -1 }

		command = { type = add_division value = interceptor when = 3 }
		command = { type = add_division value = interceptor when = 3 }
		command = { type = add_corps value = land where = 780 }
		command = { type = add_division which = "81st Armor Brigade" value = light_armor when = 8 where = heavy_armor }
		command = { type = add_division value = mechanized when = 3 }
		command = { type = add_division value = mechanized when = 3 }
		command = { type = add_corps value = land where = 771 }
		command = { type = add_division which = "116th Armor Brigade" value = light_armor when = 8 where = heavy_armor }
		command = { type = add_division value = mechanized when = 3 }
		command = { type = add_division value = mechanized when = 3 }
		
		command = { type = set_domestic which = democratic value = 9 }
		command = { type = set_domestic which = political_left value = 6 }
		command = { type = set_domestic which = free_market value = 9 }
		command = { type = set_domestic which = freedom value = 7 }
		command = { type = set_domestic which = professional_army value = 6 }
		command = { type = set_domestic which = defense_lobby value = 5 }
		command = { type = set_domestic which = interventionism value = 4 }
	
		command = { type = headofstate which = 96501 }
		command = { type = headofgovernment which = 96502 }
		command = { type = foreignminister which = 96503 }
		command = { type = armamentminister which = 96504 }
		command = { type = ministerofsecurity which = 96505 }
		command = { type = ministerofintelligence which = 96506 }
		command = { type = chiefofstaff which = 96507 }
		command = { type = chiefofarmy which = 96508 }
		command = { type = chiefofnavy which = 96509 }
		command = { type = chiefofair which = 96510 }
	}
}
###################################################
# Nevada leaves the Union
###################################################

event = {
	id = 99513
	random = no
	country = USA

	name = "EVT_99513_NAME"
	desc = "EVT_99513_DESC"
	style = 0
	picture = "congress"

	trigger = {
		exists = U22
		exists = U23
		war = { country = U22 country = USA }
		OR = {
			event = 99505
			event = 99506
		}
		
	}

	date = { day = 1 month = january year = 2004 }
	offset = 12 # Check for this every 12 days
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "We will prevail"
		command = { type = dissent value = 1 }
		command = { type = secedeprovince which = U23 value = 768 }
		command = { type = secedeprovince which = U23 value = 734 }
		command = { type = trigger which = 99514 }
	}
}
###################################################
# Nevada leaves the Union
###################################################

event = {
	id = 99514
	random = no
	country = U23

	name = "EVT_99514_NAME"
	desc = "EVT_99514_DESC"
	style = 0
	picture = "congress"

	action_a = {
		name = "Excellent"
		command = { type = add_corps value = land where = 734 }
		command = { type = add_division value = mechanized when = 3 }
	}
}
###################################################
# Alaska leaves the Union
###################################################

event = {
	id = 99515
	random = no
	country = USA

	name = "EVT_99515_NAME"
	desc = "EVT_99515_DESC"
	style = 0
	picture = "congress"

	trigger = {
		exists = U22
		exists = U23
		war = { country = U22 country = USA }
		OR = {
			event = 99505
			event = 99506
		}
		
	}

	date = { day = 1 month = january year = 2004 }
	offset = 12 # Check for this every 12 days
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "We will prevail"
		command = { type = dissent value = 1 }
		command = { type = secedeprovince which = U23 value = 550 }
		command = { type = secedeprovince which = U23 value = 549 }
		command = { type = secedeprovince which = U23 value = 551 }
		command = { type = secedeprovince which = U23 value = 553 }
		command = { type = secedeprovince which = U23 value = 548 }
		command = { type = secedeprovince which = U23 value = 547 }
		command = { type = secedeprovince which = U23 value = 546 }
		command = { type = secedeprovince which = U23 value = 545 }
		command = { type = secedeprovince which = U23 value = 544 }
		command = { type = secedeprovince which = U23 value = 542 }
		command = { type = secedeprovince which = U23 value = 543 }
		command = { type = secedeprovince which = U23 value = 1408 }
		command = { type = trigger which = 99516 }
	}
}
###################################################
# Alaska leaves the Union
###################################################

event = {
	id = 99516
	random = no
	country = U23

	name = "EVT_99516_NAME"
	desc = "EVT_99516_DESC"
	style = 0
	picture = "congress"

	action_a = {
		name = "Excellent"
		command = { type = add_corps value = land where = 549 }
		command = { type = add_division value = mechanized when = 3 }
	}
}
###################################################
# Future of California
###################################################

event = {
	id = 99517
	random = no
	country = U21

	name = "EVT_99517_NAME"
	desc = "EVT_99517_DESC"
	style = 0
	picture = "congress"

	trigger = {
		exists = U22
		exists = U23
		war = { country = U21 country = USA }
		war = { country = U22 country = USA }
		war = { country = U23 country = USA }
		OR = {
			event = 99505
			event = 99506
		}
		
	}

	date = { day = 1 month = january year = 2004 }
	offset = 12 # Check for this every 12 days
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "Join the Pacific States of America"
		ai_chance = 95
		command = { type = trigger which = 99518 }
	}
	action_b = {
		name = "California will remain independent"
		ai_chance = 5
		command = { }
	}
}
###################################################
# California joins
###################################################

event = {
	id = 99518
	random = no
	country = U23

	name = "EVT_99518_NAME"
	desc = "EVT_99518_DESC"
	style = 0
	picture = "congress"

	action_a = {
		name = "Excellent"
		command = { type = inherit which = U21 }
		command = { type = dissent value = -1 }

		command = { type = waketeam which = 96518 }
		command = { type = waketeam which = 96519 }
		command = { type = waketeam which = 96520 }
		command = { type = waketeam which = 96521 }
		command = { type = waketeam which = 96522 }
		command = { type = waketeam which = 96523 }
		command = { type = waketeam which = 96524 }
		command = { type = waketeam which = 96525 }
		command = { type = waketeam which = 96526 }
		command = { type = waketeam which = 96527 }

		command = { type = wakeleader which = 95625 }
		command = { type = wakeleader which = 95626 }
		command = { type = wakeleader which = 95627 }
		command = { type = wakeleader which = 95628 }
		command = { type = wakeleader which = 95629 }
		command = { type = wakeleader which = 95630 }
		command = { type = wakeleader which = 95631 }
		command = { type = wakeleader which = 95632 }
		command = { type = wakeleader which = 95633 }
		command = { type = wakeleader which = 95634 }
		command = { type = wakeleader which = 95635 }
		command = { type = wakeleader which = 95636 }
  		command = { type = wakeleader which = 95637 }
		command = { type = wakeleader which = 95638 }
		command = { type = wakeleader which = 95639 }
		command = { type = wakeleader which = 95640 }
		command = { type = wakeleader which = 95641 }
		command = { type = wakeleader which = 95642 }
		command = { type = wakeleader which = 95643 }
		command = { type = wakeleader which = 95644 }
		command = { type = wakeleader which = 95645 }
		command = { type = wakeleader which = 95646 }
		command = { type = wakeleader which = 95647 }
		command = { type = wakeleader which = 95648 }
		command = { type = wakeleader which = 95649 }
		command = { type = wakeleader which = 95650 }
	}
}
###################################################
# War against Texas?
###################################################

event = {
	id = 99519
	random = no
	country = MEX

	name = "EVT_99519_NAME"
	desc = "EVT_99519_DESC"
	style = 0
	picture = "aztlan"

	trigger = {
		exists = U20
		atwar = no
		war = { country = U22 country = USA }
		NOT = {
			government = democratic
			alliance = { country = U20 country = USA }
			alliance = { country = U20 country = U21 }
			alliance = { country = U20 country = U22 }
			alliance = { country = U20 country = U23 }
		}
		
	}

	date = { day = 1 month = january year = 2004 }
	offset = 25
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "Texas has no future, Viva Mexico!"
		ai_chance = 80
		command = { type = war which = U20 }
		command = { type = addcore which = 718 }
		command = { type = addcore which = 717 }
		command = { type = addcore which = 746 }
		command = { type = addcore which = 748 }
		command = { type = addcore which = 715 }
		command = { type = addcore which = 716 }
		command = { type = addcore which = 719 }
		command = { type = addcore which = 720 }
		command = { type = addcore which = 721 }
	}
	action_b = {
		name = "We can't afford a war right now"
		ai_chance = 20
		command = { }
	}
}
###################################################
# 2nd US Civil War - Cuban intervention
###################################################

event = {
	id = 99520
	random = no
	country = CUB

	name = "EVT_99520_NAME"
	desc = "EVT_99520_DESC"
	style = 0
	picture = "american_militia"

	trigger = {
		government = communist
		atwar = no
		war = { country = U22 country = USA }
		
	}

	date = { day = 1 month = january year = 2004 }
	offset = 25
	deathdate = { day = 30 month = december year = 2020 }

	action_a = {
		name = "Send volunteers and supplies"
		ai_chance = 99
		command = { type = supplies value = -1000 }
		command = { type = trigger which = 99521 }
	}
	action_b = {
		name = "We can't strain our resources"
		ai_chance = 1
		command = { }
	}
}
###################################################
# 2nd US Civil War - Cuban intervention
###################################################

event = {
	id = 99521
	random = no
	country = U22

	name = "EVT_99521_NAME"
	desc = "EVT_99521_DESC"
	style = 0
	picture = "shipment"

	action_a = {
		name = "Excellent"
		command = { type = supplies value = 1000 }
		command = { type = add_division value = mechanized when = 1 }
		command = { type = set_relation which = CUB value = 150 }
	}
}