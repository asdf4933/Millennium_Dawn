#Finland
province = { id = 142 coastalfort   = 3 }

#Greece
province = { id = 410 landfort = 5 }

#Turkey
province = { id = 409 landfort = 5 }

#North Korea
province = { id = 1201 landfort = 5 }
province = { id = 1200 landfort = 5 }

#South Korea
province = { id = 1198 landfort = 2 }
province = { id = 1199 landfort = 2 }

#Pakistan
province = { id = 1480 landfort = 5 }
province = { id = 1475 landfort = 5 }
province = { id = 1482 landfort = 5 }