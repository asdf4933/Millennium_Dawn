
###############################
# Victory points distribution #
###############################

province = { id = 1 points = 2 } # Reykjavik
province = { id = 2 points = 2 } # H�fn
province = { id = 5 points = 1 } # Scapa Flow
province = { id = 9 points = 10 } # Glasgow
province = { id = 10 points = 3 } # Edinburgh
province = { id = 15 points = 2 } # Cardiff
province = { id = 16 points = 10 } # Birmingham
province = { id = 19 points = 20 } # London
province = { id = 21 points = 5 } # Portsmouth
province = { id = 23 points = 5 } # Plymouth
province = { id = 24 points = 2 } # Cork
province = { id = 28 points = 3 } # Belfast
province = { id = 30 points = 6 } # Dublin
province = { id = 31 points = 8 } # Malta
province = { id = 45 points = 3 } # Ghent
province = { id = 46 points = 3 } # Rotterdam
province = { id = 47 points = 10 } # Amsterdam
province = { id = 49 points = 2 } # Eindhoven
province = { id = 50 points = 3 } # Antwerp
province = { id = 51 points = 8 } # Brussels
province = { id = 56 points = 30 } # Paris
province = { id = 57 points = 2 } # Orl�ans
province = { id = 68 points = 1 } # Li�ge
province = { id = 71 points = 3 } # Luxembourg
province = { id = 75 points = 7 } # Cologne
province = { id = 76 points = 7 } # Dortmund
province = { id = 86 points = 2 } # Hannover
province = { id = 88 points = 11 } # Hamburg
province = { id = 92 points = 2 } # �rhus
province = { id = 93 points = 2 } # Malm�
province = { id = 95 points = 1 } # Karlskrona
province = { id = 98 points = 3 } # G�teborg
province = { id = 101 points = 8 } # Stockholm
province = { id = 107 points = 5 } # Oslo
province = { id = 109 points = 1 } # Stavanger
province = { id = 110 points = 1 } # Bergen
province = { id = 118 points = 1 } # Trondheim
province = { id = 125 points = 1 } # Narvik
province = { id = 127 points = 1 } # Lule�
province = { id = 129 points = 1 } # Vard�
province = { id = 133 points = 8 } # Murmansk
province = { id = 137 points = 1 } # Oulu
province = { id = 138 points = 1 } # Vaasa
province = { id = 141 points = 2 } # Turku
province = { id = 142 points = 6 } # Helsinki
province = { id = 154 points = 5 } # Archangelsk
province = { id = 163 points = 10 } # Volgograd
province = { id = 175 points = 25 } # Moscow
province = { id = 187 points = 10 } # St. Petersburg
province = { id = 194 points = 2 } # Tallinn
province = { id = 195 points = 1 } # Tartu
province = { id = 197 points = 3 } # Riga
province = { id = 205 points = 1 } # Mazirbe
province = { id = 207 points = 1 } # Kaunas
province = { id = 208 points = 4 } # Wilno
province = { id = 211 points = 1 } # Lida
province = { id = 212 points = 6 } # Minsk
province = { id = 217 points = 1 } # Vitebsk
province = { id = 225 points = 2 } # Mozyr
province = { id = 230 points = 1 } # Beltsy
province = { id = 233 points = 8 } # Odessa
province = { id = 238 points = 12 } # Kiev
province = { id = 241 points = 1 } # Chernigov
province = { id = 248 points = 2 } # Kharkov
province = { id = 253 points = 5 } # Sevastopol
province = { id = 258 points = 3 } # Rostov
province = { id = 294 points = 6 } # Copenhagen
province = { id = 295 points = 1 } # Bornholm
province = { id = 300 points = 30 } # Berlin
province = { id = 302 points = 4 } # Szczecin
province = { id = 303 points = 3 } # Gdansk
province = { id = 311 points = 2 } # Dresden
province = { id = 313 points = 3 } # Frankfurt-am-Main
province = { id = 325 points = 2 } # Bordeaux
province = { id = 331 points = 2 } # Bilbao
province = { id = 335 points = 2 } # Oporto
province = { id = 336 points = 5 } # Lissabon
province = { id = 337 points = 2 } # Guarda
province = { id = 341 points = 9 } # Madrid
province = { id = 347 points = 1 } # Seville
province = { id = 354 points = 3 } # Val�ncia
province = { id = 357 points = 5 } # Barcelona
province = { id = 359 points = 2 } # Toulouse
province = { id = 364 points = 10 } # Marseille
province = { id = 366 points = 4 } # Nice
province = { id = 367 points = 1 } # Corsica
province = { id = 370 points = 8 } # Milan
province = { id = 376 points = 18 } # Munich
province = { id = 377 points = 1 } # Innsbruck
province = { id = 378 points = 5 } # Venice
province = { id = 380 points = 3 } # Ljubljana
province = { id = 381 points = 2 } # Rijeka
province = { id = 382 points = 7 } # Zagreb
province = { id = 383 points = 2 } # Banja Luka
province = { id = 384 points = 2 } # Split
province = { id = 386 points = 5 } # Sarajevo
province = { id = 387 points = 1 } # Mostar
province = { id = 388 points = 1 } # Dubrovnik
province = { id = 389 points = 3 } # Podgorica
province = { id = 390 points = 4 } # Tirana
province = { id = 391 points = 1 } # Vlor�
province = { id = 396 points = 1 } # Crete
province = { id = 401 points = 8 } # Athens
province = { id = 409 points = 8 } # Istanbul
province = { id = 410 points = 3 } # Kavala
province = { id = 411 points = 3 } # Salonika
province = { id = 413 points = 3 } # Skopje
province = { id = 415 points = 5 } # Pristina
province = { id = 417 points = 6 } # Sofia
province = { id = 421 points = 2 } # Plovdiv
province = { id = 422 points = 2 } # Varna
province = { id = 423 points = 1 } # Constanta
province = { id = 424 points = 10 } # Bucharest
province = { id = 430 points = 3 } # Brasov
province = { id = 431 points = 4 } # Ploesti
province = { id = 434 points = 3 } # Chisinev
province = { id = 437 points = 4 } # Izmir
province = { id = 444 points = 8 } # Cyprus
province = { id = 445 points = 2 } # Gaziantep
province = { id = 448 points = 6 } # Ankara
province = { id = 453 points = 10 } # Belgrade
province = { id = 457 points = 6 } # Budapest
province = { id = 460 points = 1 } # Graz
province = { id = 462 points = 1 } # Salzburg
province = { id = 463 points = 6 } # Vienna
province = { id = 465 points = 1 } # Linz
province = { id = 469 points = 6 } # Prague
province = { id = 470 points = 2 } # Pilsen
province = { id = 473 points = 5 } # Wroclaw
province = { id = 475 points = 2 } # Brno
province = { id = 476 points = 5 } # Bratislava
province = { id = 479 points = 7 } # Cracow
province = { id = 485 points = 15 } # Warsaw
province = { id = 487 points = 7 } # Lublin
province = { id = 490 points = 1 } # Kosice
province = { id = 491 points = 2 } # Miskolc
province = { id = 495 points = 2 } # Timisoara
province = { id = 501 points = 2 } # Lvov
province = { id = 504 points = 2 } # Brest Litovsk
province = { id = 508 points = 4 } # Bialystok
province = { id = 515 points = 15 } # Rome
province = { id = 521 points = 8 } # Naples
province = { id = 523 points = 2 } # Taranto
province = { id = 525 points = 2 } # Palermo
province = { id = 528 points = 8 } # Lyon
province = { id = 530 points = 4 } # Geneva
province = { id = 532 points = 3 } # Bern
province = { id = 533 points = 3 } # Z�rich
province = { id = 539 points = 3 } # Managua
province = { id = 540 points = 3 } # San Jos�
province = { id = 541 points = 1 } # Vancouver
province = { id = 549 points = 6 } # Anchorage
province = { id = 569 points = 1 } # Winnipeg
province = { id = 576 points = 1 } # Newfoundland
province = { id = 579 points = 1 } # Halifax
province = { id = 586 points = 3 } # Qu�bec City
province = { id = 588 points = 5 } # Ottawa
province = { id = 589 points = 7 } # Toronto
province = { id = 600 points = 3 } # Boston
province = { id = 602 points = 30 } # New York
province = { id = 606 points = 20 } # Washington D.C.
province = { id = 608 points = 3 } # Philadelphia
province = { id = 620 points = 15 } # Chicago
province = { id = 651 points = 8 } # Miami
province = { id = 652 points = 1 } # Andros
province = { id = 654 points = 8 } # Havana
province = { id = 657 points = 2 } # Guant�namo
province = { id = 663 points = 8 } # Pearl Harbour
province = { id = 667 points = 10 } # Buenos Aires
province = { id = 670 points = 5 } # Atlanta
province = { id = 680 points = 5 } # New Orleans
province = { id = 707 points = 3 } # Denver
province = { id = 716 points = 12 } # Houston
province = { id = 738 points = 1 } # La Paz
province = { id = 747 points = 4 } # Monterrey
province = { id = 751 points = 4 } # Guadalajara
province = { id = 752 points = 8 } # Mexico City
province = { id = 754 points = 1 } # Acapulco
province = { id = 756 points = 2 } # M�rida
province = { id = 757 points = 3 } # Guatemala
province = { id = 759 points = 3 } # San Salvador
province = { id = 760 points = 3 } # Tegucigalpa
province = { id = 762 points = 15 } # Los Angeles
province = { id = 765 points = 10 } # San Francisco
province = { id = 780 points = 10 } # Seattle
province = { id = 796 points = 1 } # Edmonton
province = { id = 802 points = 3 } # Barquisimeto
province = { id = 803 points = 2 } # Barranquilla
province = { id = 804 points = 5 } # Maracaibo
province = { id = 810 points = 2 } # Cuman�
province = { id = 811 points = 10 } # Caracas
province = { id = 813 points = 6 } # Bogot�
province = { id = 814 points = 2 } # Guayaquil
province = { id = 815 points = 6 } # Quito
province = { id = 816 points = 2 } # Trujillo
province = { id = 818 points = 1 } # Iquitos
province = { id = 830 points = 6 } # Lima
province = { id = 831 points = 1 } # Arequipa
province = { id = 832 points = 4 } # La Paz
province = { id = 835 points = 2 } # Mato Grosso
province = { id = 836 points = 1 } # Santa Cruz
province = { id = 837 points = 3 } # Asunci�n
province = { id = 838 points = 1 } # Sucre
province = { id = 840 points = 4 } # Santiago
province = { id = 848 points = 2 } # P�rto Alegre
province = { id = 849 points = 3 } # Montevideo
province = { id = 851 points = 2 } # Rosario
province = { id = 853 points = 1 } # Bahia Blanca
province = { id = 856 points = 4 } # Puerto Montt
province = { id = 857 points = 1 } # Puerto Madryn
province = { id = 861 points = 4 } # Wellington
province = { id = 865 points = 1 } # Tierra del Fuego
province = { id = 866 points = 1 } # Falkland Islands
province = { id = 873 points = 1 } # Georgetown
province = { id = 874 points = 1 } # Paramaribo
province = { id = 881 points = 1 } # Recife
province = { id = 883 points = 6 } # Goi�s
province = { id = 885 points = 8 } # Rio de Janeiro
province = { id = 886 points = 6 } # S�o Paulo
province = { id = 888 points = 3 } # Panam�
province = { id = 889 points = 15 } # Colon
province = { id = 890 points = 2 } # Medell�n
province = { id = 891 points = 1 } # Jamaica
province = { id = 892 points = 1 } # Port-au-Prince
province = { id = 894 points = 3 } # Santo Domingo
province = { id = 895 points = 2 } # Puerto Rico
province = { id = 900 points = 15 } # Suez
province = { id = 903 points = 8 } # Cairo
province = { id = 906 points = 6 } # Alexandria
province = { id = 919 points = 1 } # Tobruk
province = { id = 924 points = 2 } # Bengazi
province = { id = 930 points = 1 } # Homs
province = { id = 932 points = 8 } # Tripoli
province = { id = 936 points = 1 } # Sfax
province = { id = 939 points = 5 } # Tunis
province = { id = 942 points = 2 } # B�ne
province = { id = 949 points = 5 } # Algiers
province = { id = 953 points = 1 } # Oran
province = { id = 958 points = 8 } # Casablanca
province = { id = 959 points = 1 } # Marrakech
province = { id = 971 points = 1 } # El Aai�n
province = { id = 973 points = 2 } # Port-�tienne
province = { id = 977 points = 2 } # Timbuktu
province = { id = 981 points = 3 } # Dakar
province = { id = 992 points = 2 } # Monrovia
province = { id = 993 points = 3 } # Abidjan
province = { id = 1002 points = 3 } # Accra
province = { id = 1007 points = 1 } # Ibadan
province = { id = 1008 points = 5 } # Lagos
province = { id = 1009 points = 8 } # Kaduna
province = { id = 1024 points = 2 } # Luxor
province = { id = 1030 points = 1 } # Port Sudan
province = { id = 1039 points = 3 } # Asmara
province = { id = 1043 points = 2 } # Des�
province = { id = 1045 points = 1 } # Gamb�la
province = { id = 1046 points = 5 } # Addis Ababa
province = { id = 1056 points = 3 } # Mogadishu
province = { id = 1060 points = 3 } # Nairobi
province = { id = 1061 points = 1 } # Mombasa
province = { id = 1063 points = 3 } # Kigali
province = { id = 1064 points = 2 } # Entebbe
province = { id = 1066 points = 3 } # Khartoum
province = { id = 1068 points = 1 } # El Obeid
province = { id = 1073 points = 1 } # Costermanville
province = { id = 1075 points = 4 } # Brazzaville
province = { id = 1077 points = 3 } # Yaound�
province = { id = 1078 points = 3 } # Bangui
province = { id = 1080 points = 4 } # Fort-Lamy
province = { id = 1081 points = 1 } # Largeau
province = { id = 1086 points = 1 } # Maroua
province = { id = 1089 points = 1 } # Douala
province = { id = 1091 points = 3 } # Bata
province = { id = 1092 points = 3 } # Libreville
province = { id = 1093 points = 1 } # Pointe-Noire
province = { id = 1095 points = 2 } # Banana
province = { id = 1096 points = 5 } # Loanda
province = { id = 1097 points = 1 } # Vila Henrique de Carvalho
province = { id = 1101 points = 2 } # Benguella
province = { id = 1110 points = 3 } # Windhoek
province = { id = 1111 points = 1 } # Francistown
province = { id = 1112 points = 1 } # Gaberones
province = { id = 1116 points = 2 } # Cape Town
province = { id = 1120 points = 4 } # Johannesburg
province = { id = 1121 points = 2 } # Pretoria
province = { id = 1122 points = 2 } # Louren�o Marques
province = { id = 1123 points = 5 } # Inhambane
province = { id = 1125 points = 5 } # Harare
province = { id = 1126 points = 3 } # Lusaka
province = { id = 1130 points = 1 } # Beira
province = { id = 1132 points = 3 } # Zomba
province = { id = 1134 points = 1 } # Fianarantsoa
province = { id = 1137 points = 4 } # Tananarive
province = { id = 1140 points = 3 } # Dar es Salaam
province = { id = 1142 points = 5 } # L�opoldville
province = { id = 1146 points = 1 } # Cap Verde Islands
province = { id = 1163 points = 3 } # Yakutsk
province = { id = 1179 points = 2 } # Sapporo
province = { id = 1180 points = 1 } # Sendai
province = { id = 1184 points = 15 } # Tokyo
province = { id = 1185 points = 6 } # Nagoya
province = { id = 1187 points = 9 } # Osaka
province = { id = 1188 points = 6 } # Hiroshima
province = { id = 1190 points = 7 } # Fukuoka
province = { id = 1193 points = 3 } # Okinawa
province = { id = 1195 points = 5 } # Gwangju
province = { id = 1196 points = 10 } # Busan
province = { id = 1199 points = 25 } # Seoul
province = { id = 1200 points = 2 } # Wonsan
province = { id = 1201 points = 20 } # Pyongyang
province = { id = 1202 points = 8 } # Dalian
province = { id = 1204 points = 10 } # Tianjin
province = { id = 1205 points = 30 } # Beijing
province = { id = 1237 points = 40 } # Shanghai
province = { id = 1269 points = 5 } # Xi'an
province = { id = 1280 points = 15 } # Chongqing
province = { id = 1285 points = 5 } # Dhaka
province = { id = 1288 points = 1 } # Chittagong
province = { id = 1296 points = 2 } # Mandalay
province = { id = 1298 points = 1 } # Bassein
province = { id = 1299 points = 5 } # Rangoon
province = { id = 1303 points = 2 } # Chiang Rai
province = { id = 1313 points = 3 } # Guangzhou
province = { id = 1315 points = 35 } # Hong Kong
province = { id = 1321 points = 3 } # Hainan
province = { id = 1322 points = 8 } # Kaohsiung
province = { id = 1323 points = 30 } # Taipei
province = { id = 1324 points = 2 } # Hualien
province = { id = 1328 points = 10 } # Hanoi
province = { id = 1329 points = 4 } # Vientiane
province = { id = 1332 points = 1 } # Nhommarath
province = { id = 1333 points = 2 } # Da Nang
province = { id = 1337 points = 8 } # Saigon
province = { id = 1339 points = 1 } # Battambang
province = { id = 1340 points = 4 } # Phnom Penh
province = { id = 1343 points = 10 } # Bangkok
province = { id = 1347 points = 3 } # Kra
province = { id = 1352 points = 8 } # Kuala Lumpur
province = { id = 1353 points = 15 } # Singapore
province = { id = 1356 points = 8 } # Dumai
province = { id = 1358 points = 2 } # Palembang
province = { id = 1372 points = 10 } # Vladivostok
province = { id = 1379 points = 8 } # Harbin
province = { id = 1392 points = 1 } # Hamhung
province = { id = 1393 points = 2 } # Sinuiju
province = { id = 1395 points = 8 } # Yingkou
province = { id = 1404 points = 1 } # Ulan Bator
province = { id = 1416 points = 5 } # Irkutsk
province = { id = 1422 points = 1 } # Alma-Ata
province = { id = 1427 points = 4 } # Tashkent
province = { id = 1429 points = 3 } # Frunze
province = { id = 1433 points = 5 } # Urumqi
province = { id = 1453 points = 5 } # Lhasa
province = { id = 1455 points = 1 } # Punakha
province = { id = 1457 points = 20 } # Calcutta
province = { id = 1466 points = 10 } # Ahmadabad
province = { id = 1469 points = 30 } # Delhi
province = { id = 1473 points = 5 } # Kathmandu
province = { id = 1474 points = 15 } # Srinagar
province = { id = 1475 points = 10 } # Lahore
province = { id = 1480 points = 2 } # Hyderabad
province = { id = 1483 points = 3 } # Peshawar
province = { id = 1485 points = 3 } # Stalinabad
province = { id = 1486 points = 2 } # Kabul
province = { id = 1490 points = 4 } # Ashgabat
province = { id = 1493 points = 1 } # Kandahar
province = { id = 1494 points = 10 } # Karachi
province = { id = 1497 points = 2 } # Bandar Abbas
province = { id = 1500 points = 5 } # Mashhad
province = { id = 1502 points = 16 } # Teheran
province = { id = 1504 points = 10 } # Dubai
province = { id = 1505 points = 20 } # Bombay
province = { id = 1507 points = 7 } # Hyderabad
province = { id = 1509 points = 10 } # Madras
province = { id = 1511 points = 8 } # Bangalore
province = { id = 1516 points = 2 } # Trincomalee
province = { id = 1517 points = 6 } # Colombo
province = { id = 1552 points = 8 } # Omsk
province = { id = 1567 points = 8 } # Ufa
province = { id = 1576 points = 1 } # Kostanai
province = { id = 1578 points = 1 } # Aktyubinsk
province = { id = 1581 points = 5 } # Turgai
province = { id = 1585 points = 1 } # Nukus
province = { id = 1599 points = 1 } # Iwo Jima
province = { id = 1605 points = 1 } # Majuro
province = { id = 1611 points = 1 } # Nauru
province = { id = 1613 points = 2 } # Truk
province = { id = 1624 points = 3 } # Guam
province = { id = 1628 points = 10 } # Batavia
province = { id = 1629 points = 6 } # Tjilatjap
province = { id = 1632 points = 6 } # Soerabaja
province = { id = 1635 points = 2 } # Bali
province = { id = 1640 points = 2 } # Makassar
province = { id = 1641 points = 2 } # Bandjermasin
province = { id = 1646 points = 2 } # Miri
province = { id = 1658 points = 3 } # East Timor
province = { id = 1665 points = 2 } # Hollandia
province = { id = 1679 points = 2 } # Port Moresby
province = { id = 1685 points = 2 } # Guadalcanal
province = { id = 1692 points = 1 } # Noumea
province = { id = 1703 points = 1 } # Darwin
province = { id = 1712 points = 1 } # Perth
province = { id = 1722 points = 1 } # Brisbane
province = { id = 1727 points = 8 } # Sydney
province = { id = 1728 points = 5 } # Canberra
province = { id = 1730 points = 3 } # Melbourne
province = { id = 1733 points = 1 } # Adelaide
province = { id = 1735 points = 1 } # Palawan
province = { id = 1737 points = 8 } # Manila
province = { id = 1739 points = 3 } # Clark Field
province = { id = 1747 points = 1 } # Negros
province = { id = 1750 points = 2 } # Davao
province = { id = 1752 points = 2 } # Fiji
province = { id = 1756 points = 1 } # Samoa
province = { id = 1769 points = 1 } # Tarawa
province = { id = 1780 points = 5 } # Kazan
province = { id = 1792 points = 6 } # Damascus
province = { id = 1794 points = 6 } # Beirut
province = { id = 1795 points = 3 } # Golan
province = { id = 1796 points = 4 } # Amman
province = { id = 1797 points = 7 } # Jerusalem
province = { id = 1798 points = 2 } # Tel Aviv
province = { id = 1807 points = 10 } # Riyadh
province = { id = 1808 points = 5 } # Jiddah
province = { id = 1812 points = 4 } # Sanaa
province = { id = 1813 points = 1 } # Aden
province = { id = 1819 points = 5 } # Mascate
province = { id = 1820 points = 8 } # Doha
province = { id = 1821 points = 10 } # Dammam
province = { id = 1822 points = 10 } # Kuweit City
province = { id = 1823 points = 5 } # Basrah
province = { id = 1826 points = 2 } # Esfahan
province = { id = 1848 points = 1 } # Batum
province = { id = 1850 points = 5 } # Tblisi
province = { id = 1856 points = 3 } # Yerevan
province = { id = 1859 points = 5 } # Tabriz
province = { id = 1860 points = 3 } # Kirkuk
province = { id = 1862 points = 2 } # Aleppo
province = { id = 1864 points = 2 } # Mosul
province = { id = 1866 points = 10 } # Baghdad
province = { id = 1868 points = 3 } # Auckland
province = { id = 1871 points = 6 } # Wellington
province = { id = 1872 points = 1 } # Christchurch
province = { id = 1907 points = 12 } # Baku
