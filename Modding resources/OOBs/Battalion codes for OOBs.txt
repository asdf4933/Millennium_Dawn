﻿#Infantry

Militia_Bat #Militia Battalion
Mot_Militia_Bat #Motorized Militia Battalion

L_Inf_Bat #Light (foot) Infantry Battalion
Mot_Inf_Bat #Motorized Infantry Battalion
Mech_Inf_Bat #Mechanized Infantry Battalion
Arm_Inf_Bat #Armored Infantry Battalion

L_Air_Inf_Bat #Light Airborne Infantry Battalion
Mot_Air_Inf_Bat #Motorized Airborne Infantry Battalion
Mech_Air_Inf_Bat #Mechanized Airborne Infantry Battalion
Arm_Air_Inf_Bat #Airborne Armored Infantry Battalion

L_Air_assault_Bat #Light Air Assault Infantry Battalion
Arm_Air_assault_Bat #Air Assault Armored Infantry Battalion

L_Marine_Bat #Light Marine Infantry Battalion
Mot_Marine_Bat #Motorized Marine Infantry Battalion
Mech_Marine_Bat #Mechanized Marine Infantry Battalion
Arm_Marine_Bat #Marine Armored Infantry Battalion

Special_Forces #Special Forces Battalion

#Tanks

armor_Bat #Tank Battalion
armor_Comp #Tank Company

#Recce

L_Recce_Bat #Light Recce Battalion
L_Recce_Comp #Light Recce Company

Mot_Recce_Bat #Motorized Recce Battalion
Mot_Recce_Comp #Motorized Recce Company

Mech_Recce_Bat #Mechanized Recce Battalion
Mech_Recce_Comp #Mechanized Recce Company

Arm_Recce_Bat #Heavy Mechanized Recce Battalion
Arm_Recce_Comp #Heavy Mechanized Recce Company

armor_Recce_Bat #Armored Recce Battalion (uses light tanks)
armor_Recce_Comp #Armored Recce Company (uses light tanks)

#Engineers

L_Engi_Bat #Light Engineer Battalion
L_Engi_Comp #Light Engineer Company

H_Engi_Bat #Heavy Engineer Battalion
H_Engi_Comp #Heavy Engineer Company

#Artillery

Arty_Bat #Towed Artillery Battalion
Arty_Battery #Towed Artillery Battery

SP_Arty_Bat #Self-Propelled Artillery Battalion
SP_Arty_Battery #Self-Propelled Artillery Battery

#NOT USED #SP_R_Arty_Bat #Self-Propelled Rocket Artillery (MLRS) Battalion
#NOT USED #SP_R_Arty_Battery #Self-Propelled Rocket Artillery (MLRS) Battery

#Anti Air

SP_AA_Bat #Self-Propelled Anti Air Battalion
SP_AA_Battery #Self-Propelled Anti Air Battery

