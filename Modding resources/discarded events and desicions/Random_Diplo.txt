﻿# Random_Diplo
# How to and how does this work
#	This is a event chain framework that is easily expanded.
#	The event chain have 4 parts
#	Init	- The init starts the chain and make sure that the
#			correct nation gets the event in the chain. Also 
#			in order to get the correct nation names.
#	Start  - Start event contain unique events. They have two 
#			parts; First initializes were the nation gets
#			information about the upcoming incident.
#			The second one is the information the other nation
#			that the first nation f*t up.
#	Event  - Event contain the main escalation chain. This is
#			generic in nature.
#	News	- This is the end event and is called by the Event
#			part and is called by event random_diplo_end.0

add_namespace = random_diplo_init
add_namespace = random_diplo_start
add_namespace = random_diplo_esc
add_namespace = random_diplo_end
add_namespace = random_diplo_news

country_event = {	# Event chain init
	id = random_diplo_init.0
	title = random_diplo_init.0.t
	desc = random_diplo_init.0.d
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		has_war = no
		NOT = { original_tag = ISI }
		NOT = { original_tag = SHB }
		NOT = { original_tag = AQY }
		NOT = { original_tag = TAL }
		NOT = { original_tag = TTP }
		NOT = { original_tag = NUS }

		OR = {
		  NOT = { has_country_flag = random_dip_0_incident_flag }
		  NOT = { has_country_flag = random_dip_1_incident_flag }
		}
	}
	
	option = {
		log = "[GetDateText]: [This.GetName]: random_diplo_init.0 completed"
		random_neighbor_country = {
		  limit = {
				NOT = { original_tag = ISI }
				NOT = { original_tag = SHB }
				NOT = { original_tag = AQY }
				NOT = { original_tag = TAL }
				NOT = { original_tag = TTP }
				NOT = { original_tag = NUS }
			 has_opinion = { target = FROM value > -20 }
			 
			 OR = {
				NOT = { has_country_flag = random_dip_0_incident_flag }
				NOT = { has_country_flag = random_dip_1_incident_flag }
			 }
		  }
		  country_event = { days = 1 id = random_diplo_start.0 }
		}
	}
}

# Start
country_event = {	# Incident 1: The Meh did Meh
	id = random_diplo_start.0
	title = random_diplo_start.0.t
	desc = random_diplo_start.0.d
	picture = GFX_report_event_worker_protests
	
	is_triggered_only = yes
	
	immediate = {
		set_country_flag = random_dip_0_incident_active_flag
	}
	
	option = {  # Eh. thats nothing.
		name = random_diplo_start.0.a
		log = "[GetDateText]: [This.GetName]: random_diplo_start.0.a executed"
		ai_chance = { factor = 20 }
		if = {
		  limit = {
			 has_government = communism
			 FROM = { NOT = { has_government = communism } }
		  }
		  add_popularity = { ideology = communism popularity = 0.025 }
		}
		if = {
		  limit = {
			 has_government = neutrality
		  }
		  add_popularity = { ideology = neutrality popularity = 0.025 }
		}
		if = {
		  limit = { has_government = nationalist }
		  add_popularity = { ideology = nationalist popularity = 0.05 }
		}
		  set_country_flag = random_dip_0_incident_flag
		  FROM = { country_event = { days = 1 id = random_diplo_start.1 } }
	}
	option = {  # Oh no!
		name = random_diplo_start.0.b
		log = "[GetDateText]: [This.GetName]: random_diplo_start.0.b executed"
		ai_chance = { factor = 80 }
		if = {
		  limit = {
			 has_government = democratic
			 FROM = { has_government = democratic }
		  }
		  add_popularity = { ideology = democratic popularity = -0.025 }
		}
		if = {
		  limit = {
			 has_government = communism
			 FROM = { has_government = communism }
		  }
		  add_popularity = { ideology = communism popularity = -0.025 }
		}
		if = {
		  limit = {
			 has_government = neutrality
			 FROM = { has_government = neutrality }
		  }
		  add_popularity = { ideology = neutrality popularity = -0.025 }
		}
		if = {
		  limit = {
			 has_government = nationalist
			 FROM = { has_government = nationalist }
		  }
		  add_popularity = { ideology = nationalist popularity = -0.025 }
		}

		set_country_flag = random_dip_0_incident_flag
		FROM = { country_event = { days = 1 id = random_diplo_start.1 } }
	}
}
country_event = {	# Incident 1 Meh did Meh against us!
	id = random_diplo_start.1
	title = random_diplo_start.1.t
	desc = random_diplo_start.1.d
	picture = GFX_report_event_worker_protests
	
	is_triggered_only = yes

	immediate = {
		set_country_flag = random_dip_1_incident_flag
	}
	
	option = { # Unacceptable!
		name = random_diplo_start.1.a
		log = "[GetDateText]: [This.GetName]: random_diplo_start.1.a executed"
		ai_chance = { factor = 80 }
		if = {
		  limit = { has_government = democratic }
		  add_popularity = { ideology = democratic popularity = 0.0025 }
		}
		if = {
		  limit = { has_government = communism }
		  add_popularity = { ideology = communism popularity = 0.0025 }
		}
		if = {
		  limit = { has_government = neutrality }
		  add_popularity = { ideology = neutrality popularity = 0.0025 }
		}
		if = {
		  limit = { has_government = nationalist }
		  add_popularity = { ideology = nationalist popularity = 0.0025 }
		}
		add_political_power = 10
		add_opinion_modifier = { target = FROM modifier = random_diplo_start_angry }
		FROM = { country_event = { days = 1 id = random_diplo_esc.0 } }
	}
	option = { # Our pride nation have been insulted!
		name = random_diplo_start.1.c
		log = "[GetDateText]: [This.GetName]: random_diplo_start.1.c executed"
		ai_chance = { factor = 1000 }
		trigger = {
		  has_government = nationalist
		}
		add_political_power = 25
		add_popularity = { ideology = nationalist popularity = 0.01 }
		add_opinion_modifier = { target = FROM modifier = random_diplo_start_angry_nationalists }
		FROM = { country_event = { days = 1 id = random_diplo_esc.0 } }
	}
	option = { # let it slide.
		name = random_diplo_start.1.b
		log = "[GetDateText]: [This.GetName]: random_diplo_start.1.b executed"
		ai_chance = { factor = 20 }
		if = {
		  limit = { has_government = democratic }
		  add_popularity = { ideology = democratic popularity = -0.025 }
		}
		if = {
		  limit = { has_government = communism }
		  add_popularity = { ideology = communism popularity = -0.025 }
		}
		if = {
		  limit = { has_government = neutrality }
		  add_popularity = { ideology = neutrality popularity = -0.025 }
		}
		FROM = { country_event = { days = 1 id = random_diplo_end.0 } }
	}
}

# Escalation
country_event = {	# Apology
	id = random_diplo_esc.0
	title = random_diplo_esc.0.t
	desc = random_diplo_esc.0.d
	picture = GFX_report_event_worker_protests
	
	is_triggered_only = yes

	option = {  # Send a official apology!
		name = random_diplo_esc.0.a
		log = "[GetDateText]: [This.GetName]: random_diplo_esc.0.a executed"
		ai_chance = { factor = 70 }
		FROM = { country_event = { days = 1 id = random_diplo_esc.1 } }
	}
	option = {  # Send a apology but don't make it official.
		name = random_diplo_esc.0.b
		log = "[GetDateText]: [This.GetName]: random_diplo_esc.0.b executed"
		ai_chance = { factor = 10 }
		FROM = { country_event = { days = 1 id = random_diplo_esc.1 } }

	}
	option = {  # Do not apology
		name = random_diplo_esc.0.c
		log = "[GetDateText]: [This.GetName]: random_diplo_esc.0.c executed"
		ai_chance = { factor = 20 }
		FROM = { country_event = { days = 1 id = random_diplo_end.1 } }
	}
}
country_event = {	# Apology Reply
	id = random_diplo_esc.1
	title = random_diplo_esc.1.t
	desc = random_diplo_esc.1.d
	picture = GFX_report_event_worker_protests
	
	is_triggered_only = yes

	option = {  # Accept the apology
		name = random_diplo_esc.1.a
		log = "[GetDateText]: [This.GetName]: random_diplo_esc.1.a executed"
		ai_chance = { factor = 70 }
		trigger = {
		  NOT = { has_opinion_modifier = random_diplo_start_angry_nationalists }
		}
		FROM = { country_event = { days = 1 id = random_diplo_end.0 } }
		remove_opinion_modifier = { target = FROM modifier = random_diplo_start_angry }
	}
	option = {  # Refuse the apology
		name = random_diplo_esc.1.b
		log = "[GetDateText]: [This.GetName]: random_diplo_esc.1.b executed"
		ai_chance = { factor = 10 }
		FROM = { country_event = { days = 1 id = random_diplo_end.1 } }
		add_opinion_modifier = { target = FROM modifier = random_diplo_esc_refuced_our_apology }
	}
}
# End
country_event = {	# Crises avoided
	id = random_diplo_end.0
	title = random_diplo_end.0.t
	desc = random_diplo_end.0.d
	picture = GFX_report_event_worker_protests
	
	is_triggered_only = yes

	option = {  # Good!
		name = random_diplo_end.0.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.0.a executed"
		trigger = { has_country_flag = random_dip_0_incident_active_flag }
		#clr_country_flag = random_dip_0_incident_flag
		#clr_country_flag = random_dip_1_incident_flag
		clr_country_flag = random_dip_2_incident_flag
		clr_country_flag = random_dip_3_incident_flag
		clr_country_flag = random_dip_4_incident_flag
		clr_country_flag = random_dip_5_incident_flag
		clr_country_flag = random_dip_6_incident_flag
		clr_country_flag = random_dip_7_incident_flag
		clr_country_flag = random_dip_8_incident_flag
		clr_country_flag = random_dip_9_incident_flag
		clr_country_flag = random_dip_10_incident_flag
		clr_country_flag = random_dip_11_incident_flag

		#FROM = { clr_country_flag = random_dip_0_incident_flag }
		#FROM = { clr_country_flag = random_dip_1_incident_flag }
		FROM = { clr_country_flag = random_dip_2_incident_flag }
		FROM = { clr_country_flag = random_dip_3_incident_flag }
		FROM = { clr_country_flag = random_dip_4_incident_flag }
		FROM = { clr_country_flag = random_dip_5_incident_flag }
		FROM = { clr_country_flag = random_dip_6_incident_flag }
		FROM = { clr_country_flag = random_dip_7_incident_flag }
		FROM = { clr_country_flag = random_dip_8_incident_flag }
		FROM = { clr_country_flag = random_dip_9_incident_flag }
		FROM = { clr_country_flag = random_dip_10_incident_flag }
		FROM = { clr_country_flag = random_dip_11_incident_flag }

		clr_country_flag = random_dip_0_incident_active_flag
		#clr_country_flag = random_dip_2_incident_active_flag
		#clr_country_flag = random_dip_4_incident_active_flag
		#clr_country_flag = random_dip_6_incident_active_flag
		#clr_country_flag = random_dip_8_incident_active_flag
		#clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.0 }
	}	
	option = {  # Good!
		name = random_diplo_end.0.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.0.a executed"
		trigger = { has_country_flag = random_dip_2_incident_active_flag }
		clr_country_flag = random_dip_0_incident_flag
		clr_country_flag = random_dip_1_incident_flag
		#clr_country_flag = random_dip_2_incident_flag
		#clr_country_flag = random_dip_3_incident_flag
		clr_country_flag = random_dip_4_incident_flag
		clr_country_flag = random_dip_5_incident_flag
		clr_country_flag = random_dip_6_incident_flag
		clr_country_flag = random_dip_7_incident_flag
		clr_country_flag = random_dip_8_incident_flag
		clr_country_flag = random_dip_9_incident_flag
		clr_country_flag = random_dip_10_incident_flag
		clr_country_flag = random_dip_11_incident_flag

		FROM = { clr_country_flag = random_dip_0_incident_flag }
		FROM = { clr_country_flag = random_dip_1_incident_flag }
		#FROM = { clr_country_flag = random_dip_2_incident_flag }
		#FROM = { clr_country_flag = random_dip_3_incident_flag }
		FROM = { clr_country_flag = random_dip_4_incident_flag }
		FROM = { clr_country_flag = random_dip_5_incident_flag }
		FROM = { clr_country_flag = random_dip_6_incident_flag }
		FROM = { clr_country_flag = random_dip_7_incident_flag }
		FROM = { clr_country_flag = random_dip_8_incident_flag }
		FROM = { clr_country_flag = random_dip_9_incident_flag }
		FROM = { clr_country_flag = random_dip_10_incident_flag }
		FROM = { clr_country_flag = random_dip_11_incident_flag }

		#clr_country_flag = random_dip_0_incident_active_flag
		clr_country_flag = random_dip_2_incident_active_flag
		#clr_country_flag = random_dip_4_incident_active_flag
		#clr_country_flag = random_dip_6_incident_active_flag
		#clr_country_flag = random_dip_8_incident_active_flag
		#clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.2 }
	}	
	option = {  # Good!
		name = random_diplo_end.0.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.0.a executed"
		trigger = { has_country_flag = random_dip_4_incident_active_flag }
		clr_country_flag = random_dip_0_incident_flag
		clr_country_flag = random_dip_1_incident_flag
		clr_country_flag = random_dip_2_incident_flag
		clr_country_flag = random_dip_3_incident_flag
		#clr_country_flag = random_dip_4_incident_flag
		#clr_country_flag = random_dip_5_incident_flag
		clr_country_flag = random_dip_6_incident_flag
		clr_country_flag = random_dip_7_incident_flag
		clr_country_flag = random_dip_8_incident_flag
		clr_country_flag = random_dip_9_incident_flag
		clr_country_flag = random_dip_10_incident_flag
		clr_country_flag = random_dip_11_incident_flag

		FROM = { clr_country_flag = random_dip_0_incident_flag }
		FROM = { clr_country_flag = random_dip_1_incident_flag }
		FROM = { clr_country_flag = random_dip_2_incident_flag }
		FROM = { clr_country_flag = random_dip_3_incident_flag }
		#FROM = { clr_country_flag = random_dip_4_incident_flag }
		#FROM = { clr_country_flag = random_dip_5_incident_flag }
		FROM = { clr_country_flag = random_dip_6_incident_flag }
		FROM = { clr_country_flag = random_dip_7_incident_flag }
		FROM = { clr_country_flag = random_dip_8_incident_flag }
		FROM = { clr_country_flag = random_dip_9_incident_flag }
		FROM = { clr_country_flag = random_dip_10_incident_flag }
		FROM = { clr_country_flag = random_dip_11_incident_flag }

		#clr_country_flag = random_dip_0_incident_active_flag
		#clr_country_flag = random_dip_2_incident_active_flag
		clr_country_flag = random_dip_4_incident_active_flag
		#clr_country_flag = random_dip_6_incident_active_flag
		#clr_country_flag = random_dip_8_incident_active_flag
		#clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.4 }
	}	
	option = {  # Good!
		name = random_diplo_end.0.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.0.a executed"
		trigger = { has_country_flag = random_dip_6_incident_active_flag }
		clr_country_flag = random_dip_0_incident_flag
		clr_country_flag = random_dip_1_incident_flag
		clr_country_flag = random_dip_2_incident_flag
		clr_country_flag = random_dip_3_incident_flag
		clr_country_flag = random_dip_4_incident_flag
		clr_country_flag = random_dip_5_incident_flag
		#clr_country_flag = random_dip_6_incident_flag
		#clr_country_flag = random_dip_7_incident_flag
		clr_country_flag = random_dip_8_incident_flag
		clr_country_flag = random_dip_9_incident_flag
		clr_country_flag = random_dip_10_incident_flag
		clr_country_flag = random_dip_11_incident_flag

		FROM = { clr_country_flag = random_dip_0_incident_flag }
		FROM = { clr_country_flag = random_dip_1_incident_flag }
		FROM = { clr_country_flag = random_dip_2_incident_flag }
		FROM = { clr_country_flag = random_dip_3_incident_flag }
		FROM = { clr_country_flag = random_dip_4_incident_flag }
		FROM = { clr_country_flag = random_dip_5_incident_flag }
		#FROM = { clr_country_flag = random_dip_6_incident_flag }
		#FROM = { clr_country_flag = random_dip_7_incident_flag }
		FROM = { clr_country_flag = random_dip_8_incident_flag }
		FROM = { clr_country_flag = random_dip_9_incident_flag }
		FROM = { clr_country_flag = random_dip_10_incident_flag }
		FROM = { clr_country_flag = random_dip_11_incident_flag }

		#clr_country_flag = random_dip_0_incident_active_flag
		#clr_country_flag = random_dip_2_incident_active_flag
		#clr_country_flag = random_dip_4_incident_active_flag
		clr_country_flag = random_dip_6_incident_active_flag
		#clr_country_flag = random_dip_8_incident_active_flag
		#clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.6 }
	}	
	option = {  # Good!
		name = random_diplo_end.0.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.0.a executed"
		trigger = { has_country_flag = random_dip_8_incident_active_flag }
		clr_country_flag = random_dip_0_incident_flag
		clr_country_flag = random_dip_1_incident_flag
		clr_country_flag = random_dip_2_incident_flag
		clr_country_flag = random_dip_3_incident_flag
		clr_country_flag = random_dip_4_incident_flag
		clr_country_flag = random_dip_5_incident_flag
		clr_country_flag = random_dip_6_incident_flag
		clr_country_flag = random_dip_7_incident_flag
		#clr_country_flag = random_dip_8_incident_flag
		#clr_country_flag = random_dip_9_incident_flag
		clr_country_flag = random_dip_10_incident_flag
		clr_country_flag = random_dip_11_incident_flag

		FROM = { clr_country_flag = random_dip_0_incident_flag }
		FROM = { clr_country_flag = random_dip_1_incident_flag }
		FROM = { clr_country_flag = random_dip_2_incident_flag }
		FROM = { clr_country_flag = random_dip_3_incident_flag }
		FROM = { clr_country_flag = random_dip_4_incident_flag }
		FROM = { clr_country_flag = random_dip_5_incident_flag }
		FROM = { clr_country_flag = random_dip_6_incident_flag }
		FROM = { clr_country_flag = random_dip_7_incident_flag }
		#FROM = { clr_country_flag = random_dip_8_incident_flag }
		#FROM = { clr_country_flag = random_dip_9_incident_flag }
		FROM = { clr_country_flag = random_dip_10_incident_flag }
		FROM = { clr_country_flag = random_dip_11_incident_flag }

		#clr_country_flag = random_dip_0_incident_active_flag
		#clr_country_flag = random_dip_2_incident_active_flag
		#clr_country_flag = random_dip_4_incident_active_flag
		#clr_country_flag = random_dip_6_incident_active_flag
		clr_country_flag = random_dip_8_incident_active_flag
		#clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.8 }
	}	
	option = {  # Good!
		name = random_diplo_end.0.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.0.a executed"
		trigger = { has_country_flag = random_dip_10_incident_active_flag }
		clr_country_flag = random_dip_0_incident_flag
		clr_country_flag = random_dip_1_incident_flag
		clr_country_flag = random_dip_2_incident_flag
		clr_country_flag = random_dip_3_incident_flag
		clr_country_flag = random_dip_4_incident_flag
		clr_country_flag = random_dip_5_incident_flag
		clr_country_flag = random_dip_6_incident_flag
		clr_country_flag = random_dip_7_incident_flag
		clr_country_flag = random_dip_8_incident_flag
		clr_country_flag = random_dip_9_incident_flag
		#clr_country_flag = random_dip_10_incident_flag
		#clr_country_flag = random_dip_11_incident_flag

		FROM = { clr_country_flag = random_dip_0_incident_flag }
		FROM = { clr_country_flag = random_dip_1_incident_flag }
		FROM = { clr_country_flag = random_dip_2_incident_flag }
		FROM = { clr_country_flag = random_dip_3_incident_flag }
		FROM = { clr_country_flag = random_dip_4_incident_flag }
		FROM = { clr_country_flag = random_dip_5_incident_flag }
		FROM = { clr_country_flag = random_dip_6_incident_flag }
		FROM = { clr_country_flag = random_dip_7_incident_flag }
		FROM = { clr_country_flag = random_dip_8_incident_flag }
		FROM = { clr_country_flag = random_dip_9_incident_flag }
		#FROM = { clr_country_flag = random_dip_10_incident_flag }
		#FROM = { clr_country_flag = random_dip_11_incident_flag }

		#clr_country_flag = random_dip_0_incident_active_flag
		#clr_country_flag = random_dip_2_incident_active_flag
		#clr_country_flag = random_dip_4_incident_active_flag
		#clr_country_flag = random_dip_6_incident_active_flag
		#clr_country_flag = random_dip_8_incident_active_flag
		clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.10 }
	}	
}
country_event = {	# Crises turned out bad
	id = random_diplo_end.1
	title = random_diplo_end.1.t
	desc = random_diplo_end.1.d
	picture = GFX_report_event_worker_protests
	
	is_triggered_only = yes
	
	option = {  # Awww!
		name = random_diplo_end.1.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.1.a executed"
		trigger = { has_country_flag = random_dip_0_incident_active_flag }
		#clr_country_flag = random_dip_0_incident_flag
		#clr_country_flag = random_dip_1_incident_flag
		clr_country_flag = random_dip_2_incident_flag
		clr_country_flag = random_dip_3_incident_flag
		clr_country_flag = random_dip_4_incident_flag
		clr_country_flag = random_dip_5_incident_flag
		clr_country_flag = random_dip_6_incident_flag
		clr_country_flag = random_dip_7_incident_flag
		clr_country_flag = random_dip_8_incident_flag
		clr_country_flag = random_dip_9_incident_flag
		clr_country_flag = random_dip_10_incident_flag
		clr_country_flag = random_dip_11_incident_flag

		#FROM = { clr_country_flag = random_dip_0_incident_flag }
		#FROM = { clr_country_flag = random_dip_1_incident_flag }
		FROM = { clr_country_flag = random_dip_2_incident_flag }
		FROM = { clr_country_flag = random_dip_3_incident_flag }
		FROM = { clr_country_flag = random_dip_4_incident_flag }
		FROM = { clr_country_flag = random_dip_5_incident_flag }
		FROM = { clr_country_flag = random_dip_6_incident_flag }
		FROM = { clr_country_flag = random_dip_7_incident_flag }
		FROM = { clr_country_flag = random_dip_8_incident_flag }
		FROM = { clr_country_flag = random_dip_9_incident_flag }
		FROM = { clr_country_flag = random_dip_10_incident_flag }
		FROM = { clr_country_flag = random_dip_11_incident_flag }

		clr_country_flag = random_dip_0_incident_active_flag
		#clr_country_flag = random_dip_2_incident_active_flag
		#clr_country_flag = random_dip_4_incident_active_flag
		#clr_country_flag = random_dip_6_incident_active_flag
		#clr_country_flag = random_dip_8_incident_active_flag
		#clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.1 }
	}	
	option = {  # Awww!
		name = random_diplo_end.1.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.1.a executed"
		trigger = { has_country_flag = random_dip_2_incident_active_flag }
		clr_country_flag = random_dip_0_incident_flag
		clr_country_flag = random_dip_1_incident_flag
		#clr_country_flag = random_dip_2_incident_flag
		#clr_country_flag = random_dip_3_incident_flag
		clr_country_flag = random_dip_4_incident_flag
		clr_country_flag = random_dip_5_incident_flag
		clr_country_flag = random_dip_6_incident_flag
		clr_country_flag = random_dip_7_incident_flag
		clr_country_flag = random_dip_8_incident_flag
		clr_country_flag = random_dip_9_incident_flag
		clr_country_flag = random_dip_10_incident_flag
		clr_country_flag = random_dip_11_incident_flag

		FROM = { clr_country_flag = random_dip_0_incident_flag }
		FROM = { clr_country_flag = random_dip_1_incident_flag }
		#FROM = { clr_country_flag = random_dip_2_incident_flag }
		#FROM = { clr_country_flag = random_dip_3_incident_flag }
		FROM = { clr_country_flag = random_dip_4_incident_flag }
		FROM = { clr_country_flag = random_dip_5_incident_flag }
		FROM = { clr_country_flag = random_dip_6_incident_flag }
		FROM = { clr_country_flag = random_dip_7_incident_flag }
		FROM = { clr_country_flag = random_dip_8_incident_flag }
		FROM = { clr_country_flag = random_dip_9_incident_flag }
		FROM = { clr_country_flag = random_dip_10_incident_flag }
		FROM = { clr_country_flag = random_dip_11_incident_flag }

		#clr_country_flag = random_dip_0_incident_active_flag
		clr_country_flag = random_dip_2_incident_active_flag
		#clr_country_flag = random_dip_4_incident_active_flag
		#clr_country_flag = random_dip_6_incident_active_flag
		#clr_country_flag = random_dip_8_incident_active_flag
		#clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.3 }
	}	
	option = {  # Awww!
		name = random_diplo_end.1.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.1.a executed"
		trigger = { has_country_flag = random_dip_4_incident_active_flag }
		clr_country_flag = random_dip_0_incident_flag
		clr_country_flag = random_dip_1_incident_flag
		clr_country_flag = random_dip_2_incident_flag
		clr_country_flag = random_dip_3_incident_flag
		#clr_country_flag = random_dip_4_incident_flag
		#clr_country_flag = random_dip_5_incident_flag
		clr_country_flag = random_dip_6_incident_flag
		clr_country_flag = random_dip_7_incident_flag
		clr_country_flag = random_dip_8_incident_flag
		clr_country_flag = random_dip_9_incident_flag
		clr_country_flag = random_dip_10_incident_flag
		clr_country_flag = random_dip_11_incident_flag

		FROM = { clr_country_flag = random_dip_0_incident_flag }
		FROM = { clr_country_flag = random_dip_1_incident_flag }
		FROM = { clr_country_flag = random_dip_2_incident_flag }
		FROM = { clr_country_flag = random_dip_3_incident_flag }
		#FROM = { clr_country_flag = random_dip_4_incident_flag }
		#FROM = { clr_country_flag = random_dip_5_incident_flag }
		FROM = { clr_country_flag = random_dip_6_incident_flag }
		FROM = { clr_country_flag = random_dip_7_incident_flag }
		FROM = { clr_country_flag = random_dip_8_incident_flag }
		FROM = { clr_country_flag = random_dip_9_incident_flag }
		FROM = { clr_country_flag = random_dip_10_incident_flag }
		FROM = { clr_country_flag = random_dip_11_incident_flag }

		#clr_country_flag = random_dip_0_incident_active_flag
		#clr_country_flag = random_dip_2_incident_active_flag
		clr_country_flag = random_dip_4_incident_active_flag
		#clr_country_flag = random_dip_6_incident_active_flag
		#clr_country_flag = random_dip_8_incident_active_flag
		#clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.5 }
	}	
	option = {  # Awww!
		name = random_diplo_end.1.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.1.a executed"
		trigger = { has_country_flag = random_dip_6_incident_active_flag }
		clr_country_flag = random_dip_0_incident_flag
		clr_country_flag = random_dip_1_incident_flag
		clr_country_flag = random_dip_2_incident_flag
		clr_country_flag = random_dip_3_incident_flag
		clr_country_flag = random_dip_4_incident_flag
		clr_country_flag = random_dip_5_incident_flag
		#clr_country_flag = random_dip_6_incident_flag
		#clr_country_flag = random_dip_7_incident_flag
		clr_country_flag = random_dip_8_incident_flag
		clr_country_flag = random_dip_9_incident_flag
		clr_country_flag = random_dip_10_incident_flag
		clr_country_flag = random_dip_11_incident_flag

		FROM = { clr_country_flag = random_dip_0_incident_flag }
		FROM = { clr_country_flag = random_dip_1_incident_flag }
		FROM = { clr_country_flag = random_dip_2_incident_flag }
		FROM = { clr_country_flag = random_dip_3_incident_flag }
		FROM = { clr_country_flag = random_dip_4_incident_flag }
		FROM = { clr_country_flag = random_dip_5_incident_flag }
		#FROM = { clr_country_flag = random_dip_6_incident_flag }
		#FROM = { clr_country_flag = random_dip_7_incident_flag }
		FROM = { clr_country_flag = random_dip_8_incident_flag }
		FROM = { clr_country_flag = random_dip_9_incident_flag }
		FROM = { clr_country_flag = random_dip_10_incident_flag }
		FROM = { clr_country_flag = random_dip_11_incident_flag }

		#clr_country_flag = random_dip_0_incident_active_flag
		#clr_country_flag = random_dip_2_incident_active_flag
		#clr_country_flag = random_dip_4_incident_active_flag
		clr_country_flag = random_dip_6_incident_active_flag
		#clr_country_flag = random_dip_8_incident_active_flag
		#clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.7 }
	}	
	option = {  # Awww!
		name = random_diplo_end.1.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.1.a executed"
		trigger = { has_country_flag = random_dip_8_incident_active_flag }
		clr_country_flag = random_dip_0_incident_flag
		clr_country_flag = random_dip_1_incident_flag
		clr_country_flag = random_dip_2_incident_flag
		clr_country_flag = random_dip_3_incident_flag
		clr_country_flag = random_dip_4_incident_flag
		clr_country_flag = random_dip_5_incident_flag
		clr_country_flag = random_dip_6_incident_flag
		clr_country_flag = random_dip_7_incident_flag
		#clr_country_flag = random_dip_8_incident_flag
		#clr_country_flag = random_dip_9_incident_flag
		clr_country_flag = random_dip_10_incident_flag
		clr_country_flag = random_dip_11_incident_flag

		FROM = { clr_country_flag = random_dip_0_incident_flag }
		FROM = { clr_country_flag = random_dip_1_incident_flag }
		FROM = { clr_country_flag = random_dip_2_incident_flag }
		FROM = { clr_country_flag = random_dip_3_incident_flag }
		FROM = { clr_country_flag = random_dip_4_incident_flag }
		FROM = { clr_country_flag = random_dip_5_incident_flag }
		FROM = { clr_country_flag = random_dip_6_incident_flag }
		FROM = { clr_country_flag = random_dip_7_incident_flag }
		#FROM = { clr_country_flag = random_dip_8_incident_flag }
		#FROM = { clr_country_flag = random_dip_9_incident_flag }
		FROM = { clr_country_flag = random_dip_10_incident_flag }
		FROM = { clr_country_flag = random_dip_11_incident_flag }

		#clr_country_flag = random_dip_0_incident_active_flag
		#clr_country_flag = random_dip_2_incident_active_flag
		#clr_country_flag = random_dip_4_incident_active_flag
		#clr_country_flag = random_dip_6_incident_active_flag
		clr_country_flag = random_dip_8_incident_active_flag
		#clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.9 }
	}	
	option = {  # Awww!
		name = random_diplo_end.1.a
		log = "[GetDateText]: [This.GetName]: random_diplo_end.1.a executed"
		trigger = { has_country_flag = random_dip_10_incident_active_flag }
		clr_country_flag = random_dip_0_incident_flag
		clr_country_flag = random_dip_1_incident_flag
		clr_country_flag = random_dip_2_incident_flag
		clr_country_flag = random_dip_3_incident_flag
		clr_country_flag = random_dip_4_incident_flag
		clr_country_flag = random_dip_5_incident_flag
		clr_country_flag = random_dip_6_incident_flag
		clr_country_flag = random_dip_7_incident_flag
		clr_country_flag = random_dip_8_incident_flag
		clr_country_flag = random_dip_9_incident_flag
		#clr_country_flag = random_dip_10_incident_flag
		#clr_country_flag = random_dip_11_incident_flag

		FROM = { clr_country_flag = random_dip_0_incident_flag }
		FROM = { clr_country_flag = random_dip_1_incident_flag }
		FROM = { clr_country_flag = random_dip_2_incident_flag }
		FROM = { clr_country_flag = random_dip_3_incident_flag }
		FROM = { clr_country_flag = random_dip_4_incident_flag }
		FROM = { clr_country_flag = random_dip_5_incident_flag }
		FROM = { clr_country_flag = random_dip_6_incident_flag }
		FROM = { clr_country_flag = random_dip_7_incident_flag }
		FROM = { clr_country_flag = random_dip_8_incident_flag }
		FROM = { clr_country_flag = random_dip_9_incident_flag }
		#FROM = { clr_country_flag = random_dip_10_incident_flag }
		#FROM = { clr_country_flag = random_dip_11_incident_flag }

		#clr_country_flag = random_dip_0_incident_active_flag
		#clr_country_flag = random_dip_2_incident_active_flag
		#clr_country_flag = random_dip_4_incident_active_flag
		#clr_country_flag = random_dip_6_incident_active_flag
		#clr_country_flag = random_dip_8_incident_active_flag
		clr_country_flag = random_dip_10_incident_active_flag
		
		every_country = { news_event = random_diplo_news.11 }
	}
}

# End News Events
news_event = {	 # Incident 1: Good ending
	id = random_diplo_news.0
	title = random_diplo_news.0.t
	desc = random_diplo_news.0.d
	picture = GFX_report_event_worker_protests
	
	is_triggered_only = yes

	option = {  # That was a close call.
		name = random_diplo_news.0.a
		log = "[GetDateText]: [This.GetName]: random_diplo_news.0.a executed"
	}
}
news_event = {	 # Incident 1: Bad ending
	id = random_diplo_news.1
	title = random_diplo_news.1.t
	desc = random_diplo_news.1.d
	picture = GFX_report_event_worker_protests
	
	is_triggered_only = yes

	option = {  # I need to twitter this.
		name = random_diplo_news.1.a
		log = "[GetDateText]: [This.GetName]: random_diplo_news.1.a executed"
	}
}