focus_tree = {
	id = United_states-gearz
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = USA
		}
	}
	default = no
	focus = {
		id = USA_350_ship
		icon = GFX_goal_unknown
		cost = 0.00
		x = 24
		y = 4
		
		completion_reward = {
			add_ideas = IDEA_USA_350_ship
		}

	}
	focus = {
		id = USA_EXPAND_ARMY
		icon = GFX_goal_unknown
		cost = 0.00
		x = 22
		y = 5
		
		completion_reward = {
			add_ideas = IDEA_USA_EXPAND_ARMY
		}
		
	}
	focus = {
		id = USA_air_dominance
		icon = GFX_goal_unknown
		cost = 0.00
		x = 26
		y = 5
		
		completion_reward = {
			add_ideas = IDEA_USA_air_dominance
			add_tech_bonus = {
				name = USA_air_dominance
				bonus = 0.5
				ahead_reduction = 1
				uses = 1
				category = strategic_bomber
			}
			
		}

	}
	focus = {
		id = USA_MAGA
		icon = GFX_USA_MAGA
		cost = 0.00
		x = 16
		y = 1

	}
	focus = {
		id = USA_PACIFIC_POWER
		icon = GFX_goal_generic_wolf_pack
		cost = 0.00
		x = 27
		y = 9
		
		available = {
			has_completed_focus = USA_TRUMP_CABINET
		}

	}
	focus = {
		id = USA_NEW_MIDDLE_EAST
		icon = GFX_goal_continuous_boost_freedom
		cost = 0.00
		x = 14
		y = 9
		
		available = {
			has_completed_focus = USA_TRUMP_CABINET
		}

	}
	focus = {
		id = USA_THE_WEST
		icon = GFX_goal_unknown
		cost = 0.00
		x = 3
		y = 9
		
		available = {
			has_completed_focus = USA_TRUMP_CABINET
		}

	}
	focus = {
		id = USA_leave_nato
		icon = GFX_USA_disintegration_of_nato
		cost = 0.00
		
		available = {
			has_government = nationalist
		}
		
		prerequisite = {
			focus = USA_THE_WEST
			focus = USA_2017_budget_congress
		}
		mutually_exclusive = {
			focus = USA_FOCUS_NATO
		}
		x = 0
		y = 10
		
		completion_reward = {
			dismantle_faction = yes
		}

	}
	focus = {
		id = USA_REAPROACH_RUSSIA
		icon = GFX_USA_reapproach_russia
		cost = 0.00
		prerequisite = {
			focus = USA_leave_nato
		}
		x = 0
		y = 11
		
		completion_reward = {
			SOV = {
				add_opinion_modifier = { target = USA modifier = USA_REAPROACH_RUSSIA }
				reverse_add_opinion_modifier = { target = USA modifier = USA_REAPROACH_RUSSIA }
			}
		}
		

	}
	focus = {
		id = USA_NAZI_EUROPE
		icon = GFX_USA_goal_nazi_eu
		cost = 0.00
		
		available = {
			is_in_faction_with = SOV
		}
		
		prerequisite = {
			focus = USA_REAPROACH_RUSSIA
		}
		x = 0
		y = 12
		
		completion_reward = {
			every_country = {
				limit = {
					not = {has_government = nationalist}
					has_idea = EU_member
				}
				add_opinion_modifier = { target = USA modifier = embargo }
				reverse_add_opinion_modifier = { target = USA modifier = embargo }
				
			}
		
		}

	}
	focus = {
		id = USA_TRUMP_CABINET
		icon = GFX_USA_cabinet_pick
		cost = 0.00
		prerequisite = {
			focus = USA_MAGA
		}
		x = 16
		y = 2

	}
	focus = {
		id = USA_TRUMP_promises
		icon = GFX_focus_generic_support_the_left_right
		cost = 0.00
		prerequisite = {
			focus = USA_TRUMP_CABINET
		}
		x = 9
		y = 3
		
		completion_reward = {
			add_popularity = { ideology = nationalist popularity = 0.015 }
			add_national_unity = -0.01
		}
		

	}
	focus = {
		id = USA_withdraw_tpp
		icon = GFX_goal_no_tpp
		cost = 0.00
		prerequisite = {
			focus = USA_TRUMP_promises
		}
		x = 3
		y = 4
		
		completion_reward = {
			add_popularity = { ideology = nationalist popularity = 0.015 }
			add_national_unity = -0.01
			
			every_country = {
				limit = {
					not = {has_government = nationalist}
					or = {
						tag = AST
						tag = BRU
						tag = CAN
						tag = CHL
						tag = JAP
						tag = MAY
						tag = MEX
						tag = NZL
						tag = PRU
						tag = SIN
						tag = VIE
						
					}
				}
				add_opinion_modifier = {
					target = USA
					modifier = USA_Withdraw_TPP
				}
				reverse_add_opinion_modifier = {
					target = USA
					modifier = USA_Withdraw_TPP
				}
				
			}
		}

	}
	focus = {
		id = USA_withdraw_TTIP
		icon = GFX_USA_withdraw_TTIP
		cost = 0.00
		prerequisite = {
			focus = USA_withdraw_tpp
		}
		x = 2
		y = 5

	}
	focus = {
		id = USA_reneotiate_NAFTA
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_withdraw_tpp
		}
		x = 4
		y = 5
		
		completion_reward = {
			add_popularity = { ideology = nationalist popularity = 0.015 }
			add_national_unity = -0.01
			
			every_country = {
				limit = {
					not = {has_government = nationalist}
					has_idea = EU_member
				}
				add_opinion_modifier = {
					target = USA
					modifier = USA_Withdraw_TTIP
				}
				reverse_add_opinion_modifier = {
					target = USA
					modifier = USA_Withdraw_TTIP
				}
				
			}
		}

	}
	focus = {
		id = USA_Ban_muslims
		icon = GFX_USA_ban_muslim
		cost = 0.00
		prerequisite = {
			focus = USA_TRUMP_promises
		}
		x = 6
		y = 4
		
		completion_reward = {
			add_popularity = { ideology = nationalist popularity = 0.015 }
			add_national_unity = -0.01
		}

	}
	focus = {
		id = USA_repeal_obama_care
		icon = GFX_USA_repeal_obama
		cost = 0.00
		prerequisite = {
			focus = USA_TRUMP_promises
		}
		x = 9
		y = 4
		
		completion_reward = {
			add_popularity = { ideology = nationalist popularity = 0.015 }
			add_national_unity = -0.01
		}

	}
	focus = {
		id = USA_AXIS_EVIL
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_NEW_MIDDLE_EAST
		}
		x = 9
		y = 10

	}
	focus = {
		id = USA_THE_WALL
		icon = GFX_USA_the_wall
		cost = 0.00
		prerequisite = {
			focus = USA_TRUMP_promises
		}
		x = 13
		y = 4
		
		completion_reward = {
			add_popularity = { ideology = nationalist popularity = 0.015 }
			add_national_unity = -0.01
		}

	}
	focus = {
		id = USA_CUT_DOS
		icon = GFX_USA_gut_dos
		cost = 0.00
		prerequisite = {
			focus = USA_withdraw_tpp
		}
		prerequisite = {
			focus = USA_Ban_muslims
		}
		prerequisite = {
			focus = USA_repeal_obama_care
		}
		prerequisite = {
			focus = USA_THE_WALL
		}
		x = 7
		y = 6

	}
	focus = {
		id = USA_CUT_USDA
		icon = GFX_USA_gut_usda
		cost = 0.00
		prerequisite = {
			focus = USA_CUT_DOS
		}
		x = 5
		y = 7

	}
	focus = {
		id = USA_CUT_EPA
		icon = GFX_USA_gut_epa
		cost = 0.00
		prerequisite = {
			focus = USA_CUT_DOS
		}
		x = 9
		y = 7

	}
	focus = {
		id = USA_Hire_USBP
		icon = GFX_USA_USBP
		cost = 0.00
		prerequisite = {
			focus = USA_THE_WALL
		}
		x = 13
		y = 5
		
		completion_reward = {
			add_popularity = { ideology = nationalist popularity = 0.015 }
			add_national_unity = -0.01
		}

	}
	focus = {
		id = USA_greatest_fence
		icon = GFX_USA_fence
		cost = 0.00
		prerequisite = {
			focus = USA_Hire_USBP
		}
		x = 13
		y = 6
		
		completion_reward = {
			add_popularity = { ideology = nationalist popularity = 0.015 }
			add_national_unity = -0.01
		}

	}
	focus = {
		id = USA_offer_citizenship
		icon = GFX_USA_amnesty
		cost = 0.00
		prerequisite = {
			focus = USA_greatest_fence
		}
		x = 11
		y = 7
		
		completion_reward = {
			add_popularity = { ideology = democratic popularity = 0.02 }
			add_national_unity = 0.05

		}

	}
	focus = {
		id = USA_DEPORT_BAD_ONES
		icon = GFX_USA_deport_amigos
		cost = 0.00
		prerequisite = {
			focus = USA_greatest_fence
		}
		mutually_exclusive = {
			focus = USA_offer_citizenship
		}
		x = 13
		y = 7
		
		completion_reward = {
			add_popularity = { ideology = nationalist popularity = 0.02 }
			add_national_unity = -0.015
			
			811 = { #Southern California
				add_manpower = -306000
			} 
			795 = { #Florida
				add_manpower = -153000
			}
			769 = { #New York
				add_manpower = -139500
			}
			800 = { #Texas Gulf Coast
				add_manpower = -136800
			}
			801 = { #Northeastern Texas
				add_manpower = -122400
			}
			812 = { #Silicon Valley
				add_manpower = -95400
			}
			770 = { #New Jersey
				add_manpower = -90000
			}
			778 = { #Illinois
				add_manpower = -81000
			}
			
			792 = { #Georgia
				add_manpower = -67500
			}
			
			790 = { #North Carolina
				add_manpower = -63000
			}
			
			806 = { #Arizona
				add_manpower = -58500
			}
			
			773 = { #Virginia
				add_manpower = -54000
			}
			
			772 = { #Maryland
				add_manpower = -49500
			}
			
			809 = { #Washington
				add_manpower = -45000
			}
			
			767 = { #Massachusetts
				add_manpower = -37800
			}
			
			802 = { #Western Texas
				add_manpower = -37800
			}
			
			807 = { #Nevada
				add_manpower = -37800
			}
			
			804 = { #Colorado
				add_manpower = -36000
			}
			
			771 = { #Pennsylvania
				add_manpower = -32400
			}
			
			768 = { #Connecticut
				add_manpower = -27000
			}
			777 = { #Michigan
				add_manpower = -23400
			}
			810 = { #Oregon
				add_manpower = -23400
			}
			793 = { #Tennessee
				add_manpower = -21600
			}
			813 = { #Jefferson
				add_manpower = -21600
			}
			776 = { #Indiana
				add_manpower = -19800
			}
			780 = { #Minnesota
				add_manpower = -18000
			}
			805 = { #Utah
				add_manpower = -18000
			}
			775 = { #Ohio
				add_manpower = -17100
			}
			799 = { #Oklahoma
				add_manpower = -17100
			}
			791 = { #South Carolina
				add_manpower = -15300
			}
			803 = { #New Mexico
				add_manpower = -15300
			}
			779 = { #Wisconsin
				add_manpower = -14400
			}
			784 = { #Kansas
				add_manpower = -13500
			}
			797 = { #Louisiana
				add_manpower = -12600
			}
			798 = { #Arkansas
				add_manpower = -12600
			}
			794 = { #Alabama
				add_manpower = -11700
			}
			783 = { #Missouri
				add_manpower = -9900
			}
			
			
		}

	}
	focus = {
		id = USA_ISR_TWO_STATE
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_NEW_MIDDLE_EAST
		}
		mutually_exclusive = {
			focus = USA_ISREAL_ONE_STATE
		}
		x = 13
		y = 10

	}
	focus = {
		id = USA_DEPORT_ALL_ILLEGALS
		icon = GFX_USA_deport_all
		cost = 0.00
		prerequisite = {
			focus = USA_greatest_fence
		}
		mutually_exclusive = {
			focus = USA_DEPORT_BAD_ONES
		}
		x = 15
		y = 7
		
		completion_reward = {
			add_popularity = { ideology = nationalist popularity = 0.04 }
			add_national_unity = -0.02
			
			811 = { #Southern California
				add_manpower = -1700000
			} 
			795 = { #Florida
				add_manpower = -850000
			}
			769 = { #New York
				add_manpower = -775000
			}
			800 = { #Texas Gulf Coast
				add_manpower = -760000
			}
			801 = { #Northeastern Texas
				add_manpower = -680000
			}
			812 = { #Silicon Valley
				add_manpower = -530000
			}
			770 = { #New Jersey
				add_manpower = -500000
			}
			778 = { #Illinois
				add_manpower = -450000
			}
			
			792 = { #Georgia
				add_manpower = -375000
			}
			
			790 = { #North Carolina
				add_manpower = -350000
			}
			
			806 = { #Arizona
				add_manpower = -325000
			}
			
			773 = { #Virginia
				add_manpower = -300000
			}
			
			772 = { #Maryland
				add_manpower = -275000
			}
			
			809 = { #Washington
				add_manpower = -250000
			}
			
			767 = { #Massachusetts
				add_manpower = -210000
			}
			
			802 = { #Western Texas
				add_manpower = -210000
			}
			
			807 = { #Nevada
				add_manpower = -210000
			}
			
			804 = { #Colorado
				add_manpower = -180000
			}
			
			771 = { #Pennsylvania
				add_manpower = -150000
			}
			
			768 = { #Connecticut
				add_manpower = -130000
			}
			777 = { #Michigan
				add_manpower = -130000
			}
			810 = { #Oregon
				add_manpower = -120000
			}
			793 = { #Tennessee
				add_manpower = -120000
			}
			813 = { #Jefferson
				add_manpower = -110000
			}
			776 = { #Indiana
				add_manpower = -100000
			}
			780 = { #Minnesota
				add_manpower = -100000
			}
			805 = { #Utah
				add_manpower = -95000
			}
			775 = { #Ohio
				add_manpower = -95000
			}
			799 = { #Oklahoma
				add_manpower = -85000
			}
			791 = { #South Carolina
				add_manpower = -85000
			}
			803 = { #New Mexico
				add_manpower = -80000
			}
			779 = { #Wisconsin
				add_manpower = -75000
			}
			784 = { #Kansas
				add_manpower = -70000
			}
			797 = { #Louisiana
				add_manpower = -70000
			}
			798 = { #Arkansas
				add_manpower = -65000
			}
			794 = { #Alabama
				add_manpower = -55000
			}
			783 = { #Missouri
				add_manpower = -50000
			}
			
		}

	}
	focus = {
		id = USA_REASURE_ALLIES
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_NEW_MIDDLE_EAST
		}
		mutually_exclusive = {
			focus = USA_ABANDON_ALLIES
		}
		x = 16
		y = 10

	}
	focus = {
		id = US_SAUDI_GUNS
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_REASURE_ALLIES
		}
		x = 15
		y = 11

	}
	focus = {
		id = USA_ABANDON_ALLIES
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_NEW_MIDDLE_EAST
		}
		mutually_exclusive = {
			focus = USA_REASURE_ALLIES
		}
		x = 18
		y = 10

	}
	focus = {
		id = USA_TURKS_KURDS
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_REASURE_ALLIES
		}
		prerequisite = {
			focus = USA_ABANDON_ALLIES
		}
		x = 17
		y = 11

	}
	focus = {
		id = USA_AMERICA_FIRST
		icon = GFX_goal_generic_construct_infrastructure
		cost = 0.00
		prerequisite = {
			focus = USA_TRUMP_CABINET
		}
		mutually_exclusive = {
			focus = USA_PARTNERS_ALLIES
			focus = USA_PAX_AMERICANA
		}
		x = 20
		y = 4

	}
	focus = {
		id = USA_roads
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_AMERICA_FIRST
		}
		x = 20
		y = 5
		
		completion_reward = {
			add_ideas = IDEA_USA_roads
		}

	}
	focus = {
		id = USA_factories
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_roads
		}
		x = 20
		y = 6
		
		completion_reward = {
			add_ideas = IDEA_USA_factories
		}

	}
	focus = {
		id = USA_ports
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_factories
		}
		x = 20
		y = 7
		
		completion_reward = {
			add_ideas = IDEA_USA_ports
		}

	}
	focus = {
		id = USA_PIVOT_ASIA_2
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_PACIFIC_POWER
		}
		x = 22
		y = 10

	}
	focus = {
		id = USA_SCS_TRUMP
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_PIVOT_ASIA_2
		}
		x = 20
		y = 11

	}
	focus = {
		id = USA_RULE_WAVES
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_PIVOT_ASIA_2
		}
		x = 22
		y = 11

	}
	focus = {
		id = USA_PROTECT_SCS
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_SCS_TRUMP
		}
		prerequisite = {
			focus = USA_RULE_WAVES
		}
		x = 21
		y = 12

	}
	focus = {
		id = USA_PAX_AMERICANA
		icon = GFX_goal_generic_allies_build_infantry
		cost = 0.00
		prerequisite = {
			focus = USA_TRUMP_CABINET
		}
		mutually_exclusive = {
			focus = USA_AMERICA_FIRST
			focus = USA_PARTNERS_ALLIES
		}
		x = 24
		y = 3

	}
	focus = {
		id = USA_Fund_marines
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = newfocus_9
		}
		prerequisite = {
			focus = USA_air_dominance
		}
		prerequisite = {
			focus = USA_air_dominance
		}
		prerequisite = {
			focus = USA_350_ship
		}
		x = 24
		y = 6
		
		completion_reward = {
			add_ideas = IDEA_USA_Fund_marines
			add_tech_bonus = {
				name = USA_FUND_APC
				bonus = 0.5
				ahead_reduction = 0
				uses = 1
				category = Amph_APC
			}
		}

	}
	focus = {
		id = USA_REFORM_PROCURMENT
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_Fund_marines
		}
		x = 24
		y = 7
		
		completion_reward = {
			remove_ideas = USA_BAD_PROCURMENT
		}

	}
	focus = {
		id = USA_RECOGNIZE_TAIWAN
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_PIVOT_ASIA_2
		}
		x = 24
		y = 11
		
		completion_reward = {
			TAI = {
				add_opinion_modifier = { target = USA modifier = USA_TAIWAN_ALLIANCE }
				reverse_add_opinion_modifier = { target = USA modifier = USA_TAIWAN_ALLIANCE }
				
			}	
			
		}

	}
	focus = {
		id = USA_ALLY_TAIWAN
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_RECOGNIZE_TAIWAN
		}
		x = 24
		y = 12

	}
	focus = {
		id = USA_CONFRONT_NK
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_PACIFIC_POWER
		}
		x = 27
		y = 10
		
		available = {
			NKO = {
				has_country_flag = ballistic_misisles
			}
		}

	}
	focus = {
		id = USA_PREASURE_CHINA
		icon = GFX_goal_unknown
		cost = 0.00
		
		available = {
			NKO = {
				has_country_flag = Nuclear_ballistic_misisles
			}
		}
		
		prerequisite = {
			focus = USA_CONFRONT_NK
		}
		x = 26
		y = 11
		
		

	}
	focus = {
		id = USA_NK_HAMMER_NAIL
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_CONFRONT_NK
		}
		x = 28
		y = 11
		
		available = {
			NKO = {
				has_country_flag = ballistic_misisles
			}
		}
		
		completion_reward = {
			add_threat = 10
		}

	}
	focus = {
		id = USA_OPLAN_5027
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_PREASURE_CHINA
			focus = USA_NK_HAMMER_NAIL
		}
		x = 27
		y = 12
		
		completion_reward = {
			create_wargoal = {
				type = liberate_wargoal
				target = NKO
			}
		}

	}
	focus = {
		id = USA_PARTNERS_ALLIES
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_TRUMP_CABINET
		}
		mutually_exclusive = {
			focus = USA_AMERICA_FIRST
			focus = USA_PAX_AMERICANA
		}
		x = 29
		y = 4

	}
	focus = {
		id = USA_NEGOTIATE_TTIP
		icon = GFX_USA_TTIP
		cost = 0.00
		prerequisite = {
			focus = USA_PARTNERS_ALLIES
		}
		x = 28
		y = 5
		
		completion_reward = {
			every_country = {
				limit = {
					not = {has_government = nationalist}
					has_idea = EU_member
				}
				add_opinion_modifier = {
					target = USA
					modifier = USA_Negotiate_TTIP
				}
				reverse_add_opinion_modifier = {
					target = USA
					modifier = USA_Negotiate_TTIP
				}
				
			}
		}

	}
	focus = {
		id = USA_REJOIN_TPP
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_PARTNERS_ALLIES
		}
		x = 30
		y = 5

	}
	focus = {
		id = USA_NEGOTIATE_TISA
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_NEGOTIATE_TTIP
		}
		prerequisite = {
			focus = USA_REJOIN_TPP
		}
		x = 29
		y = 6

	}
	focus = {
		id = USA_JAP_RELATIONS
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_PACIFIC_POWER
		}
		x = 30
		y = 10

	}
	focus = {
		id = USA_JAP_PACIFIC
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_JAP_RELATIONS
		}
		x = 30
		y = 11

	}
	focus = {
		id = USA_JAP_NATO
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_JAP_PACIFIC
		}
		x = 30
		y = 12

	}
	focus = {
		id = USA_CUT_DOL
		icon = GFX_USA_gut_dol
		cost = 0.00
		prerequisite = {
			focus = USA_CUT_DOS
		}
		x = 7
		y = 7

	}
	focus = {
		id = USA_2017_budget_congress
		icon = GFX_USA_propose_the_budget
		cost = 0.00
		
		available = { 
			nationalist > 0.42
		}
		
		prerequisite = {
			focus = USA_CUT_USDA
		}
		prerequisite = {
			focus = USA_CUT_DOL
		}
		prerequisite = {
			focus = USA_CUT_EPA
		}
		x = 7
		y = 8
		
		completion_reward = {
			swap_ideas = { remove_idea = medium_taxes add_idea = low_taxes }
			swap_ideas = { remove_idea = medium_social add_idea = low_social }
			
		}

	}
	focus = {
		id = USA_FOCUS_NATO
		icon = GFX_join_nato
		cost = 0.00
		prerequisite = {
			focus = USA_THE_WEST
		}
		mutually_exclusive = {
			focus = USA_leave_nato
		}
		x = 3
		y = 10
		
		completion_reward = {
			every_country = {
				limit = {
					is_in_faction_with = USA
				}
				add_opinion_modifier = {
					target = USA
					modifier = USA_COMMIT_TO_NATO
				}
				reverse_add_opinion_modifier = {
					target = USA
					modifier = USA_COMMIT_TO_NATO
				}
				
			}
		}

	}
	focus = {
		id = USA_TRANSATLANITC
		icon = GFX_USA_transatlantic_focus
		cost = 0.00
		prerequisite = {
			focus = USA_THE_WEST
		}
		x = 6
		y = 10
		
		completion_reward = {
		
			every_country = {
				limit = {
					is_in_faction_with = USA
				}
				add_popularity = { ideology = democratic popularity = 0.1 }
			}
		}

	}
	focus = {
		id = USA_focus_isolation
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_withdraw_TTIP
		}
		prerequisite = {
			focus = USA_reneotiate_NAFTA
		}
		x = 3
		y = 6
		
		completion_reward = {
			add_popularity = { ideology = nationalist popularity = 0.015 }
			add_national_unity = -0.01
			add_ideas = IDEA_USA_isolationism
		}

	}
	focus = {
		id = USA_Speical_relatioship
		icon = GFX_USA_special_relationship
		cost = 0.00
		
		available = {
			ENG = {
				has_government = democratic
			}
		}
		
		prerequisite = {
			focus = USA_TRANSATLANITC
		}
		x = 7
		y = 12
		
		completion_reward = {
			ENG = {
				add_popularity = { ideology = democratic popularity = 0.15 }
				add_opinion_modifier = { target = USA modifier = USA_ENG_SPECIAL }
				reverse_add_opinion_modifier = { target = USA modifier = USA_ENG_SPECIAL }
			}
		}
		

	}
	focus = {
		id = USA_Reinforce_NATO_EAST
		icon = GFX_USA_reinforce_eastern
		cost = 0.00
		prerequisite = {
			focus = USA_FOCUS_NATO
		}
		x = 2
		y = 11
		
		completion_reward = {

			every_country = {
				limit = {
					is_in_faction_with = USA
					or = {
						tag = POL
						tag = LIT
						tag = LAT
						tag = SLO
						tag = HUN
						tag = ROM
						
					}
				}
				add_opinion_modifier = {
					target = USA
					modifier = USA_COMMIT_TO_NATO_EAST
				}
				reverse_add_opinion_modifier = {
					target = USA
					modifier = USA_COMMIT_TO_NATO_EAST
				}
				
			}
			set_country_flag = Reinforce_NATO_EAST
			country_event = { id = USA_FOCUS.1 days = 1 }
		}

	}
	focus = {
		id = USA_ISREAL_ONE_STATE
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_NEW_MIDDLE_EAST
		}
		mutually_exclusive = {
			focus = USA_ISR_TWO_STATE
		}
		x = 11
		y = 10

	}
	focus = {
		id = USA_REINFORCE_Bosphorus
		icon = GFX_USA_reinforce_bosphorus
		cost = 0.00
		prerequisite = {
			focus = USA_FOCUS_NATO
		}
		x = 4
		y = 11
		
		completion_reward = {
			every_country = {
				limit = {
					is_in_faction_with = USA
					or = {
						tag = BUL
						tag = GRE
						tag = ALB
						tag = TUR
						
					}
				}
				add_opinion_modifier = {
					target = USA
					modifier = USA_COMMIT_TO_NATO_SOUTH
				}
				reverse_add_opinion_modifier = {
					target = USA
					modifier = USA_COMMIT_TO_NATO_SOUTH
				}
				
			}
			set_country_flag = REINFORCE_Bosphorus
			country_event = { id = USA_FOCUS.3 days = 1 }
		}

	}
	focus = {
		id = A_UNITED_EUROPE
		icon = GFX_USA_united_eu
		cost = 0.00
		prerequisite = {
			focus = USA_TRANSATLANITC
		}
		x = 5
		y = 12
		
		completion_reward = {
			every_country = {
				limit = {
					not = {has_government = nationalist}
					has_idea = EU_member
				}
				add_ideas = IDEA_USA_UNITED_EUROPE
				
			}
		
		}

	}
	focus = {
		id = USA_COURT_INDIA
		icon = GFX_goal_unknown
		cost = 0.00
		
		available = {
			RAJ = {
				NOT = {
					or = {
						has_government = communism
						has_government = fascism
						has_government = nationalist
						
					}
				}
			}
		}
		
		prerequisite = {
			focus = USA_PACIFIC_POWER
		}
		x = 32
		y = 10
		
		completion_reward = {
			RAJ = {
				add_ideas = IDEA_USA_RAJ_COURT
			}
		}

	}
	focus = {
		id = USA_NATO_7th_enlargement
		icon = GFX_USA_7th_enlargement
		cost = 0.00
		
		available = {
			BOS = {
				has_government = democratic
			}
			GEO = {
				has_government = democratic
			}
			FYR = {
				has_government = democratic
			}
			MNT = {
				has_government = democratic
			}
			
		}
		
		prerequisite = {
			focus = USA_Reinforce_NATO_EAST
		}
		prerequisite = {
			focus = USA_REINFORCE_Bosphorus
		}
		x = 3
		y = 12
		
		completion_reward = {
			
			add_to_faction = BOS
			add_to_faction = GEO
			add_to_faction = FYR
			add_to_faction = MNT
			add_threat = 5
		}

	}
	focus = {
		id = USA_CONFRONT_CHINA
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_ALLY_TAIWAN
		}
		x = 24
		y = 13
		
		completion_reward = {
			create_wargoal = {
				type = liberate_wargoal
				target = CHI
			}
		}

	}
	focus = {
		id = USA_INDIA_ALLIANCE
		icon = GFX_goal_unknown
		cost = 0.00
		
		available = {
			RAJ = {
				has_government = democratic
			}
		}
		
		prerequisite = {
			focus = USA_INVEST_INDIA
		}
		x = 32
		y = 12
		
		completion_reward = {
			RAJ = {
				add_opinion_modifier = { target = USA modifier = USA_INDIA_ALLIANCE }
				reverse_add_opinion_modifier = { target = USA modifier = USA_INDIA_ALLIANCE }
				
			}	
			
		}

	}
	focus = {
		id = USA_INVEST_INDIA
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_COURT_INDIA
		}
		x = 32
		y = 11
		
		completion_reward = {

			USA = {
				country_event = { id = USA_FOCUS.5 days = 1 }
			}
		}

	}
	focus = {
		id = USA_IRAN_UN_VIOLATE_2231
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_ISR_TWO_STATE
			focus = USA_ISREAL_ONE_STATE
		}
		x = 11
		y = 11

	}
	focus = {
		id = USA_INVADE_IRAN
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_IRAN_UN_VIOLATE_2231
		}
		prerequisite = {
			focus = USA_AXIS_EVIL
		}
		x = 10
		y = 12
		
		completion_reward = {
			create_wargoal = {
				type = liberate_wargoal
				target = PER
			}
		}

	}
	focus = {
		id = USA_SANCTION_RUSSIA
		icon = GFX_goal_unknown
		cost = 0.00
		prerequisite = {
			focus = USA_TRANSATLANITC
		}
		x = 6
		y = 11

	}
}
